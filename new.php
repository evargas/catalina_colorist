<?php /* Template Name: new */ ?>

<?php get_header();
query_posts(array(
   'post_type' => 'inspirations'
)); ?>
<section class="hero-section" id="hero-section" role="banner"><div class="wrap"><h1 class="archive-title">Inspiration</h1></div></section>
<div id="inspiration-png">
<div class="container">
	<div  id="contain-mgmt" class="row">
		<?php
		while (have_posts()) : the_post(); ?>
			
			<div class="col-sm-4 com-img">
			<?php the_post_thumbnail('medium'); ?> 
		<div class="entry-contain">	
		<p class="entry-metas">
			<time class="entry-time"><?php the_time( 'F j, Y' ); ?></time>
		</p>
			<!-- <?php add_image_size( 'single-post-thumbnail', 590, 180 );?> -->
		<h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>

		<p><?php the_excerpt(); ?></p>

		<?php $categories = get_categories();
		foreach($categories as $category) {
		   echo
		    '<div class="col-md-12 category-mgmt">
		  
		  <img class="ls" width="20" height="20" src="/color/wp-content/themes/theme-visual-composer/images" data-src="https://coloronly.com/wp-content/themes/coloronly/assets/images/cats.svg">
		   <a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></div>';
		}?>

</div>
</div>
<?php endwhile;
?>
</div>
</div>
</div>
<?php
get_footer();
?>