<?php
session_start();
/**
 * Template Name:Demo Template
 *
 * 
 */
get_header(); ?>
<?php

$uploads = wp_upload_dir();

if ($dir = opendir($uploads['basedir'] . '/color_swatches')) {
  $images = array();
  while (false !== ($file = readdir($dir))) {
    if ($file != "." && $file != "..") {
      $images[] = $file;
    }
  }
  closedir($dir);
}
?>
<?php
global $wpdb;
?>
<div class="<?php echo esc_attr(visualcomposerstarter_get_content_container_class()); ?>">
  <div class="content-wrapper">
    <div class="row">
      <div class="<?php echo esc_attr(visualcomposerstarter_get_maincontent_block_class()); ?>">
        <div class="main-content">
          <form id="questionaire-form" class="questionaire-form">
            <div class="row main-form dont-know-color-brand">
              <ul id="progressbar">
                <li class="active"></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
              </ul>
              <input type="button" name="restart" class="action-button restart" value="Restart Quiz">

              <fieldset class="required screen-step touch-or-all screen-step screen-step1">
                <div class="bg color-type q-1">
                  <h6 class="que">Will this be a root touch up or all over color?</h6>
                  <div>
                    <label>
                      <input type="radio" name="color-type" class="first" value="Root Touch Up">
                      <span class="input-label">Root Touch Up</span>
                    </label>
                    <label>
                      <input type="radio" name="color-type" class="last" value="All Over Color">
                      <span class="input-label">All Over Color</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>

              <fieldset class="required  screen-step screen-step2">
                <div class="currently-color sub-que">
                  <div class="bg no-count currently-coloring">
                    <h6 class="que  que2A">What type of hair color do you currently have?</h6>
                    <div>
                      <label>
                        <input type="radio" name="currently-coloring" class="first" value="All Over">
                        <span class="input-label">All Over</span>
                      </label>
                      <label>
                        <input type="radio" name="currently-coloring" class="4-wk" value="All Over Lightening">
                        <span class="input-label">All Over Lightening</span>
                      </label>
                      <label>
                        <input type="radio" name="currently-coloring" class="6-wk" value="Henna">
                        <span class="input-label">Henna</span>
                      </label>
                      <label>
                        <input type="radio" name="currently-coloring" class="8-wk" value="Color & High Lights">
                        <span class="input-label">Color & High Lights</span>
                      </label>
                      <label>
                        <input type="radio" name="currently-coloring" class="6-month currently" value="High Lights Only">
                        <span class="input-label">High Lights Only</span>
                      </label>
                      <label>
                        <input type="radio" name="currently-coloring" class="6-month last" value="Virgin">
                        <span class="input-label">None, my color is natural</span>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="no-current-color sub-que hidden">
                  <h6 class="que  que2B">Are you sure you have no color in your hair?</h6>
                  <div>
                    <label>
                      <span class="input-label have-some-color">No, I have some color</span>
                    </label>
                    <label>
                      <span class="input-label no-color-sure">Yes, I'm sure</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="action-button previous" value="Previous" />
                <input type="button" name="next" class="action-button next" id="currently-coloring-btn-next" value="Confirm" />
              </fieldset>

              <fieldset class="service-checkbox screen-step screen-step3">
                <div class="bg services q-2">
                  <h6 class="que">Have you used any of the following treatment within the last 2 months?</h6>
                  <div class="note">Choose most recent.</div>
                  <div>
                    <label>
                      <input type="checkbox" name="recent-services" value="Keratin" class="recent-services">
                      <span class="input-label">Keratin</span>
                    </label>
                    <label>
                      <input type="checkbox" name="recent-services" value="Hair Color Remover" class="recent-services">
                      <span class="input-label">Hair Color Remover</span>
                    </label>
                    <label>
                      <input type="checkbox" name="recent-services" class="recent-services check-uncheck" value="Relaxer">
                      <span class="input-label">Relaxer</span>
                    </label>
                    <label>
                      <input type="checkbox" name="recent-services" class="recent-services" value="Perm">
                      <span class="input-label">Perm</span>
                    </label>
                    <label>
                      <input type="checkbox" name="recent-services" class="recent-services" value="None">
                      <span class="input-label">None</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Confirm" />
              </fieldset>

              <fieldset class="required  screen-step screen-step4">
                <div class="bg no-count last-coloring">
                  <h6 class="que  que2A"> How long since last color?</h6>
                  <div>
                    <label>
                      <input type="radio" name="last-coloring" class="first" value="2 WKS">
                      <span class="input-label">2 Weeks</span>
                    </label>
                    <label>
                      <input type="radio" name="last-coloring" class="4-wk" value="4 WKS">
                      <span class="input-label">4 Weeks</span>
                    </label>
                    <label>
                      <input type="radio" name="last-coloring" class="6-wk" value="6 WKS">
                      <span class="input-label"> 6 Weeks</span>
                    </label>
                    <label>
                      <input type="radio" name="last-coloring" class="8-wk" value="8 WKS">
                      <span class="input-label"> 8 Weeks</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Confirm" />
              </fieldset>

              <fieldset class="required custom-tooltips screen-step screen-step5">
                <div class="bg q-10">
                  <h6 class="que">What is the condition of your hair?</h6>
                  <div class="custom-tooltips">
                    <label>
                      <input type="radio" name="hair-condition" class="first" value="Healthy">
                      <span class="input-label">Normal/Healthy</span>
                    </label>
                    <label class="got-custom-tooltip">
                      <input type="radio" name="hair-condition" class="sec" value="Porous">
                      <span class="input-label">Porous</span>
                    </label>
                    <img class="img-tooltip hidden-md hidden-lg" src='<?php echo get_stylesheet_directory_uri(); ?>/images/help.jpg' alt="help">
                    <div class="custom-tooltip hidden-sm hidden-xs">Refers to how well your hair is able to absorb and hold moisture; porous hair has a hard time holding in color.</div>
                  </div>
                  <div class="custom-tooltips">
                    <label class="got-custom-tooltip">
                      <input type="radio" name="hair-condition" class="last" value="Damaged">
                      <span class="input-label">Severe damaged</span>
                    </label>
                    <img class="img-tooltip hidden-md hidden-lg" src='<?php echo get_stylesheet_directory_uri(); ?>/images/help.jpg' alt="help">
                    <div class="custom-tooltip hidden-sm hidden-xs">this may be caused by too many chemical hair services; hair breaks from being too brittle or dry.</div>
                  </div>
                </div>
                <div class="hair-condition-damaged sub-que hidden">
                  <h6 class="que  que2B">Is your hair?</h6>
                  <div>
                    <label>
                      <input type="radio" name="hair-condition-damaged" class="last" value="Dry">
                      <span class="input-label hair-condition-dry">Dry</span>
                    </label>
                    <label>
                      <input type="radio" name="hair-condition-damaged" class="last" value="Breakage">
                      <span class="input-label hair-condition-breakage">Breakage</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>

              <fieldset class="required  screen-step screen-step6">
                <div class="select-hair-length">
                  <div class="bg hair-length no-count">
                    <h6 class="que  que2B"> Select length of hair?</h6>
                    <div>
                      <label>
                        <input type="radio" name="hair-length" class="first short" value="Very Short">
                        <span class="input-label">Very Short</span>
                      </label>
                      <label>
                        <input type="radio" name="hair-length" class="first short" value="Short">
                        <span class="input-label">Short</span>
                      </label>
                      <label>
                        <input type="radio" name="hair-length" class="mid medium" value="Shoulder Length">
                        <span class="input-label">Shoulder Length</span>
                      </label>
                      <label>
                        <input type="radio" name="hair-length" class="last long" value="Long">
                        <span class="input-label">Long</span>
                      </label>
                      <label>
                        <input type="radio" name="hair-length" class="last long" value="Very Long">
                        <span class="input-label">Very Long</span>
                      </label>
                    </div>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Confirm" />
              </fieldset>

              <fieldset class="required screen-step screen-step7">
                <div class="bg how-much-gray">
                  <h6 class="que mb-1">How much gray do you have?</h6>
                  <div class="note">To be specific - look at your hair at the roots as it grows in.</div>
                  <div>
                    <label>
                      <input type="radio" name="much-gray" class="first" value="None">
                      <span class="input-label">None or hardly any</span>
                    </label>
                    <label>
                      <input type="radio" name="much-gray" class="sec" value="Few">
                      <span class="input-label">Some</span>
                    </label>
                  </div>
                  <div>
                    <label>
                      <input type="radio" name="much-gray" class="neutral" value="Lots">
                      <span class="input-label">Mostly Gray</span>
                    </label>
                    <label>
                      <input type="radio" name="much-gray" class="last" value="All">
                      <span class="input-label">All Gray</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>

              <fieldset class="required  screen-step screen-step8">
                <div class="hair-resistant">
                  <h6 class="que  que2B"> Is your hair/gray resistance to color?</h6>
                  <div>
                    <label>
                      <input type="radio" name="hair-resistant" class="first resistant" value="Yes">
                      <span class="input-label">Yes</span>
                    </label>
                    <label>
                      <input type="radio" name="hair-resistant" class="first not-resistant" value="No">
                      <span class="input-label">No</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Confirm" />
              </fieldset>

              <fieldset class="required  screen-step screen-step9">
                <div class="natural-texture sub-que">
                  <h6 class="que  que2B"> What is your natural texture?</h6>
                  <div>
                    <label>
                      <input type="radio" name="natural-texture" class="first resistant" value="Fine">
                      <span class="input-label">Fine</span>
                    </label>
                    <label>
                      <input type="radio" name="natural-texture" class="first not-resistant" value="Normal">
                      <span class="input-label">Normal</span>
                    </label>
                    <label>
                      <input type="radio" name="natural-texture" class="first resistant" value="Coarse">
                      <span class="input-label">Coarse</span>
                    </label>
                    <label>
                      <input type="radio" name="natural-texture" class="first not-resistant" value="Very Coarse">
                      <span class="input-label">Very Coarse</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Confirm" />
              </fieldset>

              <fieldset class="required screen-step screen-step10">
                <div class="bg color-shades new-color q-9">
                  <h6 class="que natural-label">What is the natural color of you hair?</h6>
                  <h6 class="que current-label">What is the current color of you hair?</h6>
                  <div>

                    <label class="natural-label">
                      <input type="radio" name="natural-color-shades" value="Black">
                      <span class="input-label bg-black" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-1.jpg) !important;">Black</span>
                    </label>
                    <label class="natural-label">
                      <input type="radio" name="natural-color-shades" value="Brown">
                      <span class="input-label bg-brown" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-3.jpg) !important;">Brown</span>
                    </label>
                    <label class="natural-label">
                      <input type="radio" name="natural-color-shades" value="Blonde">
                      <span class="input-label bg-blonde" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-7.jpg) !important;">Blonde</span>
                    </label>
                    <label class="natural-label">
                      <input type="radio" name="natural-color-shades" value="Red">
                      <span class="input-label bg-red" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Auburn_6.54.jpg) !important;">Red</span>
                    </label>

                    <label class="current-label">
                      <input type="radio" name="current-color-shades" value="Black">
                      <span class="input-label bg-black" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-1.jpg) !important;">Black</span>
                    </label>
                    <label class="current-label">
                      <input type="radio" name="current-color-shades" value="Brown">
                      <span class="input-label bg-brown" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-3.jpg) !important;">Brown</span>
                    </label>
                    <label class="current-label">
                      <input type="radio" name="current-color-shades" value="Blonde">
                      <span class="input-label bg-blonde" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-7.jpg) !important;">Blonde</span>
                    </label>
                    <label class="current-label">
                      <input type="radio" name="current-color-shades" value="Red">
                      <span class="input-label bg-red" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Auburn_6.54.jpg) !important;">Red</span>
                    </label>

                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>

              <fieldset class="required screen-step screen-step11">
                <div class="bg new-color sub-que-row q9-sub-que q9-sub-1">
                  <h6 class="que">how dark is your natural hair? </h6>
                  <div>
                    <label class="Black">
                      <input type="radio" name="natural-color-level" value="1">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-1.jpg) !important;">Level-1</span>
                    </label>
                    <label class="Black">
                      <input type="radio" name="natural-color-level" value="2">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-2.jpg) !important;">Level-2</span>
                    </label>
                    <label class="Black">
                      <input type="radio" name="natural-color-level" value="3">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-1.jpg) !important;">Level-3</span>
                    </label>
                    <label class="Brown">
                      <input type="radio" name="natural-color-level" value="4">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-4.jpg) !important;">Level-4</span>
                    </label>
                    <label class="Brown">
                      <input type="radio" name="natural-color-level" value="5">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-5.jpg) !important;">Level-5</span>
                    </label>
                    <label class="Brown">
                      <input type="radio" name="natural-color-level" value="6">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-6.jpg) !important;">Level-6</span>
                    </label>
                    <label class="Brown">
                      <input type="radio" name="natural-color-level" value="7">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-6.jpg) !important;">Level-7</span>
                    </label>
                    <label class="Blonde">
                      <input type="radio" name="natural-color-level" value="8">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-8.jpg) !important;">Level-8</span>
                    </label>
                    <label class="Blonde">
                      <input type="radio" name="natural-color-level" value="9">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-9.jpg) !important;">Level-9</span>
                    </label>
                    <label class="Blonde">
                      <input type="radio" name="natural-color-level" value="10">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-10.jpg) !important;">Level-10 </span>
                    </label>
                    <label class="Blonde">
                      <input type="radio" name="natural-color-level" value="11">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-10.jpg) !important;">Level-11 </span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="natural-color-level" value="3">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Auburn_6.54.jpg) !important;">Level-3</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="natural-color-level" value="4">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Copper_gold_6.43.jpg) !important;">Level-4</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="natural-color-level" value="5">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Mahogany6.56.jpg) !important;">Level-5</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="natural-color-level" value="6">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Red_6.5.jpg) !important;">Level-6</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="natural-color-level" value="7">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Auburn_6.54.jpg) !important;">Level-7</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="natural-color-level" value="8">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Copper_gold_6.43.jpg) !important;">Level-8</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="natural-color-level" value="9">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Mahogany6.56.jpg) !important;">Level-9</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="natural-color-level" value="10">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Red_6.5.jpg) !important;">Level-10</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="natural-color-level" value="11">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Mahogany6.56.jpg) !important;">Level-11</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>

              <fieldset class="required screen-step screen-step12">
                <div class="bg color-shades new-color q-9">
                  <h6 class="que">What is your desired color? </h6>
                  <div>
                    <label>
                      <input type="radio" name="color-shades" class="first" value="Black">
                      <span class="input-label bg-black" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-1.jpg) !important;">Black</span>
                    </label>
                    <label>
                      <input type="radio" name="color-shades" class="brown" value="Brown">
                      <span class="input-label bg-brown" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-3.jpg) !important;">Brown</span>
                    </label>
                    <label>
                      <input type="radio" name="color-shades" class="blonde" value="Blonde">
                      <span class="input-label bg-blonde" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-7.jpg) !important;">Blonde</span>
                    </label>
                    <label>
                      <input type="radio" name="color-shades" class="last" value="Red">
                      <span class="input-label bg-red" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Auburn_6.54.jpg) !important;">Red</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>

              <fieldset class="required screen-step screen-step13">
                <div class="bg black-options no-count hidden new-color sub-que-row q9-sub-que q9-sub-1">
                  <h6 class="que">How dark would you like your hair? </h6>
                  <div>
                    <label class="Black">
                      <input type="radio" name="color-level" class="first" value="1">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-1.jpg) !important;">Level-1</span>
                    </label>
                    <label class="Black">
                      <input type="radio" name="color-level" class="sec" value="2">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-2.jpg) !important;">Level-2</span>
                    </label>
                    <label class="Black">
                      <input type="radio" name="color-level" class="first" value="3">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-1.jpg) !important;">Level-3</span>
                    </label>
                  </div>
                  <div class="color-error-level">Your current level is not compatible with this color, you must go back</div>
                </div>
                <div class="bg brown-options no-count new-color  hidden sub-que-row q9-sub-que q9-sub-2">
                  <h6 class="que"> How dark would you like your hair? </h6>
                  <div>
                    <label class="Brown">
                      <input type="radio" name="color-level" class="sec" value="4">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-4.jpg) !important;">Level-4</span>
                    </label>
                    <label class="Brown">
                      <input type="radio" name="color-level" class="third" value="5">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-5.jpg) !important;">Level-5</span>
                    </label>
                    <label class="Brown">
                      <input type="radio" name="color-level" class="fourth" value="6">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-6.jpg) !important;">Level-6</span>
                    </label>
                    <label class="Brown">
                      <input type="radio" name="color-level" class="fourth" value="7">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-6.jpg) !important;">Level-7</span>
                    </label>
                  </div>
                  <div class="color-error-level">Your current level is not compatible with this color, you must go back</div>
                </div>
                <div class="bg blonde-options no-count hidden new-color sub-que-row q9-sub-que q9-sub-3">
                  <h6 class="que"> How dark would you like your hair?</h6>
                  <div>
                    <label class="Blonde">
                      <input type="radio" name="color-level" class="sec" value="8">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-8.jpg) !important;">Level-8</span>
                    </label>
                    <label class="Blonde">
                      <input type="radio" name="color-level" class="third" value="9">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-9.jpg) !important;">Level-9</span>
                    </label>
                    <label class="Blonde">
                      <input type="radio" name="color-level" class="last" value="10">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-10.jpg) !important;">Level-10 </span>
                    </label>
                    <label class="Blonde">
                      <input type="radio" name="color-level" class="last" value="11">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-10.jpg) !important;">Level-11 </span>
                    </label>
                  </div>
                  <div class="color-error-level">Your current level is not compatible with this color, you must go back</div>
                </div>
                <div class="bg red-options no-count new-color hidden sub-que-row q9-sub-que q9-sub-4">
                  <h6 class="que "> How dark would you like your hair? </h6>
                  <div>
                    <label class="Red">
                      <input type="radio" name="color-level" class="first" value="3">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Auburn_6.54.jpg) !important;">Level-3</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="color-level" class="sec" value="4">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Copper_gold_6.43.jpg) !important;">Level-4</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="color-level" class="last" value="5">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Mahogany6.56.jpg) !important;">Level-5</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="color-level" class="third" value="6">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Red_6.5.jpg) !important;">Level-6</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="color-level" class="first" value="7">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Auburn_6.54.jpg) !important;">Level-7</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="color-level" class="sec" value="8">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Copper_gold_6.43.jpg) !important;">Level-8</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="color-level" class="last" value="9">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Mahogany6.56.jpg) !important;">Level-9</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="color-level" class="third" value="10">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Red_6.5.jpg) !important;">Level-10</span>
                    </label>
                    <label class="Red">
                      <input type="radio" name="color-level" class="last" value="11">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Mahogany6.56.jpg) !important;">Level-11</span>
                    </label>
                  </div>
                  <div class="color-error-level">Your current level is not compatible with this color, you must go back</div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>

              <fieldset class="required screen-step screen-step14">
                <div class="bg black-options no-count hidden new-color sub-que-row q9-sub-que q9-sub-1">
                  <h6 class="que">What is the desired outcome of your black color? </h6>
                  <div>
                    <label>
                      <input type="radio" name="color-shade" class="first" value="Blue Black">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-1.jpg) !important;">Blue Black</span>
                    </label>
                    <label>
                      <input type="radio" name="color-shade" class="sec" value="Natural Black">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-2.jpg) !important;">Natural Black</span>
                    </label>

                    <label>
                      <input type="radio" name="color-shade" class="first" value="Violet Black">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-1.jpg) !important;">Violet Black</span>
                    </label>
                  </div>
                </div>
                <div class="bg brown-options no-count new-color  hidden sub-que-row q9-sub-que q9-sub-2">
                  <h6 class="que"> What is the desired outcome of your brown color? </h6>
                  <div>
                    <label>
                      <input type="radio" name="color-shade" class="first" value="Golden Brown">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-3.jpg) !important;">Golden Brown</span>
                    </label>
                    <label>
                      <input type="radio" name="color-shade" class="sec" value="Mahogany Brown">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-4.jpg) !important;">Mahogany Brown</span>
                    </label>

                    <label>
                      <input type="radio" name="color-shade" class="third" value="Cool Brown">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-5.jpg) !important;">Cool Brown</span>
                    </label>
                    <label>
                      <input type="radio" name="color-shade" class="fourth" value="Natural Brown">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-6.jpg) !important;">Natural Brown</span>
                    </label>
                  </div>
                </div>
                <div class="bg blonde-options no-count hidden new-color sub-que-row q9-sub-que q9-sub-3">
                  <h6 class="que"> What is the desired outcome of your blonde color?</h6>
                  <div>
                    <label><input type="radio" name="color-shade" class="first" value="Cool Blonde">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-7.jpg) !important;">Cool Blonde</span>
                    </label>
                    <label>
                      <input type="radio" name="color-shade" class="sec" value="Golden Blonde">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-8.jpg) !important;">Golden Blonde</span>
                    </label>
                  </div>
                  <div>
                    <label>
                      <input type="radio" name="color-shade" class="third" value="Natural Blonde">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-9.jpg) !important;">Natural Blonde</span>
                    </label>
                    <label>
                      <input type="radio" name="color-shade" class="last" value="Strawberry Blonde">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-10.jpg) !important;">Strawberry Blonde </span>
                    </label>
                  </div>
                </div>
                <div class="bg red-options no-count new-color hidden sub-que-row q9-sub-que q9-sub-4">
                  <h6 class="que "> What is the desired outcome of your red color?</h6>
                  <div>
                    <label>
                      <input type="radio" name="color-shade" class="first" value="Mahogany Red">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Mahogany6.56.jpg) !important;">Mahogany Red</span>
                    </label>
                    <label>
                      <input type="radio" name="color-shade" class="sec" value="Violet Red">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Auburn_6.54.jpg) !important;">Violet Red</span>
                    </label>
                    <label>
                      <input type="radio" name="color-shade" class="third" value="Copper">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Copper_gold_6.43.jpg) !important;">Copper</span>
                    </label>
                    <label>
                      <input type="radio" name="color-shade" class="fourth" value="True Red">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Red_6.5.jpg) !important;">True Red</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>

              <fieldset class="required preview swatch-selection screen-step screen-step15">
                <div class="bg swatches sub-que-row q9-sub-que q9-sub-1">
                  <h6 class="que">Which color swatch would you like? </h6>
                  <div class="multi-swatches">
                    <h6 class="text-left"></h6>
                    <div class="col-sm-6 listing listing-centered">
                      <ul>
                      </ul>
                    </div>
                    <div class="col-sm-6">
                      <div class="selected-swatch listing-centered">
                      </div>
                    </div>
                  </div>
                  <div class="color-error-swatch-level">Your selected level is not compatible with this color, you must go back</div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button hidden show-current-data" value="Confirm" />
              </fieldset>

              <fieldset class="summary summary1 screen-step screen-step16">
                <h5 style="color:#000;margin-bottom:20px;">Questionnaire Summary</h5>
                <div class="row">
                  <div class="col-sm-4">
                    <div class="summary-field">Date:<span class="current_date"></span></div>
                    <div class="summary-field">Kit Choice:<span class="kit_choice"></span></div>
                    <div class="summary-field">Formula:<span class="formula"></span></div>
                  </div>
                  <div class="col-sm-4">
                    <input placeholder="What is your name?" id="user-name" name="user-name" type="text" required>
                    <input placeholder="Mobile number?" id="user-phone" name="user-phone" type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required>
                    <input placeholder="Email" id="user-email" name="user-email" type="email" required>
                  </div>
                  <div class="col-sm-4">
                    <input type="button" id="user-fields" name="submit" class="text-center action-button" value="Submit" />
                  </div>
                </div>
              </fieldset>

              <fieldset class="summary screen-step screen-step17">
                <h5 style="color:#000;margin-bottom:20px;">Questionnaire Summary</h5>
                <div class="row">
                  <div class="col-sm-4">
                    <div class="summary-field">Date:<span class="current_date"></span></div>
                    <div class="summary-field">Kit Choice:<span class="kit_choice"></span></div>
                    <div class="summary-field">Formula:<span class="formula"></span></div>
                  </div>
                  <div class="col-sm-4">
                    <div class="summary-field">Name:<span class="user-name"></span></div>
                    <div class="summary-field">Phone:<span class="user-phone"></span></div>
                    <div class="summary-field">Email:<span class="user-email"></span></div>
                  </div>
                  <div class="col-sm-4">
                    <div class="alert alert-success" role="alert">
                      <strong>Well done!</strong>
                      You successfully read this important alert message
                    </div>
                  </div>
                </div>
                <fieldset>
                  <legend>Just For Dev</legend>
                  <div id="json_output"></div>
                </fieldset>
              </fieldset>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" type="text/javascript"></script>
<script>
  var base = "<?php echo get_home_url(); ?>";
  document.getElementById("questionaire-form").reset();

  var state = {};
  var master_tones_list = {};

  master_tones_list['Natural Black'] = [];
  master_tones_list['Natural Black'][0] = [];
  master_tones_list['Natural Black'][1] = [];
  master_tones_list['Violet Black'] = [];
  master_tones_list['Violet Black'][0] = [];
  master_tones_list['Violet Black'][1] = [];
  master_tones_list['Blue Black'] = [];
  master_tones_list['Blue Black'][0] = [];
  master_tones_list['Blue Black'][1] = [];
  master_tones_list['Mahogany Brown'] = [];
  master_tones_list['Mahogany Brown'][0] = [];
  master_tones_list['Mahogany Brown'][1] = [];
  master_tones_list['Golden Brown'] = [];
  master_tones_list['Golden Brown'][0] = [];
  master_tones_list['Golden Brown'][1] = [];
  master_tones_list['Natural Brown'] = [];
  master_tones_list['Natural Brown'][0] = [];
  master_tones_list['Natural Brown'][1] = [];
  master_tones_list['Cool Brown'] = [];
  master_tones_list['Cool Brown'][0] = [];
  master_tones_list['Cool Brown'][1] = [];
  master_tones_list['Golden Blonde'] = [];
  master_tones_list['Golden Blonde'][0] = [];
  master_tones_list['Golden Blonde'][1] = [];
  master_tones_list['Cool Blonde'] = [];
  master_tones_list['Cool Blonde'][0] = [];
  master_tones_list['Cool Blonde'][1] = [];
  master_tones_list['Natural Blonde'] = [];
  master_tones_list['Natural Blonde'][0] = [];
  master_tones_list['Natural Blonde'][1] = [];
  master_tones_list['Strawberry Blonde'] = [];
  master_tones_list['Strawberry Blonde'][0] = [];
  master_tones_list['Strawberry Blonde'][1] = [];
  master_tones_list['True Red'] = [];
  master_tones_list['True Red'][0] = [];
  master_tones_list['True Red'][1] = [];
  master_tones_list['Copper'] = [];
  master_tones_list['Copper'][0] = [];
  master_tones_list['Copper'][1] = [];
  master_tones_list['Mahogany'] = [];
  master_tones_list['Mahogany'][0] = [];
  master_tones_list['Mahogany'][1] = [];

  master_tones_list['Mahogany Red'] = [];
  master_tones_list['Mahogany Red'][0] = [];
  master_tones_list['Mahogany Red'][1] = [];

  master_tones_list['Violet Red'] = [];
  master_tones_list['Violet Red'][0] = [];
  master_tones_list['Violet Red'][1] = [];

  master_tones_list['Natural Black'][1][1] = ['1N'];
  master_tones_list['Natural Black'][1][2] = ['1N', '2N', '2.5NNN'];
  master_tones_list['Natural Black'][1][3] = ['1N', '2N', '2.5NNN', '3N', '3.5NNN'];
  master_tones_list['Natural Black'][0][1] = ['1N', '2N', '2.5NNN', '3N', '3.5NNN'];
  master_tones_list['Natural Black'][0][2] = ['1N', '2N', '2.5NNN', '3N', '3.5NNN'];
  master_tones_list['Natural Black'][0][3] = ['1N', '2N', '2.5NNN', '3N', '3.5NNN'];
  master_tones_list['Natural Black'][0][4] = ['2N', '2.5NNN', '3N', '3.5NNN'];
  master_tones_list['Natural Black'][0][5] = ['3N', '3.5NNN'];

  master_tones_list['Violet Black'][0][1] = ['3NVV'];
  master_tones_list['Violet Black'][0][2] = ['3NVV', '4NVV'];
  master_tones_list['Violet Black'][0][3] = ['3NVV', '4NVV'];
  master_tones_list['Violet Black'][0][4] = ['3NVV', '4NVV'];
  master_tones_list['Violet Black'][0][5] = ['3NVV', '4NVV'];
  master_tones_list['Violet Black'][0][6] = ['4NVV'];
  master_tones_list['Violet Black'][1][1] = ['3NVV'];
  master_tones_list['Violet Black'][1][2] = ['3NVV'];
  master_tones_list['Violet Black'][1][3] = ['3NVV'];
  master_tones_list['Violet Black'][1][4] = ['3NVV', '4NVV'];
  master_tones_list['Violet Black'][1][5] = [];
  master_tones_list['Violet Black'][1][6] = [];

  master_tones_list['Blue Black'][0][1] = ['1NAA', '2NAA', '3NAA'];
  master_tones_list['Blue Black'][0][2] = ['1NAA', '2NAA', '3NAA'];
  master_tones_list['Blue Black'][0][3] = ['1NAA', '2NAA', '3NAA'];
  master_tones_list['Blue Black'][0][4] = ['2NAA', '3NAA'];
  master_tones_list['Blue Black'][0][5] = ['3NAA'];
  master_tones_list['Blue Black'][1][1] = ['1NAA'];
  master_tones_list['Blue Black'][1][2] = ['1NAA', '2NAA'];
  master_tones_list['Blue Black'][1][3] = ['1NAA', '2NAA', '3NAA'];
  master_tones_list['Blue Black'][1][4] = ['2NAA', '3NAA'];
  master_tones_list['Blue Black'][1][5] = ['3NAA'];

  master_tones_list['Mahogany Brown'][0][1] = ['3NMG'];
  master_tones_list['Mahogany Brown'][0][2] = ['3NMG', '4NGM'];
  master_tones_list['Mahogany Brown'][0][3] = ['3NMG', '4NGM', '4NMG', '5NGM', '5NMG'];
  master_tones_list['Mahogany Brown'][0][4] = ['3NMG', '4NGM', '4NMG', '5NGM', '5NMG', '6NGM', '6NMG'];
  master_tones_list['Mahogany Brown'][0][5] = ['3NMG', '4NGM', '4NMG', '5NGM', '5NMG', '6NGM', '6NMG', '7NGM', '7NMG'];
  master_tones_list['Mahogany Brown'][0][6] = ['4NGM', '4NMG', '5NGM', '5NMG', '6NGM', '6NMG', '7NGM', '7NMG', '8NGM'];
  master_tones_list['Mahogany Brown'][0][7] = ['5NGM', '5NMG', '6NGM', '6NMG', '7NGM', '7NMG', '8NGM'];
  master_tones_list['Mahogany Brown'][0][8] = ['6NGM', '6NMG', '7NGM', '7NMG', '8NGM'];
  master_tones_list['Mahogany Brown'][0][9] = ['7NGM', '7NMG', '8NGM'];
  master_tones_list['Mahogany Brown'][0][10] = ['8NGM'];
  master_tones_list['Mahogany Brown'][1][1] = [];
  master_tones_list['Mahogany Brown'][1][2] = [];
  master_tones_list['Mahogany Brown'][1][3] = ['3NMG'];
  master_tones_list['Mahogany Brown'][1][4] = ['3NMG', '4NGM', '4NMG'];
  master_tones_list['Mahogany Brown'][1][5] = ['3NMG', '4NGM', '4NMG', '5NGM', '5NMG'];
  master_tones_list['Mahogany Brown'][1][6] = ['4NGM', '4NMG', '5NGM', '5NMG', '6NGM', '6NMG'];
  master_tones_list['Mahogany Brown'][1][7] = ['5NGM', '5NMG', '6NGM', '6NMG', '7NGM', '7NMG'];
  master_tones_list['Mahogany Brown'][1][8] = ['6NGM', '6NMG', '7NGM', '7NMG', '8NGM'];
  master_tones_list['Mahogany Brown'][1][9] = ['7NGM', '7NMG', '8NGM'];
  master_tones_list['Mahogany Brown'][1][10] = ['8NGM'];

  master_tones_list['Golden Brown'][0][1] = [];
  master_tones_list['Golden Brown'][0][2] = ['4G', '4GA'];
  master_tones_list['Golden Brown'][0][3] = ['4G', '4GA', '5G', '5GA'];
  master_tones_list['Golden Brown'][0][4] = ['4G', '4GA', '5G', '5GA', '6G', '6GA'];
  master_tones_list['Golden Brown'][0][5] = ['4G', '4GA', '5G', '5GA', '6G', '6GA', '7G', '7GA'];
  master_tones_list['Golden Brown'][0][6] = ['4G', '4GA', '5G', '5GA', '6G', '6GA', '7G', '7GA'];
  master_tones_list['Golden Brown'][0][7] = ['5G', '5GA', '6G', '6GA', '7G', '7GA', '7NG'];
  master_tones_list['Golden Brown'][0][8] = ['6G', '6GA', '7G', '7GA', '7NG'];
  master_tones_list['Golden Brown'][0][9] = ['7G', '7GA', '7NG'];
  master_tones_list['Golden Brown'][1][1] = [];
  master_tones_list['Golden Brown'][1][2] = [];
  master_tones_list['Golden Brown'][1][3] = [];
  master_tones_list['Golden Brown'][1][4] = ['4G', '4GA'];
  master_tones_list['Golden Brown'][1][5] = ['4G', '4GA', '5G', '5GA'];
  master_tones_list['Golden Brown'][1][6] = ['4G', '4GA', '5G', '5GA', '6G', '6GA'];
  master_tones_list['Golden Brown'][1][7] = ['5G', '5GA', '6G', '6GA', '7G', '7GA'];
  master_tones_list['Golden Brown'][1][8] = ['6G', '6GA', '7G', '7GA'];
  master_tones_list['Golden Brown'][1][9] = ['7G', '7GA'];

  master_tones_list['Natural Brown'][0][1] = ['3N', '3.5NNN', '3NAA', '3NNA'];
  master_tones_list['Natural Brown'][0][2] = ['3N', '3.5NNN', '3NAA', '3NNA', '4N', '4NAA', '4.5NNN'];
  master_tones_list['Natural Brown'][0][3] = ['3N', '3.5NNN', '3NAA', '3NNA', '4N', '4NAA', '4.5NNN', '5N'];
  master_tones_list['Natural Brown'][0][4] = ['3N', '3.5NNN', '3NAA', '3NNA', '4N', '4NAA', '4.5NNN', '5N', '6N'];
  master_tones_list['Natural Brown'][0][5] = ['3N', '3.5NNN', '3NAA', '3NNA', '4N', '4NAA', '4.5NNN', '5N', '6N', '7N'];
  master_tones_list['Natural Brown'][0][6] = ['4N', '4NAA', '4.5NNN', '5N', '6N', '7N'];
  master_tones_list['Natural Brown'][0][7] = ['5N', '6N', '7N'];
  master_tones_list['Natural Brown'][0][8] = ['6N', '7N'];
  master_tones_list['Natural Brown'][0][9] = ['7N'];
  master_tones_list['Natural Brown'][1][1] = [];
  master_tones_list['Natural Brown'][1][2] = [];
  master_tones_list['Natural Brown'][1][3] = ['3N', '3.5NNN', '3NAA', '3NNA'];
  master_tones_list['Natural Brown'][1][4] = ['3N', '3.5NNN', '3NAA', '3NNA', '4N', '4NAA', '4.5NNN'];
  master_tones_list['Natural Brown'][1][5] = ['3N', '3.5NNN', '3NAA', '3NNA', '4N', '4NAA', '4.5NNN', '5N'];
  master_tones_list['Natural Brown'][1][6] = ['4N', '4NAA', '4.5NNN', '5N', '6N'];
  master_tones_list['Natural Brown'][1][7] = ['5N', '6N', '7N'];
  master_tones_list['Natural Brown'][1][8] = ['7N'];
  master_tones_list['Natural Brown'][1][9] = ['6N', '7N'];

  master_tones_list['Cool Brown'][0][1] = ['3NNA', '3NAA'];
  master_tones_list['Cool Brown'][0][2] = ['3NNA', '3NAA', '4NAA'];
  master_tones_list['Cool Brown'][0][3] = ['3NNA', '3NAA', '4NAA', '5NA', '5NAA'];
  master_tones_list['Cool Brown'][0][4] = ['3NNA', '3NAA', '4NAA', '5NA', '5NAA', '6NA', '6NAA'];
  master_tones_list['Cool Brown'][0][5] = ['3NNA', '3NAA', '4NAA', '5NA', '5NAA', '6NA', '6NAA', '7NA', '7NAA'];
  master_tones_list['Cool Brown'][0][6] = ['4NAA', '5NA', '5NAA', '6NA', '6NAA', '7NA', '7NAA'];
  master_tones_list['Cool Brown'][0][7] = ['5NA', '5NAA', '6NA', '6NAA', '7NA', '7NAA'];
  master_tones_list['Cool Brown'][0][8] = ['6NA', '6NAA', '7NA', '7NAA'];
  master_tones_list['Cool Brown'][0][9] = [];
  master_tones_list['Cool Brown'][1][1] = [];
  master_tones_list['Cool Brown'][1][2] = [];
  master_tones_list['Cool Brown'][1][3] = ['3NNA', '3NAA'];
  master_tones_list['Cool Brown'][1][4] = ['3NNA', '3NAA', '4NAA'];
  master_tones_list['Cool Brown'][1][5] = ['3NNA', '3NAA', '4NAA', '5NA', '5NAA'];
  master_tones_list['Cool Brown'][1][6] = ['4NAA', '5NA', '5NAA', '6NA', '6NAA'];
  master_tones_list['Cool Brown'][1][7] = ['5NA', '5NAA', '6NA', '6NAA', '7NA', '7NAA'];
  master_tones_list['Cool Brown'][1][8] = ['6NA', '6NAA', '7NA', '7NAA'];
  master_tones_list['Cool Brown'][1][9] = ['7NA', '7NAA'];

  master_tones_list['Cool Blonde'][0][5] = ['7NAA', '7NA', '7N'];
  master_tones_list['Cool Blonde'][0][6] = ['7NAA', '7NA', '7N', '8NAA', '8N', '8NN'];
  master_tones_list['Cool Blonde'][0][7] = ['7NAA', '7NA', '7N', '8NAA', '8N', '8NN', '9NAA', '9N', '9NN'];
  master_tones_list['Cool Blonde'][0][8] = ['7NAA', '7NA', '7N', '8NAA', '8N', '8NN', '9NAA', '9N', '9NN', '10NAA', '10NA', '10AV', '10NN'];
  master_tones_list['Cool Blonde'][0][9] = ['7NAA', '7NA', '7N', '8NAA', '8N', '8NN', '9NAA', '9N', '9NN', '10NAA', '10NA', '10AV', '10NN', '11N', '11AV', '11NN'];
  master_tones_list['Cool Blonde'][0][10] = ['8NAA', '8N', '8NN', '9NAA', '9N', '9NN', '10NAA', '10NA', '10AV', '10NN', '11N', '11AV', '11NN'];
  master_tones_list['Cool Blonde'][0][11] = ['9NAA', '9N', '9NN', '10NAA', '10NA', '10AV', '10NN', '11N', '11AV', '11NN'];
  master_tones_list['Cool Blonde'][0][12] = ['10NAA', '10NA', '10AV', '10NN', '11N', '11AV', '11NN'];
  master_tones_list['Cool Blonde'][0][13] = ['11N', '11AV', '11NN'];
  master_tones_list['Cool Blonde'][1][5] = [];
  master_tones_list['Cool Blonde'][1][6] = [];
  master_tones_list['Cool Blonde'][1][7] = ['7NAA', '7NA', '7N'];
  master_tones_list['Cool Blonde'][1][8] = ['7NAA', '7NA', '7N', '8NAA', '8N', '8NN'];
  master_tones_list['Cool Blonde'][1][9] = ['7NAA', '7NA', '7N', '8NAA', '8N', '8NN', '9NAA', '9N', '9NN'];
  master_tones_list['Cool Blonde'][1][10] = ['8NAA', '8N', '8NN', '9NAA', '9N', '9NN', '10NAA', '10NA', '10AV', '10NN'];
  master_tones_list['Cool Blonde'][1][11] = ['9NAA', '9N', '9NN', '10NAA', '10NA', '10AV', '10NN', '11N', '11AV', '11NN'];
  master_tones_list['Cool Blonde'][1][12] = ['10NAA', '10NA', '10AV', '10NN', '11N', '11AV', '11NN'];
  master_tones_list['Cool Blonde'][1][13] = ['11N', '11AV', '11NN'];

  master_tones_list['Golden Blonde'][0][5] = ['7G, 7GA'];
  master_tones_list['Golden Blonde'][0][6] = ['7G, 7GA'];
  master_tones_list['Golden Blonde'][0][7] = ['7G, 7GA'];
  master_tones_list['Golden Blonde'][0][8] = ['7G, 7GA','8G', '8GA', '9G','9GA', '10G', '10GA' ];
  master_tones_list['Golden Blonde'][0][9] = ['7G, 7GA','8G', '8GA', '9G','9GA', '10G', '10GA', '11GA'];
  master_tones_list['Golden Blonde'][0][10] = ['8G', '8GA', '9G','9GA', '10G', '10GA', '11GA'];
  master_tones_list['Golden Blonde'][0][11] = ['9G','9GA', '10G', '10GA','11GA'];
  master_tones_list['Golden Blonde'][1][5] = [];
  master_tones_list['Golden Blonde'][1][6] = [];
  master_tones_list['Golden Blonde'][1][7] = ['7G, 7GA'];
  master_tones_list['Golden Blonde'][1][8] = ['7G, 7GA','8G', '8GA'];
  master_tones_list['Golden Blonde'][1][9] = ['7G, 7GA','8G', '8GA', '9G','9GA'];
  master_tones_list['Golden Blonde'][1][10] = ['8G', '8GA', '9G','9GA', '10G', '10GA'];
  master_tones_list['Golden Blonde'][1][11] = ['9G','9GA', '10G', '10GA','11GA'];

  master_tones_list['Natural Blonde'][0][5] = ['7N'];
  master_tones_list['Natural Blonde'][0][6] = ['7N', '8N'];
  master_tones_list['Natural Blonde'][0][7] = ['7N', '8N', '9N'];
  master_tones_list['Natural Blonde'][0][8] = ['7N', '8N', '9N', '10N'];
  master_tones_list['Natural Blonde'][0][9] = ['7N', '8N', '9N', '10N'];
  master_tones_list['Natural Blonde'][0][10] = ['8N', '9N', '10N'];
  master_tones_list['Natural Blonde'][0][11] = ['9N', '10N', '11N'];
  master_tones_list['Natural Blonde'][0][12] = ['10N', '11N'];
  master_tones_list['Natural Blonde'][1][5] = [];
  master_tones_list['Natural Blonde'][1][6] = [];
  master_tones_list['Natural Blonde'][1][7] = ['7N'];
  master_tones_list['Natural Blonde'][1][8] = ['7N', '8N'];
  master_tones_list['Natural Blonde'][1][9] = ['8N', '9N'];
  master_tones_list['Natural Blonde'][1][10] = ['8N', '9N', '10N'];
  master_tones_list['Natural Blonde'][1][11] = ['9N', '10N', '11N'];
  master_tones_list['Natural Blonde'][1][12] = ['10N', '11N'];

  master_tones_list['Strawberry Blonde'][0][5] = ['7NV', '7NVA'];
  master_tones_list['Strawberry Blonde'][0][6] = ['7NV', '7NVA', '8NV', '8NVA'];
  master_tones_list['Strawberry Blonde'][0][7] = ['7NV', '7NVA', '8NV', '8NVA', '9NV', '9NVA'];
  master_tones_list['Strawberry Blonde'][0][8] = ['7NV', '7NVA', '8NV', '8NVA', '9NV', '9NVA', '10NV', '10NVA'];
  master_tones_list['Strawberry Blonde'][0][9] = ['7NV', '7NVA', '8NV', '8NVA', '9NV', '9NVA', '10NV', '10NVA'];
  master_tones_list['Strawberry Blonde'][0][10] = ['8NV', '8NVA', '9NV', '9NVA', '10NV', '10NVA'];
  master_tones_list['Strawberry Blonde'][0][11] = ['9NV', '9NVA', '10NV', '10NVA'];
  master_tones_list['Strawberry Blonde'][0][12] = ['10NV', '10NVA'];
  master_tones_list['Strawberry Blonde'][1][5] = [];
  master_tones_list['Strawberry Blonde'][1][6] = [];
  master_tones_list['Strawberry Blonde'][1][7] = ['7NV', '7NVA'];
  master_tones_list['Strawberry Blonde'][1][8] = ['7NV', '7NVA', '8NV', '8NVA'];
  master_tones_list['Strawberry Blonde'][1][9] = ['7NV', '7NVA', '8NV', '8NVA', '9NV', '9NVA'];
  master_tones_list['Strawberry Blonde'][1][10] = ['8NV', '8NVA','9NV', '9NVA', '10NV', '10NVA'];
  master_tones_list['Strawberry Blonde'][1][11] = ['8NV', '8NVA','9NV', '9NVA', '10NV', '10NVA'];
  master_tones_list['Strawberry Blonde'][1][12] = ['8NV', '8NVA','9NV', '9NVA', '10NV', '10NVA'];

  master_tones_list['True Red'][0][3] = ['5RC', '5NCR', '5NRR', '5RR'];
  master_tones_list['True Red'][0][4] = ['5RC', '5NCR', '5NRR', '5RR', '6RC', '6NCR', '6NRR', '6RR'];
  master_tones_list['True Red'][0][5] = ['5RC', '5NCR', '5NRR', '5RR', '6RC', '6NCR', '6NRR', '6RR', '7RC', '7NCR', '7NRR', '7RR'];
  master_tones_list['True Red'][0][6] = ['5RC', '5NCR', '5NRR', '5RR', '6RC', '6NCR', '6NRR', '6RR', '7RC', '7NCR', '7NRR', '7RR', '8RC', '8NCR', '8NRR', '8RR'];
  master_tones_list['True Red'][0][7] = ['5RC', '5NCR', '5NRR', '5RR', '6RC', '6NCR', '6NRR', '6RR', '7RC', '7NCR', '7NRR', '7RR', '8RC', '8NCR', '8NRR', '8RR', '9NRR'];
  master_tones_list['True Red'][0][8] = ['6RC', '6NCR', '6NRR', '6RR', '7RC', '7NCR', '7NRR', '7RR', '8RC', '8NCR', '8NRR', '8RR', '9NRR'];
  master_tones_list['True Red'][0][9] = ['7RC', '7NCR', '7NRR', '7RR', '8RC', '8NCR', '8NRR', '8RR', '9NRR'];
  master_tones_list['True Red'][0][10] = ['8RC', '8NCR', '8NRR', '8RR', '9NRR'];
  master_tones_list['True Red'][0][11] = ['9NRR'];
  master_tones_list['True Red'][1][3] = [];
  master_tones_list['True Red'][1][4] = [];
  master_tones_list['True Red'][1][5] = ['5RC', '5NCR', '5NRR', '5RR'];
  master_tones_list['True Red'][1][6] = ['5RC', '5NCR', '5NRR', '5RR', '6RC', '6NCR', '6NRR', '6RR'];
  master_tones_list['True Red'][1][7] = ['5RC', '5NCR', '5NRR', '5RR', '6RC', '6NCR', '6NRR', '6RR', '7RC', '7NCR', '7NRR', '7RR'];
  master_tones_list['True Red'][1][8] = ['6RC', '6NCR', '6NRR', '6RR', '7RC', '7NCR', '7NRR', '7RR', '8RC', '8NCR', '8NRR', '8RR'];
  master_tones_list['True Red'][1][9] = ['7RC', '7NCR', '7NRR', '7RR', '8RC', '8NCR', '8NRR', '8RR', '9NRR'];
  master_tones_list['True Red'][1][10] = ['8RC', '8NCR', '8NRR', '8RR', '9NRR'];
  master_tones_list['True Red'][1][11] = ['9NRR'];


  master_tones_list['Mahogany'][0][1] = [];
  master_tones_list['Mahogany'][1][1] = [];
  master_tones_list['Mahogany'][0][2] = ['4NGM'];
  master_tones_list['Mahogany'][1][2] = [];
  master_tones_list['Mahogany'][0][3] = ['4NGM', '5NGM'];
  master_tones_list['Mahogany'][1][3] = [];
  master_tones_list['Mahogany'][0][4] = ['4NGM', '5NGM', '6NGM'];
  master_tones_list['Mahogany'][1][4] = ['4NGM'];
  master_tones_list['Mahogany'][0][5] = ['4NGM', '5NGM', '6NGM', '7NGM'];
  master_tones_list['Mahogany'][1][5] = ['4NGM', '5NGM'];
  master_tones_list['Mahogany'][0][6] = ['4NGM', '5NGM', '6NGM', '7NGM', '8NGM'];
  master_tones_list['Mahogany'][1][6] = ['4NGM', '5NGM', '6NGM'];
  master_tones_list['Mahogany'][0][7] = ['5NGM', '6NGM', '7NGM', '8NGM'];
  master_tones_list['Mahogany'][1][7] = ['5NGM', '6NGM', '7NGM'];
  master_tones_list['Mahogany'][0][8] = ['6NGM', '7NGM', '8NGM'];
  master_tones_list['Mahogany'][1][8] = ['6NGM', '7NGM', '8NGM'];
  master_tones_list['Mahogany'][0][9] = ['7NGM', '8NGM'];
  master_tones_list['Mahogany'][1][9] = ['7NGM', '8NGM'];
  master_tones_list['Mahogany'][0][10] = ['8NGM'];
  master_tones_list['Mahogany'][1][10] = ['8NGM'];
  master_tones_list['Mahogany'][0][11] = [];
  master_tones_list['Mahogany'][1][11] = [];

  master_tones_list['Mahogany Red'][0][1] = ['3RM', '3NMG'];
  master_tones_list['Mahogany Red'][1][1] = [];
  master_tones_list['Mahogany Red'][0][2] = ['3RM', '3NMG', '4RM', '4NRM', '4NGM', '4NMG'];
  master_tones_list['Mahogany Red'][1][2] = [];
  master_tones_list['Mahogany Red'][0][3] = ['3RM', '3NMG', '4RM', '4NRM', '4NGM', '4NMG', '5RM', '5NRM', '5NGM', '5NMG'];
  master_tones_list['Mahogany Red'][1][3] = ['3RM', '3NMG'];
  master_tones_list['Mahogany Red'][0][4] = ['3RM', '3NMG', '4RM', '4NRM', '4NGM', '4NMG', '5RM', '5NRM', '5NGM', '5NMG', , '6RM', '6NRM', '6MGM', '6NMG'];
  master_tones_list['Mahogany Red'][1][4] = ['3RM', '3NMG', '4RM', '4NRM', '4NGM', '4NMG'];
  master_tones_list['Mahogany Red'][0][5] = ['3RM', '3NMG', '4RM', '4NRM', '4NGM', '4NMG', '5RM', '5NRM', '5NGM', '5NMG', '6RM', '6NRM', '6MGM', '6NMG'];
  master_tones_list['Mahogany Red'][1][5] = ['3RM', '3NMG', '4RM', '4NRM', '4NGM', '4NMG', '5RM', '5NRM', '5NGM', '5NMG'];
  master_tones_list['Mahogany Red'][0][6] = ['4RM', '4NRM', '4NGM', '4NMG', '5RM', '5NRM', '5NGM', '5NMG', '6RM', '6NRM', '6MGM', '6NMG'];
  master_tones_list['Mahogany Red'][1][6] = ['4RM', '4NRM', '4NGM', '4NMG', '5RM', '5NRM', '5NGM', '5NMG', '6RM', '6NRM', '6MGM', '6NMG'];
  master_tones_list['Mahogany Red'][0][7] = ['5RM', '5NRM', '5NGM', '5NMG', '6RM', '6NRM', '6MGM', '6NMG', '7NGM', '7NMG'];
  master_tones_list['Mahogany Red'][1][7] = ['5RM', '5NRM', '5NGM', '5NMG', '6RM', '6NRM', '6MGM', '6NMG', '7NGM', '7NMG'];
  master_tones_list['Mahogany Red'][0][8] = ['6RM', '6NRM', '6MGM', '6NMG', '7NGM', '7NMG', '8NGM'];
  master_tones_list['Mahogany Red'][1][8] = ['6RM', '6NRM', '6MGM', '6NMG', '7NGM', '7NMG', '8NGM'];
  master_tones_list['Mahogany Red'][0][9] = ['7NGM', '7NMG', '8NGM'];
  master_tones_list['Mahogany Red'][1][9] = ['7NGM', '7NMG', '8NGM'];
  master_tones_list['Mahogany Red'][0][10] = ['8NGM'];
  master_tones_list['Mahogany Red'][1][10] = ['8NGM'];

  master_tones_list['Copper'][0][2] = ['4NCG', '4NAC'];
  master_tones_list['Copper'][0][3] = ['4NCG', '4NAC', '5CC', '5NCG', '5NAC', '5RC', '5NCR'];
  master_tones_list['Copper'][0][4] = ['4NCG', '4NAC', '5CC', '5NCG', '5NAC', '5RC', '5NCR', '6CC', '6NCG', '6NAC', '6RC', '6NCR'];
  master_tones_list['Copper'][0][5] = ['4NCG', '4NAC', '5CC', '5NCG', '5NAC', '5RC', '5NCR', '6CC', '6NCG', '6NAC', '6RC', '6NCR', '7CC', '7NCG', '7RC', '7NCR'];
  master_tones_list['Copper'][0][6] = ['4NCG', '4NAC', '5CC', '5NCG', '5NAC', '5RC', '5NCR', '6CC', '6NCG', '6NAC', '6RC', '6NCR', '7CC', '7NCG', '7RC', '7NCR', '8CC', '8NCG', '8RC', '8NCR'];
  master_tones_list['Copper'][0][7] = ['5CC', '5NCG', '5NAC', '5RC', '5NCR', '6CC', '6NCG', '6NAC', '6RC', '6NCR', '7CC', '7NCG', '7RC', '7NCR', '8CC', '8NCG', '8RC', '8NCR'];
  master_tones_list['Copper'][0][8] = ['6CC', '6NCG', '6NAC', '6RC', '6NCR', '7CC', '7NCG', '7RC', '7NCR', '8CC', '8NCG', '8RC', '8NCR'];
  master_tones_list['Copper'][0][9] = ['6CC', '6NCG', '6NAC', '6RC', '6NCR', '7CC', '7NCG', '7RC', '7NCR', '8CC', '8NCG', '8RC', '8NCR'];
  master_tones_list['Copper'][0][10] = ['8CC', '8NCG', '8RC', '8NCR'];
  master_tones_list['Copper'][1][2] = [];
  master_tones_list['Copper'][1][3] = [];
  master_tones_list['Copper'][1][4] = ['4NCG', '4NAC'];
  master_tones_list['Copper'][1][5] = ['4NCG', '4NAC', '5CC', '5NCG', '5NAC', '5RC', '5NCR'];
  master_tones_list['Copper'][1][6] = ['5CC', '5NCG', '5NAC', '5RC', '5NCR','6CC', '6NCG', '6NAC', '6RC', '6NCR'];
  master_tones_list['Copper'][1][7] = ['5CC', '5NCG', '5NAC', '5RC', '5NCR', '6CC', '6NCG', '6NAC', '6RC', '6NCR', '7CC', '7NCG', '7RC', '7NCR'];
  master_tones_list['Copper'][1][8] = ['6CC', '6NCG', '6NAC', '6RC', '6NCR', '7CC', '7NCG', '7RC', '7NCR', '8CC', '8NCG', '8RC', '8NCR'];
  master_tones_list['Copper'][1][9] = ['7CC', '7NCG', '7RC', '7NCR', '8CC', '8NCG', '8RC', '8NCR'];
  master_tones_list['Copper'][1][10] = ['8CC', '8NCG', '8RC', '8NCR'];

  master_tones_list['Violet Red'][0][1] = ['3VR', '3NVV'];
  master_tones_list['Violet Red'][0][2] = ['3VR', '3NVV', '4VR', '4NVV'];
  master_tones_list['Violet Red'][0][3] = ['3VR', '3NVV', '4VR', '4NVV', '5RV', '5VR', '5NVV'];
  master_tones_list['Violet Red'][0][4] = ['3VR', '3NVV', '4VR', '4NVV', '5RV', '5VR', '5NVV', '6RV', '6NVA'];
  master_tones_list['Violet Red'][0][5] = ['3VR', '3NVV', '4VR', '4NVV', '5RV', '5VR', '5NVV', '6RV', '6NVA', '7RV'];
  master_tones_list['Violet Red'][0][6] = ['4VR', '4NVV', '5RV', '5VR', '5NVV', '6RV', '6NVA', '7RV'];
  master_tones_list['Violet Red'][0][7] = ['5RV', '5VR', '5NVV', '6RV', '6NVA', '7RV'];
  master_tones_list['Violet Red'][0][8] = ['6RV', '6NVA', '7RV'];
  master_tones_list['Violet Red'][0][9] = ['7RV'];
  master_tones_list['Violet Red'][1][1] = [];
  master_tones_list['Violet Red'][1][2] = [];
  master_tones_list['Violet Red'][1][3] = ['3VR', '3NVV'];
  master_tones_list['Violet Red'][1][4] = ['3VR', '3NVV', '4VR', '4NVV'];
  master_tones_list['Violet Red'][1][5] = ['4VR', '4NVV', '5RV', '5VR', '5NVV'];
  master_tones_list['Violet Red'][1][6] = ['4VR', '4NVV', '5RV', '5VR', '5NVV', '6RV', '6NVA'];
  master_tones_list['Violet Red'][1][7] = ['5RV', '5VR', '5NVV', '6RV', '6NVA', '7RV'];
  master_tones_list['Violet Red'][1][8] = ['6RV', '6NVA', '7RV'];
  master_tones_list['Violet Red'][1][9] = ['7RV'];


  var validUserTone = user_tone => master_tones_list[user_tone];
  var validUserToneAllOver = (user_tone, all_over) => master_tones_list[user_tone][parseInt(all_over)];
  var validUserToneLevel = (user_tone, all_over, level) => master_tones_list[user_tone][parseInt(all_over)][parseInt(level)];

  var colorFormula = (user_tone, all_over, level) => {
    let listColorsResult = [];
    let message = null;
    let validRoot = (element, level) => {
      if (element[0][level]) {
        if (element[0][level].length > 0) {
          return element[0][level];
        }
      }
      return []
    }
    if (validUserTone(user_tone) &&
      validUserToneAllOver(user_tone, all_over) &&
      validUserToneLevel(user_tone, all_over, level)) {

      if (!master_tones_list[user_tone][all_over][level]) {
        listColorsResult = validRoot();
        message = listColorsResult.length > 0 ? 'all over down to root' : null;
      } else {
        if (master_tones_list[user_tone][all_over][level].length === 0) {
          if (!master_tones_list[user_tone][0][level] || master_tones_list[user_tone][0][level].length === 0) {
            if (master_tones_list[user_tone][0][level + 1] && master_tones_list[user_tone][0][level + 1].length !== 0) {
              listColorsResult = master_tones_list[user_tone][0][level + 1];
              message = 'all over down to root && tone up +1';
            }
          } else {
            listColorsResult = master_tones_list[user_tone][0][level];
            message = 'all over down to root';
          }
        } else {
          listColorsResult = master_tones_list[user_tone][all_over][level];
        }
      }


    }
    return {
      listColorsResult,
      message
    }
  }

  var getAmountOfProduct = () => {
    var amount = 0;

    if (getInputValue("color-type") === 'All Over Color') {
      amount += 2.5;
    } else {
      amount += 1;
    }
    if (getInputValue("last-coloring")) {
      var lastColoringInt = parseInt(getInputValue("last-coloring").substr(0, 1));
      if (!isNaN(lastColoringInt)) {
        amount = amount + lastColoringInt * 0.25;
      }
    }
    var hairLength = getInputValue("hair-length");
    switch (hairLength) {
      case 'Shoulder Length':
        amount = amount + 1;
        break;
      case 'Long':
        amount = amount + 1.5;
        break;
      case 'Very Long':
        amount = amount + 2;
        break;

      default:
        break;
    }
    var texture = getInputValue("natural-texture");
    switch (texture) {
      case 'Coarse':
      case 'Very Coarse':
        amount = amount + 1;
        break;

      default:
        break;
    }

    return amount;
  }

  var getInputValue = inputName => jQuery(`input[name='${inputName}']:checked`).val();
  var getSelectValue = selectName => jQuery(selectName).val();

  var getFormInfo = () => {
    var newState = {
      "natural-color-shades": getInputValue("natural-color-shades"),
      "natural-color-level": getInputValue("natural-color-level"),
      "natural-color-shade": getInputValue("natural-color-shade"),
      "natural-color-swatches": getInputValue("natural-color-swatches"),
      "current-color-shades": getInputValue("current-color-shades"),
      "current-color-level": getInputValue("current-color-level"),
      "current-color-shade": getInputValue("natural-color-shade"),
      "current-color-swatches": getInputValue("current-color-swatches"),
      "color-shades": getInputValue("color-shades"),
      "color-level": getInputValue("color-level"),
      "color-shade": getInputValue("color-shade"),
      "color-swatches": getInputValue("color-swatches"),
      "brand-name": getSelectValue("#brand-name"),
      "color-type": getInputValue("color-type"),
      "recent-services": getInputValue("recent-services"),
      "last-coloring": getInputValue("last-coloring"),
      "hair-condition": getInputValue("hair-condition"),
      "hair-length": getInputValue("hair-length"),
      "much-gray": getInputValue("much-gray"),
      "hair-resistant": getInputValue("hair-resistant"),
      "natural-texture": getInputValue("natural-texture"),
      "user-name": getSelectValue("#user-name"),
      "mobile-number": getSelectValue("#user-phone"),
      "email": getSelectValue("#user-email"),
      "keratin": getKeratin() ? 'Yes' : 'No',
      "porous": getPorous() ? 'Yes' : 'No',
      "recent_services": JSON.stringify(getRecentServices()),
      "amount": getAmountOfProduct() + '(oz)',
      "last-coloring": getInputValue('last-coloring')
    }


    state = newState;
    console.log('---------', {
      state
    });

    let output = jQuery('<ul/>', {
      'html': function() {
        let list = '';
        let keys = Object.keys(state);
        keys.forEach(item => {
          list = `${list} <li>${item}: ${state[item]}</li>`
        });
        return list;
      },
    });

    jQuery('#json_output').html(output);
    return state;
  }

  /* /color formula */
  var bg_img = <?php echo json_encode($images); ?>;
  var current_fs, next_fs, previous_fs; //fieldsets
  var left, opacity, scale; //fieldset properties which we will animate
  var animating, current_color, next_color, selected_color; //flag to prevent quick multi-click glitches


  //var pageURL = window.location.href;

  jQuery(".next").click(function() {
    getFormInfo();

    current_fs = jQuery(this).parent();
    next_fs = jQuery(this).parent().next();

    //activate next step on progressbar using the index of next_fs
    jQuery("#progressbar li").eq(jQuery("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.fadeIn();
    current_fs.fadeOut();

  });

  jQuery(".previous").click(function() {

    current_fs = jQuery(this).parent();
    previous_fs = jQuery(this).parent().prev();

    //de-activate current step on progressbar
    jQuery("#progressbar li").eq(jQuery("fieldset").index(current_fs)).removeClass("active");

    //show the next fieldset
    previous_fs.fadeIn();
    current_fs.fadeOut()
  });

  jQuery(".restart").click(function() {
    if (confirm('You can start over at any time, just remember this will remove your answers!')) {
      location.reload(true);
    }
  });

  jQuery("fieldset .previous").click(function() {
    var find_input = jQuery(this).parent('fieldset').prev().prop("tagName");
    var check = find_input + ' input:checked';
    jQuery(this).parent('fieldset').prev().find(".next").css({
      "visibility": "visible",
      "width": "100px",
      "padding": "10px 5px",
      "height": "auto"
    });
    console.log(check);

  });
  /*****ON CLICK PREVIOUR MAKE ALL PREV DISABLED*****/
  /*jQuery("fieldset .previous").click(function(){
  	jQuery("fieldset .previous").attr('disabled',true);
  });*/

  var getLevel = string => {
    if (!string) {
      string = getInputValue('natural-color-level');
    }
    var tmp = string.split("");
    var map = tmp.map(function(current) {
      if (!isNaN(parseInt(current)) || current === '.') {
        return current;
      }
    });

    var numbers = map.filter(function(value) {
      return value != undefined;
    });
    if (numbers.join("") !== '') {
      return parseFloat(numbers.join("")).toFixed(1);
    } else {
      return 0;
    }
  }

  var validLevel = () => {
    if (getInputValue('color-type') === 'Root Touch Up') {
      return getLevel(getInputValue('natural-color-level'))
    } else {
      return getLevel(getInputValue('current-color-level'))
    }
  }

  var showNextBtn = () => {
    jQuery('.currently-color').parent().addClass('brand-section');
  }

  var goToStepUp = (from, to) => {
    jQuery(`.screen-step${from}`).slideUp();
    current_fs = jQuery(`.screen-step${to - 1}`);
    next_fs = current_fs.next();
    previous_fs = current_fs.prev();
    current_fs.slideUp();
    next_fs.slideDown();
    for (let index = 0; index < to; index++) {
      jQuery("#progressbar li").eq(index).addClass("active");
    }
  }
  var goToStepDown = (from, to) => {
    jQuery(`.screen-step${from}`).slideUp();
    current_fs = jQuery(`.screen-step${to + 1}`);
    next_fs = current_fs.next();
    previous_fs = current_fs.prev();
    current_fs.slideUp();
    previous_fs.slideDown();
    for (let index = 0; index < to; index++) {
      jQuery("#progressbar li").eq(index).addClass("active");
    }
  }

  var stopQuiz = () => {
    console.log('------------stop quiz, alert colorist');
  }
  var alertColorist = () => {
    console.log('------------ alert colorist');
  }
  var alertStaff = () => {
    console.log('------------alert staff');
  }
  var keratinFormula = () => {
    console.log('------------ keratinFormula alert staff');
  }
  var grayResistance = () => {
    console.log('------------ grayResistance formula alert staff');
  }

  var getFormulaResult = (target, inputName, allOver, level, shade) => {
    let options = colorFormula(shade, allOver, level);
    if (options.listColorsResult) {
      jQuery.each(options.listColorsResult, function(index, value) {
        var img_name = this + "_edit.png";
        image_name = this;
        if (bg_img.indexOf(img_name) !== -1) {
          var url = base + '/wp-content/uploads/color_swatches/' + img_name;
          jQuery(`${target} .listing ul`).append(`
          <li>
            <label class="no-hover">
              <input type="radio" name="${inputName}-color-swatches" title="${image_name}" value="${value}">
              <span class="input-label" style="background-image:url(${url})!important;">${value}</span>
            </label>
          </li>`);
        }
      });
      jQuery(`${target} .listing ul li:first-child`).addClass("active").click();
    }
  }

  /* ---------------------------------------step1 */
  /* color-type */
  jQuery("input[name='color-type']").click(function() {
    var colorType = jQuery(this).val();
    if (colorType === 'Root Touch Up') {
      goToStepUp(1, 4);
    } else {
      jQuery(this).parents('fieldset').find(".next").click();
    }
  });

  /* ---------------------------------------step2 */
  /* currently-coloring / virgin */
  jQuery("input[name='currently-coloring']").click(function() {
    var noCurrentColor = jQuery('.no-current-color');
    showNextBtn();
    noCurrentColor.addClass('hidden');
    var noCurrentColorValue = jQuery(this).val();
    switch (noCurrentColorValue) {
      case 'All Over':
        goToStepUp(2, 3);
        break;
      case 'All Over Lightening':
      case 'Henna':
        alertColorist();
        stopQuiz();
        break;
      case 'Color & High Lights':
      case 'High Lights Only':
        goToStepUp(2, 3);
        break;
      case 'Virgin':
        noCurrentColor.removeClass('hidden');
        break;

      default:
        break;
    }
  });
  /* ---------step 2 / virgin / yes || not*/
  jQuery('.have-some-color').click(function() {
    goToStepDown(2, 1);
    jQuery("#progressbar li").removeClass("active");
    jQuery("#progressbar li").eq(0).addClass("active");
  })
  jQuery('.no-color-sure').click(function() {
    goToStepUp(2, 5);
  })

  /* ------------------------- step 3 */
  jQuery("input[name = 'recent-services']").click(function() {
    var service = jQuery(this).val();
    if (service === 'Keratin') {
      alertStaff();
    }
  })

  /* ------------------------- step 4 */
  jQuery("input[name = 'last-coloring']").click(function() {
    jQuery(this).parents('fieldset').find(".next").click();
  })

  /* ------------------------- step 5 */
  jQuery("input[name='hair-condition']").click(function() {
    var condition = jQuery(this).val();
    var damaged = jQuery('.hair-condition-damaged');
    showNextBtn();
    damaged.addClass('hidden');
    switch (condition) {
      case 'Healthy':
        jQuery(this).parents('fieldset').find(".next").click();
        break;
      case 'Porous':
        alertStaff();
        jQuery(this).parents('fieldset').find(".next").click();
        break;
      case 'Damaged':
        damaged.removeClass('hidden');
        break;

      default:
        break;
    }
  });
  /* ------------------------- step 5 sub Dry || Breakage*/
  jQuery('.hair-condition-dry').click(function() {
    goToStepUp(5, 6);
  })
  jQuery('.hair-condition-breakage').click(function() {
    alertStaff();
    goToStepUp(5, 6);
  })

  /* ------------------------- step 6 */
  jQuery("input[name = 'hair-length']").click(function() {
    jQuery(this).parents('fieldset').find(".next").click();
    jQuery('screen-step8').fadeIn();
  })

  /* ------------------------- step 7 */
  jQuery("input[name = 'much-gray']").click(function() {
    var gray = jQuery(this).val();
    switch (gray) {
      case 'None':
      case 'Few':
        break;
      case 'Lots':
      case 'All':
        grayResistance();
        break;

      default:
        break;
    }
    jQuery(this).parents('fieldset').find(".next").click();
  })

  /* ------------------------- step 8 */
  jQuery("input[name='hair-resistant']").click(function() {
    var resistant = jQuery(this).val();
    if (resistant === 'Yes') {
      grayResistance();
    }
    jQuery(this).parents('fieldset').find(".next").click();
  })

  /* ------------------------- step 9 */
  jQuery("input[name='natural-texture']").click(function() {
    jQuery('.screen-step10').find('.natural-label').fadeOut();
    jQuery('.screen-step10').find('.current-label').fadeOut();
    var root = getInputValue('color-type');
    if (root === "Root Touch Up") {
      jQuery('.screen-step10').find('.natural-label').fadeIn();
    } else {
      jQuery('.screen-step10').find('.current-label').fadeIn();
    }
    jQuery(this).parents('fieldset').find(".next").click();
  })

  /* ------------------------- step 10 */
  jQuery("input[name='current-color-shades'], input[name='natural-color-shades']").click(function() {
    var shade = jQuery(this).val();
    jQuery('.screen-step11').find('label').fadeOut();
    jQuery('.screen-step11').find('.' + shade).fadeIn();
    jQuery('.screen-step10').fadeOut();
    jQuery('.screen-step11').fadeIn();
    jQuery('.screen-step11').find('.hidden').removeClass('hidden');
    jQuery(this).parents('fieldset').find(".next").click();
  })

  /* ------------------------- step 11 */
  jQuery("input[name='current-color-level'], input[name='natural-color-level']").click(function() {
    jQuery('.screen-step11').fadeOut();
    jQuery('.screen-step12').fadeIn();
    jQuery('.screen-step12').find('.hidden').removeClass('hidden');
    jQuery(this).parents('fieldset').find(".next").click();
  })

  /* ------------------------- step 12 */
  jQuery("input[name='color-shades']").click(function() {
    jQuery('.color-error-level').fadeOut();
    let color = jQuery(this).val();
    let level = parseFloat(validLevel());
    let minLevel = parseFloat(level - 2);
    let maxLevel = parseFloat(level);
    if (getInputValue('color-type') !== 'All Over Color') {
      maxLevel = parseFloat(level + 2);
    }
    jQuery('.screen-step13').find('label').each(function() {
      jQuery(this).fadeOut();
      if (minLevel <= parseFloat(jQuery(this).find('input').val()) && parseFloat(jQuery(this).find('input').val()) <= maxLevel) {
        jQuery(this).fadeIn();
      }
    });
    setTimeout(() => {
      if (!(jQuery('.screen-step13').find('.' + color).filter(':visible').length > 0)) {
        jQuery('.color-error-level').fadeIn();
      }
    }, 1000);
  })

  /* ------------------------- step 15 */
  /* ------------------------- step 16 */
  /* ------------------------- step 17 */
  /* ------------------------- step 21 */
  jQuery(document).on('click', "input[name='color-swatches']", function() {
    jQuery(this).parents('fieldset').find('.next').removeClass('hidden')
  })

  /****COLOR SHADE BLACK/Brown/Blonde/RED******/
  jQuery(".color-shades .first").click(function() {
    jQuery(".black-options.no-count").removeClass('hidden');
    jQuery(" .no-count.new-color:not(.black-options)").addClass('hidden');
  });

  jQuery(".color-shades .brown").click(function() {
    jQuery(".brown-options").removeClass('hidden');
    jQuery(" .no-count.new-color:not(.brown-options)").addClass('hidden');
  });

  jQuery(".color-shades .blonde").click(function() {
    jQuery(".blonde-options").removeClass('hidden');
    jQuery(".no-count.new-color:not(.blonde-options)").addClass('hidden');
  });

  jQuery(".color-shades .last").click(function() {
    jQuery(".red-options").removeClass('hidden');
    jQuery(" .no-count.new-color:not(.red-options)").addClass('hidden');
  });

  /****ON CHANGE COLOR SHADE****/
  var level_val;

  jQuery(".new-color input[name='color-shade'],.new-color input[name='color-level']").on("change", function() {
    jQuery('.color-error-swatch-level').fadeOut(100);
    jQuery(".multi-swatches ul li,.selected-swatch img,.color-preview img,.selected-color").remove();
    level_val = jQuery(".new-color input[name='color-level']:checked").val();
    var shade_val = jQuery(".new-color input[name='color-shade']:checked").val();
    var image_name;
    // var options = colorFormula(user_tone, all_over, level);
    let shade = getInputValue('color-shade');
    let over = getInputValue('color-type') === "All Over Color" ? 1 : 0;
    let level = getInputValue('color-level');
    let options = colorFormula(shade, over, level);
    //console.log(color_swatches[shade_val][level_val]);
    jQuery(".swatches div.multi-swatches h4").text(shade_val + " Color Swatches");
    if (options.listColorsResult.length === 0) {
      jQuery('.color-error-swatch-level').css({
        'display': 'inline-block'
      });
    } else {
      jQuery.each(options.listColorsResult, function(index, value) {
        var img_name = this + "_edit.png";
        image_name = this;
        //  console.log(img_name);
        if (bg_img.indexOf(img_name) !== -1) {
          var url = base + '/wp-content/uploads/color_swatches/' + img_name;
          jQuery(".swatches div.multi-swatches ul").append('<li><label class="no-hover"><input type="radio" name="color-swatches" title="' + image_name + '" value="' + value + '"><span class="input-label" style="background-image:url(' + url + ')!important;">' + value + '</span></label></li>');
        }
      });
      jQuery(".multi-swatches ul li:first-child").addClass("active");
      jQuery(".multi-swatches ul li.active input").click();
      var color = jQuery(".multi-swatches ul li.active input").attr("title");
      var active_color = jQuery(".multi-swatches ul li.active span").css('background-image');
      active_color = active_color.replace('url(', '').replace(')', '').replace(/\"/gi, "");
      jQuery(".selected-swatch").append("<p class='selected-color'>" + color + "</p><img src='" + active_color + "'>");
      jQuery(".preview .edit-color .color-preview").append("<p class='selected-color'>" + color + "</p><img src='" + active_color + "'>");
    }
  });

  /*****ON CHANGING/CLICKING SWATCHES CHOICE*******/
  jQuery(document).on('click', ".swatches .multi-swatches input", function() {
    jQuery(".preview .edit-color .color-listing ul").remove();
    jQuery(".multi-swatches ul li,.color-listing ul li").removeClass("active");
    jQuery(this).parent().parent().addClass("active");
    current_color = jQuery(".swatch-selection .multi-swatches ul li.active input").val();
    var color = jQuery(".multi-swatches ul li.active input").attr("title");
    var active_color = jQuery(".multi-swatches ul li.active span").css('background-image');
    active_color = active_color.replace('url(', '').replace(')', '').replace(/\"/gi, "");
    jQuery(".selected-swatch p.selected-color,.color-preview p.selected-color").text(color);
    jQuery(".selected-swatch img,.color-preview img").attr("src", active_color);
    if (jQuery('.multi-swatches ul li').length > 0) {
      jQuery(".preview .edit-color h6").text("Based on the answers, this is the suggested color swatch. Please look at the other color swatches and if you find one that you like better,Please select that ");
      jQuery(".preview .col-sm-6").css("width", "50%");
      jQuery(".preview .edit-color .color-listing").append(jQuery(".swatch-selection .multi-swatches .listing").html());
    }
  });

  /****PREVIEW SWATCHES*****/
  jQuery(document).on('click', ".preview .multi-swatches .color-listing input", function() {
    jQuery(".color-listing ul li").removeClass("active");
    jQuery(this).parent().parent().addClass("active");
    var color = jQuery(".color-listing ul li.active input").attr("title");
    var active_color = jQuery(".color-listing ul li.active span").css('background-image');
    active_color = active_color.replace('url(', '').replace(')', '').replace(/\"/gi, "");
    jQuery(".color-preview img,.selected-swatch img").attr("src", active_color);
    jQuery(".color-preview p.selected-color").text(color);
  });

  //*****COLOR TYPE 'ROOT TOUCH UP' OR 'ALL OVER COLOR'******/

  /*******If input is checked CLICK NEXT Button*******/
  jQuery(".color-shades input[name='color-shades'],input[name='color-shade'],input[name='hair-length'],input[name='color-level'],input[name='natural-shade']").click(function() {
    jQuery(this).parents('fieldset').find(".next").click();
  });

  /****R E C E N T   S E R V I C E S******/
  jQuery(".services input.last").click(function() {
    jQuery(".services input:not(.last)").prop('checked', false);
    jQuery(this).parents('fieldset').find(".next").click();
  });

  var humanDate = () => {
    var d = new Date();
    var date = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    return month + "/" + date + "/" + year;
  }

  var getFormula = () => {
    if (jQuery(".preview ul li.active input").val() != current_color) {
      current_color = jQuery(".preview ul li.active input").val();
    }
    return current_color;
  }

  var getRecentServices = () => {
    var services = '';
    jQuery.each(jQuery("input[name='recent-services']:checked"), function(index) {
      if (index === 0) {
        services = jQuery(this).val();
      } else {
        services = services + ', ' + jQuery(this).val();
      }
    });

    return services;
  }

  var getKeratin = () => {
    var keratin = false;
    jQuery.each(jQuery("input[name='recent-services']:checked"), function() {
      if (jQuery(this).val() === 'Keratin') {
        keratin = true;
      }
    });
    return keratin;
  }

  var getPorous = () => jQuery("input[name='hair-condition']:checked").val() === 'Porous';

  var getHairCondition = () => {
    var hairCondition = getInputValue("hair-condition");
    var hairConditionDamaged = hairCondition === 'Damaged' ? ' / ' + getInputValue("hair-condition-damaged") : '';
    return hairCondition + hairConditionDamaged;
  }

  /***Questionnaire Summary****/
  jQuery(".show-current-data").click(function() {
    var dying_service, hair_type, keratin, porous;

    if (jQuery("input[name='hair-condition']:checked").val() === 'Porous') {
      porous = "POROUS ALERT!";
      jQuery(".porous").css("color", "red");
      jQuery(".warning").append("<img src='<?php echo get_stylesheet_directory_uri(); ?>/images/danger.png'>");
    } else {
      porous = "Not Porous";
      jQuery(".warning").append("<span class='no-warning' style='font-size:18px;'>No Warnings</span>");
    }

    if (jQuery("input[name='hair-resistant']:checked").val() == 'Yes') {
      resistant = "Resistant";
      //  hair_type="Resistant";
    } else {
      resistant = "Not Resistant";
    }

    if (jQuery("input[name='color-type']:checked").val() == 'Root Touch Up') {
      hair_type = "Virgin";
    }

    jQuery(".current_date").text(humanDate());
    jQuery(".formula").text(getFormula());
    jQuery(".kit_choice").text(getInputValue('color-type'));
    jQuery(".hair_type").text(getInputValue('color-type'));
    jQuery(".resistant").text('Hair Resistance: ' + getInputValue('hair-resistant'));
    jQuery(".recent_services_arr").text(JSON.stringify(getRecentServices()));
    jQuery(".keratin").text(getKeratin() ? 'Yes' : 'No');
    jQuery(".porous").text(porous);
    jQuery(".clr_amt").text(getAmountOfProduct() + '(oz)');
  });

  var textFieldsValidation = () => {
    console.log('-----textFieldsValidation');
  }

  /*******F O R M   S E R V I C E S*****/
  jQuery("#user-fields").click(function(e) {
    let error = false;
    e.preventDefault();
    textFieldsValidation();
    if (!error) {
      jQuery(this).parents('fieldset').find(".next").click();
      jQuery('.screen-step16').fadeOut();
      jQuery('.screen-step17').fadeIn();
      jQuery(".user-name").text(getSelectValue('#user-name'));
      jQuery(".user-phone").text(getSelectValue('#user-phone'));
      jQuery(".user-email").text(getSelectValue('#user-email'));
    }
    getFormInfo();
  });

  jQuery('.custom-tooltips .got-custom-tooltip').on('mouseover', function() {
    jQuery(this).parent().find('.custom-tooltip').fadeIn();
  })

  jQuery('.custom-tooltips .got-custom-tooltip').on('mouseleave', function() {
    jQuery('.custom-tooltip').fadeOut()
  })

  jQuery('.img-tooltip').on('click', function() {
    jQuery(this).parent().find('.custom-tooltip').toggleClass('active');
  })
</script>
<style type="text/css">
  .restart {
    position: absolute;
    right: 40px;
    z-index: 1;
    top: -20px;
  }

  .custom-tooltips {
    position: relative;
  }

  .custom-tooltip {
    border: #ccc solid;
    border-radius: 5px;
    padding: 9px;
    font-size: 12px;
    color: white;
    background-color: #024c63;
    display: none;
    max-width: 300px;
    text-align: left;
    transition: ease-in-out all 0.5;
    margin-left: 47%;
    transform: translate(-50%);
    opacity: 0.9;
    position: absolute;
    left: 0;
    top: 50px;
    z-index: 100;
  }

  .custom-tooltip.active {
    display: block !important;
  }

  .img-tooltip {
    width: 25px;
    height: 25px;
    margin-left: -30px;
    margin-top: -3px;
    cursor: pointer;
  }

  #questionaire-form .listing-centered .input-label {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  #questionaire-form input[type=text],
  #questionaire-form input[type=tel],
  #questionaire-form input[type=email],
  #questionaire-form select {
    font-family: century-gothic !important;
    border: 1px solid #024c63 !important;
    border-radius: 0 !important;
    box-shadow: none !important;
    background: #ddd;
    height: 50px;
    margin-bottom: 14px;
  }

  #json_output ul {
    display: flex;
    align-content: center;
    flex-wrap: wrap;
    list-style: none;
    padding: 0;
  }

  #json_output ul li {
    border: 1px solid #024c63 !important;
    background: #ccc;
    font-size: 12px;
    padding: 5px;
    border-radius: 5px;
    margin: 2px;
  }

  .summary-field {
    padding: 10px !important;
    border: 1px solid !important;
    margin-bottom: 15px !important;
    text-align: left !important;
    background-color: white !important;
    font-size: 16px !important;
    box-shadow: none !important;
  }

  .flex-centered {
    display: flex !important;
    justify-content: center !important;
    flex-wrap: wrap !important;
  }

  #questionaire-form .q-10 .input-label {
    width: 160px !important;
  }
</style>