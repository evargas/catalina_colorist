<?php /* Template Name: Articles 
	Template Post Type: post*/ ?>

<?php get_header();
query_posts(array(
   'post_type' => 'inspiration'
)); ?>
<section class="hero-section" id="hero-section" role="banner"><div class="wrap"><h1 class="archive-title">Inspiration</h1></div></section>
<div id="inspiration-png">
<div class="container">
	<div  id="contain-mgmt" class="row">
<?php
while (have_posts()) : the_post(); ?>
	
	<div class="col-sm-4 equal-height com-img">
	<?php the_post_thumbnail('medium'); ?> 
<div class="entry-contain">	
<p class="entry-metas">
	<time class="entry-time"><?php the_time( 'F j, Y' ); ?></time>
</p>
	<!-- <?php add_image_size( 'single-post-thumbnail', 590, 180 );?> -->
<h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>

<p><?php the_excerpt(); ?></p>
			
			<div class="categories-mgmt">
 			<?php $categories = get_the_category();
 			$category_id = $categories[0]->cat_ID;
			$category_name = $categories[0]->cat_name;
			$category_folder = get_stylesheet_directory_uri();
				 echo '<h4 class="link"><img class="ls" width="20" height="20" src="'.$category_folder.'/images/folder-svg.png"/><a href="'.$category_name.'">'.$category_name .'</a></h4>';
				?>
			</div>

</div>
</div>
<?php endwhile;
?>
</div>
</div>
</div>
<?php
get_footer();
?>