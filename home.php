<?php
/**
 * Page template
 *
 * @package WordPress
 * @subpackage Visual Composer Starter
 * @since Visual Composer Starter 1.0
 */

get_header(); ?>

<section class="hero-section" id="hero-section" role="banner"><div class="wrap"><h1 class="archive-title">How To</h1></div></section>
<div id="home-mgmt">
	<div class="container">
		<div  id="home-con-mgmt" class="row">
			<?php
			while (have_posts()) : the_post(); ?>

			<div class="col-sm-4 equal-height com-img">
				<?php the_post_thumbnail('medium'); ?> 
				<div class="entry-contain">	
					<p class="entry-metas">
					<time class="entry-time"><?php the_time( 'F j, Y' ); ?></time>
					</p>
					
					<h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>

					<p><?php the_excerpt(); ?></p>

				<div class="categories-mgmt">		
			
			<?php $categories = get_the_category();
			$category_id = $categories[0]->cat_ID;
			$category_name = $categories[0]->cat_name;
			$category_folder = get_template_directory_uri();
			//echo ($a);
			
				echo '<h4 class="link"><img class="ls" width="20" height="20" src="'.$category_folder.'/images/folder-svg.png"/><a href="'.$category_name.'">'.$category_name .'</a></h4>';?>
			</div>

				</div>
			</div>
			<?php endwhile;
			?>
			</div>
		</div>
	</div>

<?php get_footer();
