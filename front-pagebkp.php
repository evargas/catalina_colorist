<?php
/**
 * Page template
 *
 * @package WordPress
 * @subpackage Visual Composer Starter
 * @since Visual Composer Starter 1.0
 */

get_header(); ?>

<?php
if(wp_is_mobile()){
      /*  echo do_shortcode('[woocommerce_my_account]');
    	// $mobile_url = 'https://www.softgetix.com/projects/color/my-account';
    	// header('Location: https://www.softgetix.com/projects/color/my-account');
		// exit();
    	echo '<script>
    	window.location.href = "'. site_url("/my-account"); echo '";
    	</script> ';*/

    	$schedule_appointment = get_field('schedule_appointment');
	if($schedule_appointment ==''){
		$schedule_appointment = 'javascript:void()';
	}

	$reschedule_appointment = get_field('reschedule_appointment');
	if($reschedule_appointment ==''){
		$reschedule_appointment = 'javascript:void()';
	}

	$color_quiz = get_field('color_quiz');
	if($color_quiz ==''){
		$color_quiz = 'javascript:void()';
	}

	$phone_number = get_field('phone_number');
	if($phone_number ==''){
		$phone_number = '123456789()';
	}

	$contact = get_field('contact');
	if($contact ==''){
		$contact = 'javascript:void()';
	}
	
?>
    	<a href="tel:<?php echo $phone_number;?>" class="call header-call-widget"> <i class="fa fa-phone"></i> </a>

	<div class="user-header"> <i class="fa fa-user-circle-o"></i> Hello, <b><?php echo $current_user->display_name; ?></b> </div>
    <div id="account"> 

    	<div id="my-account">
    		<h6 class="user-menu-title">My Account</h6>

    		<ul class="user-menu-list">
    		  <li>
    			<a href="<?php printf(__( '%1$s'), esc_url( wc_get_endpoint_url( 'my-account' ) ) ); ?>">
    			  <i class="fa fa-shopping-basket"></i>
    			  <span class="menu-txt">Order History</span>
    			</a>
    		  </li>

    		  <li>
    		  	<!-- <a href="javascript:void()"> -->
    		  		<a href="<?php printf(__( '%1$s'), esc_url( wc_get_endpoint_url( 'my-account' ) ) ); ?>">
    		  		<i class="fa fa-paint-brush"></i>
    		  		<span class="menu-txt">Color Choice</span>
    		  	</a>
    		  </li>

    		  <li class="dropdown-item">
    		  	<!-- <a href="javascript:void()"> -->
    		  		<a href="<?php printf(__( '%1$s'), esc_url( wc_get_endpoint_url( 'my-account' ) ) ); ?>">
    		  	  <i class="fa fa-calendar"></i>
    		  	  <span class="menu-txt">Appointment</span>
    		  	</a>
<!-- 	    		  	<div class="dropdown">
    		  	  <ul class="user-menu-list">
    		  	  	<li>
    		  	  	  <a href="<?php echo $schedule_appointment;?>">
    		  	  	    <i class="fa fa-calendar-check"></i>
    		  	  	    <span class="menu-txt">Schedule Appointment</span>
    		  	  	  </a>
    		  	  	</li>
    		  	  	<li>
    		  	  	  <a href="<?php echo $reschedule_appointment;?>">
    		  	  	    <i class="fa fa-calendar-check"></i>
    		  	  	    <span class="menu-txt">Reschedule Appointment</span>
    		  	  	  </a>
    		  	  	</li>
    		  	  </ul>
    		  	</div> -->
    			</li>
    		</ul>
    	</div> <!-- my-account -->

    	<div class="account-tools">
    		<h6 class="user-menu-title">Account Tools</h6>
    		<a class="btn btn-primary btn-ovl" href="<?php echo $color_quiz;?>">Take the ColorQuiz</a>
    	</div>

    	<div class="contact-color-only">
    		<h6 class="user-menu-title">Contact ColorOnly</h6>
    		<a href="tel:<?php echo $phone_number;?>" class="btn btn-o tel"> <i class="fa fa-phone"></i> <?php echo $phone_number;?></a>
    		<br>
    		<a href="<?php echo $contact;?>" class="btn btn-o"> <i class="fa fa-envelope-o"></i> Contact us</a>
    	</div>

    	<a href="<?php echo wc_logout_url(); ?>" style="position:fixed; top:-200px;" class="logout_url">Logout</a>
    	
    </div>
<?php
       } else {
?>

	<div class="<?php echo esc_attr( visualcomposerstarter_get_content_container_class() ); ?>">
		<div class="content-wrapper">
			<div class="row">
				<div class="<?php echo esc_attr( visualcomposerstarter_get_maincontent_block_class() ); ?>">
					<div class="main-content">
						<?php
						// Start the loop.
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/content', 'page' );
						endwhile; // End the loop.
						?>
					</div><!--.main-content-->
				</div><!--.<?php echo esc_html( visualcomposerstarter_get_maincontent_block_class() ); ?>-->

				<?php if ( visualcomposerstarter_get_sidebar_class() ) : ?>
					<?php get_sidebar(); ?>
				<?php endif; ?>

			</div><!--.row-->
		</div><!--.content-wrapper-->
	</div><!--.<?php echo esc_html( visualcomposerstarter_get_content_container_class() ); ?>-->

<?php } ?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.top-head-second .login-logout a').attr('href', '<?php echo wc_logout_url(); ?>');
	});
</script>

<style type="text/css">
		body.logged-in .top-head-second ul#menu-header_1 .login-logout a {font-size:0 !important;}
		body.logged-in .top-head-second .login-logout a {font-size:0 !important;}
		body.logged-in .top-head-second .login-logout a:after {content:'Logout'; font-size:15px;}
		#main-menu ul li.current-menu-item>a {color:#4d5968;}
		@media (min-width:768px){
			.header-call-widget {display:none;}
		}

		@media (max-width:767px){

			body.logged-in .top-head-second .login-logout a:after {content:'Logout'; font-size:18px;}

			#main-menu {left:-270px;}
			#main-menu.open {margin-left:270px;}
			.navbar-brand {display:inline-block; width:auto; float:none; padding:0;}
			.navbar-brand img {width:175px;}
			.navbar-toggle {display:inline-block !important; float:left; margin:0 10px 0 -10px !important;}

			.header-call-widget {position:absolute; top:-64px; right:15px; color:#fff; z-index:20; width:35px; line-height:33px; border-radius:35px; border:1px solid !important; text-align:center;}

			#main-menu {background-color:#ddf5fb; box-shadow:0 2px 5px rgba(0, 0, 0, .1);}

			footer {display:none;}
			body {background:#ddf5fb;}
			#header .navbar .navbar-wrapper {background:#0085a6; box-shadow:0 2px 5px rgba(0, 0, 0, .1);}
			.navbar-brand {padding:00;}
			.navbar-brand img {filter:invert(0.5) brightness(2);}
			.navbar-toggle .icon-bar {background:#fff;}

			#account {padding:15px;}
			.user-header {background:#0085a6; padding:10px 15px; color:#fff;}
			.user-menu-title {font-weight:700; color:#0085a6; margin:20px 0;}
			ul {list-style:none;}
			.user-menu-list li {position:relative; padding:0 0 10px 35px;}
			.user-menu-list li:last-child {padding-bottom:0;}
			.user-menu-list .fa {position:absolute; left:0; top:6px;}
			.account-tools {margin:25px -15px; border:solid #6ebdd0; border-width:3px 0; padding:0 15px 25px;}
			.account-tools .btn {background:#0085a6; border:1px solid #0085a6; color:#fff; border-radius:25px; font-size: 16px; padding:7px 14px;}
			.account-tools .btn:hover {background:#ddf5fb; color:#0085a6;}

			.contact-color-only .btn {color:#0085a6; border:1px solid #0085a6 !important; border-radius:25px; font-size:16px;  padding:7px 14px 7px 0;}
			.contact-color-only .btn .fa {border:1px solid #0085a6; border-radius:25px; width:38px; height:38px; line-height:38px; margin:-7px -1px; margin-right:4px;}
			.contact-color-only .btn.tel {margin-bottom:14px;}
		}
</style>

<?php get_footer();
