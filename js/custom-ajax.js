var getQuizProducts = (base, searchProduct, quantity) => {
  jQuery
    .ajax({
      url: quiz_vars.ajaxurl,
      type: "post",
      dataType: "json",
      data: {
        action: "get_quiz_products",
        searchProduct,
      },
    })
    .done(function (products) {
      window.location.href = `${base}/?add-to-cart=${products[0].id}&quantity=${quantity}`;
    })
    .fail(function (e) {
      console.log("Upps!!!, The service is not working!");
    });
};

var findFormulaProducts = (level, base_color) => {
  jQuery
    .ajax({
      url: quiz_vars.ajaxurl,
      type: "post",
      dataType: "json",
      data: {
        action: "find_swatches_products",
        level,
        base_color,
      },
    })
    .done(function (swatches) {
      jQuery(".swatches div.multi-swatches ul").html("");
      if (swatches.length > 0){
        jQuery.each(swatches, function (index, value) {
          var img_name = this + "_edit.png";
          image_name = this;
          if (quiz_vars.images_ajax.indexOf(img_name) !== -1) {
            var url = `${quiz_vars.base_ajax_url}/wp-content/uploads/color_swatches/${img_name}`;
            jQuery(".swatches div.multi-swatches ul").append(
              `<li>
              <label class="no-hover">
                <input type="radio" name="color-swatches" title="${image_name}" value="${value}">
                <span class="input-label" style="background-image:url(${url})!important;">${value}</span>
              </label>
            </li>`
            );
          }
        });
      }else{
        jQuery(".swatches div.multi-swatches ul").html("<li>No results!, you must go back.</li>");
      }
    })
    .fail(function (e) {
      console.log("-------------nopp swatches", { e });
    });
};
