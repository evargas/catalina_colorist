<?php

session_start();

/**
 * Template Name:new-know Template
 *
 * 
 */

get_header(); ?>
<?php
$master_tones_list = array();
$master_tones_list['Natural Black'] = array();
$master_tones_list['Violet Black'] = array();
$master_tones_list['Blue Black'] = array();
$master_tones_list['Mahogany Brown'] = array();
$master_tones_list['Cool Brown'] = array();
$master_tones_list['Natural Brown'] = array();
$master_tones_list['Golden Brown'] = array();
$master_tones_list['True Brown'] = array();
$master_tones_list['Strawberry Blonde'] = array();
$master_tones_list['Cool Blonde'] = array();
$master_tones_list['Golden Blonde'] = array();
$master_tones_list['Natural Blonde'] = array();
$master_tones_list['True Red'] = array();
$master_tones_list['Copper'] = array();
$master_tones_list['Violet Red'] = array();
$master_tones_list['Mahogany Red'] = array();
$master_tones_list['Natural Black'][1] = ['1N', '2N', '3N', '4N', '2.5NNN', '3.5NNN', '4.5NNN'];
$master_tones_list['Natural Black'][2] = ['1N', '2N', '3N', '4N', '2.5NNN', '3.5NNN', '4.5NNN'];
$master_tones_list['Natural Black'][3] = ['1N', '2N', '3N', '4N', '2.5NNN', '3.5NNN', '4.5NNN'];
$master_tones_list['Natural Black'][4] = ['1N', '2N', '3N', '4N', '2.5NNN', '3.5NNN', '4.5NNN'];
$master_tones_list['Violet Black'][1] = ['3NVV'];
$master_tones_list['Violet Black'][2] = ['3NVV'];
$master_tones_list['Violet Black'][3] = ['3NVV'];
$master_tones_list['Violet Black'][4] = ['3NVV'];
$master_tones_list['Blue Black'][1] = ['1NAA', '2NAA', '3NAA', '4NAA', '2NNA', '3NNA'];
$master_tones_list['Blue Black'][2] = ['1NAA', '2NAA', '3NAA', '4NAA', '2NNA', '3NNA'];
$master_tones_list['Blue Black'][3] = ['1NAA', '2NAA', '3NAA', '4NAA', '2NNA', '3NNA'];
$master_tones_list['Blue Black'][4] = ['1NAA', '2NAA', '3NAA', '4NAA', '2NNA', '3NNA'];
$master_tones_list['Mahogany Brown'][2] = ['4NGM', '3NMG', '4NMG'];
$master_tones_list['Mahogany Brown'][3] = ['4NGM', '5NGM', '3NMG', '4NMG', '5NMG'];
$master_tones_list['Mahogany Brown'][4] = ['4NGM', '5NGM', '6NGM', '3NMG', '4NMG', '5NMG', '6NMG'];
$master_tones_list['Mahogany Brown'][5] = ['4NGM', '5NGM', '6NGM', '7NGM', '3NMG', '4NMG', '5NMG', '6NMG', '7NMG'];
$master_tones_list['Mahogany Brown'][6] = ['4NGM', '5NGM', '6NGM', '7NGM', '4NMG', '5NMG', '6NMG', '7NMG'];
$master_tones_list['Mahogany Brown'][7] = ['5NGM', '6NGM', '7NGM', '5NMG', '6NMG', '7NMG'];
$master_tones_list['Cool Brown'][2] = ['2N', '3N', '4N', '2.5NNN', '3.5NNN', '4.5NNN', '4NA', '2NNA', '3NNA', '2NAA', '3NAA', '4NAA'];
$master_tones_list['Cool Brown'][3] = ['2N', '3N', '4N', '5N', '2.5NNN', '3.5NNN', '4.5NNN', '5.5NNN', '4NA', '5NA', '2NNA', '3NNA', '2NAA', '3NAA', '4NAA', '5NAA'];
$master_tones_list['Cool Brown'][4] = ['2N', '3N', '4N', '5N', '6N', '2.5NNN', '3.5NNN', '4.5NNN', '5.5NNN', '6.5NNN', '4NA', '5NA', '6NA', '2NNA', '3NNA', '2NAA', '3NAA', '4NAA', '5NAA', '6NAA'];
$master_tones_list['Cool Brown'][5] = ['3N', '4N', '5N', '6N', '7N', '3.5NNN', '4.5NNN', '5.5NNN', '6.5NNN', '4NA', '5NA', '6NA', '7NA', '3NNA', '3NAA', '4NAA', '5NAA', '6NAA', '7NAA'];
$master_tones_list['Cool Brown'][6] = ['4N', '5N', '6N', '7N', '4.5NNN', '5.5NNN', '6.5NNN', '4NA', '5NA', '6NA', '7NA', 'NNA', '4NAA', '5NAA', '6NAA', '7NAA'];
$master_tones_list['Cool Brown'][7] = ['5N', '6N', '7N', '5.5NNN', '6.5NNN', '5NA', '6NA', '7NA', 'NNA', '5NAA', '6NAA', '7NAA'];
$master_tones_list['Natural Brown'][2] = ['2N', '3N', '4N', '2.5NNN', '3.5NNN', '4.5NNN', '2NAA', '3NAA', '4NAA', '4NA', '2NNA', '3NNA'];
$master_tones_list['Natural Brown'][3] = ['2N', '3N', '4N', '5N', '2.5NNN', '3.5NNN', '4.5NNN', '5.5NNN', '2NAA', '3NAA', '4NAA', '5NAA', '4NA', '5NA', '2NNA', '3NNA'];
$master_tones_list['Natural Brown'][4] = ['2N', '3N', '4N', '5N', '6N', '2.5NNN', '3.5NNN', '4.5NNN', '5.5NNN', '6.5NNN', '2NAA', '3NAA', '4NAA', '5NAA', '6NAA', '4NA', '5NA', '6NA', '2NNA', '3NNA'];
$master_tones_list['Natural Brown'][5] = ['3N', '4N', '5N', '6N', '7N', '3.5NNN', '4.5NNN', '5.5NNN', '6.5NNN', '3NAA', '4NAA', '5NAA', '6NAA', '7NAA', '4NA', '5NA', '6NA', '7NA', '3NNA'];
$master_tones_list['Natural Brown'][6] = ['4N', '5N', '6N', '7N', '4.5NNN', '5.5NNN', '6.5NNN', '4NAA', '5NAA', '6NAA', '7NAA', '4NA', '5NA', '6NA', '7NA', 'NNA'];
$master_tones_list['Natural Brown'][7] = ['5N', '6N', '7N', '5.5NNN', '6.5NNN', '5NAA', '6NAA', '7NAA', '5NA', '6NA', '7NA', 'NNA'];
$master_tones_list['Golden Brown'][2] = ['4G', '4GA', '4NGM'];
$master_tones_list['Golden Brown'][3] = ['4G', '5G', '4GA', '5GA', '4NGM', '5NGM'];
$master_tones_list['Golden Brown'][4] = ['4G', '5G', '6G', '4GA', '5GA', '6GA', '4NGM', '5NGM', '6NGM'];
$master_tones_list['Golden Brown'][5] = ['4G', '5G', '6G', '7G', '4GA', '5GA', '6GA', '7GA', '4NGM', '5NGM', '6NGM', '7NGM'];
$master_tones_list['Golden Brown'][6] = ['4G', '5G', '6G', '7G', '4GA', '5GA', '6GA', '7GA', '4NGM', '5NGM', '6NGM', '7NGM'];
$master_tones_list['Golden Brown'][7] = ['5G', '6G', '7G', '5GA', '6GA', '7GA', '5NGM', '6NGM', '7NGM'];
$master_tones_list['True Brown'][2] = ['2N', '3N', '4N', 'AV'];
$master_tones_list['True Brown'][3] = ['2N', '3N', '4N', '5N', 'AV'];
$master_tones_list['True Brown'][4] = ['2N', '3N', '4N', '5N', '6N', 'AV'];
$master_tones_list['True Brown'][5] = ['3N', '4N', '5N', '6N', '7N', 'AV'];
$master_tones_list['True Brown'][6] = ['4N', '5N', '6N', '7N', 'AV'];
$master_tones_list['True Brown'][7] = ['5N', '6N', '7N', 'AV'];
$master_tones_list['Strawberry Blonde'][7] = ['7NRR', '8NRR', '9NRR', '7CC', '8CC', '9CC', '10NV', '7NV', '8NV', '9NV', '10NVA', '7NVA', '8NVA', '9NVA'];
$master_tones_list['Strawberry Blonde'][8] = ['7NRR', '8NRR', '9NRR', '7CC', '8CC', '9CC', '10NV', '7NV', '8NV', '9NV', '10NVA', '7NVA', '8NVA', '9NVA'];
$master_tones_list['Strawberry Blonde'][9] = ['7NRR', '8NRR', '9NRR', '7CC', '8CC', '9CC', '10NV', '7NV', '8NV', '9NV', '10NVA', '7NVA', '8NVA', '9NVA'];
$master_tones_list['Strawberry Blonde'][10] = ['7NRR', '8NRR', '9NRR', '7CC', '8CC', '9CC', '10NV', '7NV', '8NV', '9NV', '10NVA', '7NVA', '8NVA', '9NVA'];
$master_tones_list['Strawberry Blonde'][11] = ['7NRR', '8NRR', '9NRR', '7CC', '8CC', '9CC', '10NV', '7NV', '8NV', '9NV', '10NVA', '7NVA', '8NVA', '9NVA'];
$master_tones_list['Cool Blonde'][7] = ['10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA'];
$master_tones_list['Cool Blonde'][8] = ['10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA'];
$master_tones_list['Cool Blonde'][9] = ['10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA'];
$master_tones_list['Cool Blonde'][10] = ['10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA'];
$master_tones_list['Cool Blonde'][11] = ['10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA'];
$master_tones_list['Golden Blonde'][7] = ['10G', '7G', '8G', '9G', '10GA', '11GA', '7GA', '8GA', '9GA'];
$master_tones_list['Golden Blonde'][8] = ['10G', '7G', '8G', '9G', '10GA', '11GA', '7GA', '8GA', '9GA'];
$master_tones_list['Golden Blonde'][9] = ['10G', '7G', '8G', '9G', '10GA', '11GA', '7GA', '8GA', '9GA'];
$master_tones_list['Golden Blonde'][10] = ['10G', '7G', '8G', '9G', '10GA', '11GA', '7GA', '8GA', '9GA'];
$master_tones_list['Golden Blonde'][11] = ['10G', '7G', '8G', '9G', '10GA', '11GA', '7GA', '8GA', '9GA'];
$master_tones_list['Natural Blonde'][7] = ['10N', '11N', '7N', '8N', '9N', '10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA', '7NGM', '8NGM', '7NMG'];
$master_tones_list['Natural Blonde'][8] = ['10N', '11N', '7N', '8N', '9N', '10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA', '7NGM', '8NGM', '7NMG'];
$master_tones_list['Natural Blonde'][9] = ['10N', '11N', '7N', '8N', '9N', '10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA', '7NGM', '8NGM', '7NMG'];
$master_tones_list['Natural Blonde'][10] = ['10N', '11N', '7N', '8N', '9N', '10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA', '7NGM', '8NGM', '7NMG'];
$master_tones_list['Natural Blonde'][11] = ['10N', '11N', '7N', '8N', '9N', '10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA', '7NGM', '8NGM', '7NMG'];
$master_tones_list['True Red'][3] = ['5RC', '5NCR', '5NRR', '5RR'];
$master_tones_list['True Red'][4] = ['5RC', '6RC', '5NCR', '6NCR', '5NRR', '6NRR', '5RR', '6RR'];
$master_tones_list['True Red'][5] = ['5RC', '6RC', '7RC', '5NCR', '6NCR', '7NCR', '5NRR', '6NRR', '7NRR', '5RR', '6RR', '7RR'];
$master_tones_list['True Red'][6] = ['5RC', '6RC', '7RC', '8RC', '5NCR', '6NCR', '7NCR', '8NCR', '5NRR', '6NRR', '7NRR', '8NRR', '5RR', '6RR', '7RR', '8RR'];
$master_tones_list['True Red'][7] = ['5RC', '6RC', '7RC', '8RC', '5NCR', '6NCR', '7NCR', '8NCR', '5NRR', '6NRR', '7NRR', '8NRR', '9NRR', '5RR', '6RR', '7RR', '8RR'];
$master_tones_list['True Red'][8] = ['6RC', '7RC', '8RC', '6NCR', '7NCR', '8NCR', '6NRR', '7NRR', '8NRR', '9NRR', '6RR', '7RR', '8RR'];
$master_tones_list['True Red'][9] = ['7RC', '8RC', '7NCR', '8NCR', '7NRR', '8NRR', '9NRR', '7RR', '8RR'];
$master_tones_list['True Red'][10] = ['8RC', '8NCR', '8NRR', '9NRR', '8RR'];
$master_tones_list['True Red'][11] = ['RC', 'NCR', '9NRR', 'RR'];
$master_tones_list['Copper'][3] = ['4NCG', '5NCG', '5CC', '5RC', '5NCR'];
$master_tones_list['Copper'][4] = ['4NCG', '5NCG', '6NCG', '5CC', '6CC', '5RC', '6RC', '5NCR', '6NCR'];
$master_tones_list['Copper'][5] = ['4NCG', '5NCG', '6NCG', '7NCG', '5CC', '6CC', '7CC', '5RC', '6RC', '7RC', '5NCR', '6NCR', '7NCR'];
$master_tones_list['Copper'][6] = ['4NCG', '5NCG', '6NCG', '7NCG', '8NCG', '5CC', '6CC', '7CC', '8CC', '5RC', '6RC', '7RC', '8RC', '5NCR', '6NCR', '7NCR', '8NCR'];
$master_tones_list['Copper'][7] = ['5NCG', '6NCG', '7NCG', '8NCG', '5CC', '6CC', '7CC', '8CC', '9CC', '5RC', '6RC', '7RC', '8RC', '5NCR', '6NCR', '7NCR', '8NCR'];
$master_tones_list['Copper'][8] = ['6NCG', '7NCG', '8NCG', '6CC', '7CC', '8CC', '9CC', '6RC', '7RC', '8RC', '6NCR', '7NCR', '8NCR'];
$master_tones_list['Copper'][9] = ['7NCG', '8NCG', '7CC', '8CC', '9CC', '7RC', '8RC', '7NCR', '8NCR'];
$master_tones_list['Copper'][10] = ['8NCG', '8CC', '9CC', '8RC', '8NCR'];
$master_tones_list['Copper'][11] = ['NCG', '9CC', 'RC', 'NCR'];
$master_tones_list['Violet Red'][3] = ['5RV', 'NVA', 'NV'];
$master_tones_list['Violet Red'][4] = ['5RV', '6RV', '6NVA', 'NV'];
$master_tones_list['Violet Red'][5] = ['5RV', '6RV', '7RV', '6NVA', '7NVA', '7NV'];
$master_tones_list['Violet Red'][6] = ['5RV', '6RV', '7RV', '6NVA', '7NVA', '8NVA', '7NV', '8NV'];
$master_tones_list['Violet Red'][7] = ['5RV', '6RV', '7RV', '6NVA', '7NVA', '8NVA', '9NVA', '7NV', '8NV', '9NV'];
$master_tones_list['Violet Red'][8] = ['6RV', '7RV', '10NVA', '6NVA', '7NVA', '8NVA', '9NVA', '10NV', '7NV', '8NV', '9NV'];
$master_tones_list['Violet Red'][9] = ['7RV', '10NVA', '7NVA', '8NVA', '9NVA', '10NV', '7NV', '8NV', '9NV'];
$master_tones_list['Violet Red'][10] = ['RV', '10NVA', '8NVA', '9NVA', '10NV', '8NV', '9NV'];
$master_tones_list['Violet Red'][11] = ['RV', '10NVA', '9NVA', '10NV', '9NV'];
$master_tones_list['Mahogany Red'][3] = ['4NGM', '5NGM', '3NMG', '4NMG', '5NMG'];
$master_tones_list['Mahogany Red'][4] = ['4NGM', '5NGM', '6NGM', '3NMG', '4NMG', '5NMG', '6NMG'];
$master_tones_list['Mahogany Red'][5] = ['4NGM', '5NGM', '6NGM', '7NGM', '3NMG', '4NMG', '5NMG', '6NMG', '7NMG'];
$master_tones_list['Mahogany Red'][6] = ['4NGM', '5NGM', '6NGM', '7NGM', '8NGM', '4NMG', '5NMG', '6NMG', '7NMG'];
$master_tones_list['Mahogany Red'][7] = ['5NGM', '6NGM', '7NGM', '8NGM', '5NMG', '6NMG', '7NMG'];
$master_tones_list['Mahogany Red'][8] = ['6NGM', '7NGM', '8NGM', '6NMG', '7NMG'];
$master_tones_list['Mahogany Red'][9] = ['7NGM', '8NGM', '7NMG'];
$master_tones_list['Mahogany Red'][10] = ['8NGM', 'NMG'];
$master_tones_list['Mahogany Red'][11] = ['NGM', 'NMG'];


$uploads = wp_upload_dir();

if ($dir = opendir($uploads['basedir'] . '/color_swatches')) {
  $images = array();
  while (false !== ($file = readdir($dir))) {
    if ($file != "." && $file != "..") {
      $images[] = $file;
    }
  }
  closedir($dir);
}
//echo '<pre>';
//print_r(array_keys($master_tones_list));
//$color_array=array_keys($master_tones_list);
//print_r($color_array);
?>
<?php
global $wpdb;
?>
<div class="<?php echo esc_attr(visualcomposerstarter_get_content_container_class()); ?>">
  <div class="content-wrapper">
    <div class="row">
      <div class="<?php echo esc_attr(visualcomposerstarter_get_maincontent_block_class()); ?>">
        <div class="main-content">
          <form id="questionaire-form" class="questionaire-form">
            <div class="row main-form know-color-brand">
              <!-- progressbar -->
              <ul id="progressbar">
                <li class="active"></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <!-- 	<li></li>
					<li></li> -->
              </ul>
              <input type="button" name="restart" class="action-button restart" value="Restart Quiz">
              <span>
                <p>OutPut</p>
                <pre id="output"></pre>
              </span>
              <fieldset>
                <div class="bg current-shade new-color">
                  <h6 class="que">What is the natural color of your hair?</h6>
                  <div>
                    <label>
                      <input type="radio" name="natural-shade" class="first" value="Black">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-1.jpg) !important;">Black</span>
                    </label>
                    <label>
                      <input type="radio" name="natural-shade" class="gold" value="Brown">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-3.jpg) !important;">Brown</span>
                    </label>
                    <label>
                      <input type="radio" name="natural-shade" class="neutral" value="Blonde">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-7.jpg) !important;">Blonde</span>
                    </label>
                    <label>
                      <input type="radio" name="natural-shade" class="last" value="Red">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Auburn_6.54.jpg) !important;">Red</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>
              <fieldset class="required brand-section">
                <div class="bg brand-name">
                  <h6 class="que">PLEASE TELL US THE NAME OF THE BRAND YOU CURRENTLY USE.<span style="color:#ff003c;">*</span></h6>
                  <label>
                    <select id="brand-name" name="brand-name">
                      <option>Select Brand</option>
                      <?php
                      $customers = $wpdb->get_results("SELECT * FROM brand_name");
                      foreach ($customers as $rows) {
                        echo "<option value='" . $rows->Brand_Id . "'>" . $rows->Brand_Name . "</option>";
                      }
                      ?>
                      <!-- just 4 dev -->
                      <option value='test-brand'>test-brand</option>
                    </select>
                  </label>
                  <div class="error-msg" id="err1"></div>
                </div>
                <!--brand-name-->
                <div class="bg brand-shade new-color hidden">
                  <h6 class="que">PLEASE TELL US THE PRODUCT YOU CURRENTLY USE.<span style="color:#ff003c;">*</span></h6>
                  <label>
                    <select id="color-shade" name="color-shade">
                      <option>Select Product</option>
                      <!-- just 4 dev -->
                      <option value='test-brand-product'>test-brand-product</option>
                    </select>
                  </label>
                  <div class="brand-color">
                  </div>
                  <!-- <div id="color-shade">
                  </div> -->
                  <div class="error-msg" id="err2"></div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button hidden" value="Confirm" />
              </fieldset>
              <fieldset class="required swatch-selection">
                <div class="bg swatches sub-que-row q9-sub-que q9-sub-1">
                  <h6 class="que">Confirm color choice by showing corresponding coloronly swatch<span style="color:#ff003c;">*</span> </h6>
                  <div class="multi-swatches">
                    <h6 class="text-left"></h6>
                    <div class="col-sm-6 listing">
                      <ul>
                      </ul>
                    </div>
                    <div class="col-sm-6">
                      <div class="selected-swatch">
                      </div>
                    </div>
                  </div>
                  <!-- <div class="error-msg" id="err12"></div> -->
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Confirm" />
              </fieldset>
              <fieldset class="required touch-or-all">
                <div class="bg color-type q-1">
                  <h6 class="que">Select root touch up or all over color?<span style="color:#ff003c;">*</span></h6>
                  <div>
                    <label>
                      <input type="radio" name="color-type" class="first" value="Root Touch Up">
                      <span class="input-label">Root Touch Up</span>
                    </label>
                    <label>
                      <input type="radio" name="color-type" class="last" value="All Over Color">
                      <span class="input-label">All Over Color</span>
                    </label>
                  </div>
                  <div class="error-msg" id="err12"></div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>
              <fieldset class="required ">
                <div class="last-color sub-que hidden">
                  <div class="bg no-count last-coloring">
                    <h6 class="que  que2A"> How long since last color?</h6>
                    <div>
                      <label>
                        <input type="radio" name="last-coloring" class="first" value="2 WKS">
                        <span class="input-label">2 Weeks</span>
                      </label>

                      <label>
                        <input type="radio" name="last-coloring" class="4-wk" value="4 WKS">
                        <span class="input-label">4 Weeks</span>
                      </label>

                      <label>
                        <input type="radio" name="last-coloring" class="6-wk" value="6 WKS">
                        <span class="input-label"> 6 Weeks</span>
                      </label>

                      <label>
                        <input type="radio" name="last-coloring" class="8-wk" value="8 WKS">
                        <span class="input-label"> 8 Weeks</span>
                      </label>

                      <label>
                        <input type="radio" name="last-coloring" class="6-month last" value="6 MONTHS">
                        <span class="input-label"> 6 Months</span>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="select-hair-length sub-que hidden">
                  <div class="bg hair-length no-count">
                    <h6 class="que  que2B"> Select length of hair?</h6>
                    <div>
                      <label>
                        <input type="radio" name="hair-length" class="first short" value="Short">
                        <span class="input-label">Short</span>
                      </label>
                      <label>
                        <input type="radio" name="hair-length" class="med medium" value="Medium">
                        <span class="input-label">Medium</span>
                      </label>

                      <label>
                        <input type="radio" name="hair-length" class="last long" value="Long">
                        <span class="input-label">Long</span>
                      </label>
                    </div>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Confirm" />
              </fieldset>
              <fieldset class="service-checkbox">
                <div class="bg services q-2">
                  <h6 class="que">Have you had any of following services since your last color?</h6>
                  <div class="note"></div>
                  <div>
                    <label>
                      <input type="checkbox" name="recent-services" value="Perm or Body Wave" class="recent-services">
                      <span class="input-label">Perm or Body Wave</span>
                    </label>
                    <label>
                      <input type="checkbox" name="recent-services" value="Relaxer or KERATIN" class="recent-services">
                      <span class="input-label">Relaxer or KERATIN</span>
                    </label>
                  </div>
                  <div>
                    <label>
                      <input type="checkbox" name="recent-services" class="recent-services" value="All Over Bleach">
                      <span class="input-label">All Over Bleach</span>
                    </label>
                    <label>
                      <input type="checkbox" name="recent-services" class="recent-services" value="Heavy Highlights">
                      <span class="input-label">Heavy Highlights</span>
                    </label>
                  </div>
                  <div>
                    <label>
                      <input type="checkbox" name="recent-services" class="recent-services" value="HENNA">
                      <span class="input-label">HENNA</span>
                    </label>
                    <label>
                      <input type="checkbox" name="recent-services" class="recent-services last" value="NONE">
                      <span class="input-label">None</span>
                    </label>
                  </div>
                </div>
                <div class="warning-section hidden" style>
                  <h6 class="">Bleach may remove ability to perform dye </h6>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Confirm" />
              </fieldset>
              <fieldset class="required">
                <div class="bg how-much-gray new-color">
                  <h6 class="que mb-1">How much gray do you have in your natural color?<span style="color:#ff003c;">*</span></h6>
                  <div class="note">To be specific - look at your hair at the roots as it grows in.</div>
                  <div>
                    <label>
                      <input type="radio" name="much-gray" class="first" value="None or hardly any">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/none_or_hardly.jpg) !important;">None or hardly any</span>
                    </label>
                    <label>
                      <input type="radio" name="much-gray" class="sec" value="Some">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Some_gray.jpg) !important;">Some</span>
                    </label>
                  </div>
                  <div>
                    <label>
                      <input type="radio" name="much-gray" class="neutral" value="Mostly Gray">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/Mostly_gray.jpg) !important;">Mostly Gray</span>
                    </label>
                    <label>
                      <input type="radio" name="much-gray" class="last" value="All Gray">
                      <span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/All_gray.jpg) !important;">All Gray</span>
                    </label>
                  </div>
                  <div class="error-msg" id="err2"></div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>
              <fieldset>
                <div class="bg yes-no">
                  <h6 class="que">Have you had trouble covering your grays?</h6>
                  <div>
                    <label>
                      <input type="radio" name="covering-trouble" class="first" value="Yes">
                      <span class="input-label">Yes</span>
                    </label>
                  </div>
                  <div>
                    <label>
                      <input type="radio" name="covering-trouble" class="last" value="No">
                      <span class="input-label">No</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>

              <fieldset>
                <div class="bg q-10">
                  <h6 class="que">What is the condition of your hair?<span style="color:#ff003c;">*</span></h6>
                  <div>
                    <label>
                      <input type="radio" name="hair-condition" class="first" value="Healthy">
                      <span class="input-label">Healthy</span>
                    </label>
                    <label>
                      <input type="radio" name="hair-condition" class="sec" value="Porous">
                      <span class="input-label">Porous</span>
                    </label>
                  </div>
                  <div>
                    <label>
                      <input type="radio" name="hair-condition" class="last" value="Damaged">
                      <span class="input-label">Damaged</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>
              <fieldset class="preview">
                <div class="edit-color multi-swatches">
                  <h6>Based on the answers, this is the suggested color swatch.</h6>
                  <div class="col-sm-6 color-listing">

                  </div>
                  <div class="col-sm-6 color-preview">
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Questionnaire Summary" />
              </fieldset>
              <fieldset class="summary summary1">
                <h5 style="color:#000;margin-bottom:20px;">Questionnaire Summary</h5>
                <div class="row">
                  <div class="col-sm-4">
                    <div>Date:<span class="current_date"></span></div>
                    <div>Formula:<span class="formula"></span></div>
                  </div>
                  <div class="col-sm-4">
                    <div>Hair Type:<span class="hair_type"></span></div>
                    <div>Keratin:<span class="keratin"></span></div>
                    <div class="porous"></div>
                  </div>
                  <div class="col-sm-4">
                    <div class="warning"></div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <div>Amount to dispense: <span class="clr_amt"></span></div>
                    <div><span class="color_type"></span></div>
                    <div><span class="resistant"></span></div>
                    <div>Suggested formula: <span class="formula"> </span></div>
                  </div>
                  <div class="col-sm-8">
                    <h6 style="margin-top:35px;">Now that you've completed your formula questionnaire let's save it to your profile</h6>
                    <div class="row">
                      <div class="col-sm-6">
                        <div>What is your name?</div>
                      </div>
                      <div class="col-sm-6">
                        <input name="user-name" type="text" required>
                      </div>
                      <div class="col-sm-6">
                        <div>Mobile number?</div>
                      </div>
                      <div class="col-sm-6">
                        <input name="mobile-number" type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required>
                      </div>
                    </div>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="text" name="submit" class="text-center next que_sum action-button" value="Submit" readonly />
              </fieldset>
              <fieldset class="summary">
                <h5 style="color:#000;margin-bottom:20px;">Questionnaire Summary</h5>
                <div class="row">
                  <div class="col-sm-4">
                    <div>Date:<span class="current_date"></span></div>
                    <div>Name:<span class="name"></span></div>
                    <div>Formula:<span class="formula"></span></div>
                  </div>
                  <div class="col-sm-4">
                    <div>Hair Type:<span class="hair_type"></span></div>
                    <div>Keratin:<span class="keratin"></span></div>
                    <div class="porous"></div>
                  </div>
                  <div class="col-sm-4">
                    <div class="warning"></div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <div>Phone:<span class="phone"></span></div>
                  </div>
                  <div class="col-sm-4">
                    <div>Amount to dispense: <span class="clr_amt"></span></div>
                    <!-- <div><span class="color_type"></span></div>
          <div><span class="resistant"></span></div>
          <div>Suggested formula: <span class="formula"> </span></div> -->
                  </div>
                  <div class="col-sm-4">
                    <!-- <div>Amount to dispense: <span class="clr_amt"></span></div> -->
                    <div><span class="color_type"></span></div>

                  </div>
                  <div class="col-sm-4">

                    <div><span class="resistant"></span></div>
                    <!--  <div>Suggested formula: <span class="formula"> </span></div> -->
                  </div>
                </div>
            </div>
            </fieldset>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
<?php get_footer(); ?>
<!-- jQuery easing plugin -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" type="text/javascript"></script>
<script>
  var formValues = {
    q1: ''
  }
  var myform = {
    q1: {
      value: '',
      label: 'Will this be a root touch up or all over color?',
      choice: [{
        'rootsOnly': 'Roots Only'
      }, {
        'allOverColor': 'All over color'
      }],
      action: [
        'next',
        'goTo4'
      ],
      rule: [
        'dispense 1oz',
        'dispense 2.5oz'
      ],
      condition: []
    }
  };

  var validStepsForm = () => {
    showLog();
  }

  var mapData = () => {
    // const form = jQuery('#questionaire-form').serializeArray();
    // var formMap = new Map();
    // form.forEach(item => {
    //   formMap.set(item.name, item.value);
    // });
    // console.log('brand-name------', {
    //   formMap: formMap.get('brand-name')
    // });



    // return formMap;
  }

  var getInputValue = (inputName) => {
    const form = jQuery('#questionaire-form').serializeArray();
    var formMap = new Map();
    form.forEach(item => {
      formMap.set(item.name, item.value);
    });
    return formMap.get(inputName);
  }

  var showLog = () => {
    // jQuery("#output").text(JSON.stringify(mapData()));
    console.log('-------form', {
      form: {
        'step': getInputValue('step'),

        'natural-shade': getInputValue('natural-shade'),
        'brand-name': getInputValue('brand-name'),
        'color-shade': getInputValue('color-shade'),
        // 'swatch': getInputValue('swatch'), // missed input
        'color-type': getInputValue('color-type'),
        'last-coloring': getInputValue('last-coloring'),
        'hair-length': getInputValue('hair-length'),
        'recent-services': getInputValue('recent-services'),
        'much-gray': getInputValue('much-gray'),
        'covering-trouble': getInputValue('covering-trouble'),
        'hair-condition': getInputValue('hair-condition'),
        'suggested-color-swatch': getInputValue('suggested-color-swatch'),

        /*  ??? Summary
          Date
          Formula
          Hair Type
          Keratin
          Amount to dispense
          resistant
          Suggested formula
        */

        'user-name': getInputValue('user-name'),
        'user-email': getInputValue('user-email'), // falta en el diseño
        'mobile-number': getInputValue('mobile-number'),
      }
    });
  }

  jQuery(() => {
    validStepsForm();
  })

  var color_swatches = <?php echo json_encode($master_tones_list); ?>;
  var bg_img = <?php echo json_encode($images); ?>; //All images from swatches folder
  var keys = Object.keys(color_swatches);


  var current_fs, next_fs, previous_fs; //fieldsets
  var left, opacity, scale; //fieldset properties which we will animate
  var animating; //flag to prevent quick multi-click glitches

  var base = "<?php echo get_home_url(); ?>";

  jQuery(document).on("change", ".required div input", function() {
    jQuery(this).parents().find('.bg .error-msg').text('');
  });

  jQuery(".next").click(function() {

    if (jQuery(this).parent('.required').css('display') == 'block') {
      if (!jQuery(this).parent('.required').find('div input').is(':checked')) {
        if (jQuery(this).parent('.required').find('div select').length) {
          if (jQuery(this).parent('.required').find('div select').val() === 'Select Brand' || jQuery(this).parent('.required').find('div select').val() === 'Select Color Shade') {
            jQuery(this).parent('.required').find('.error-msg').text('This field is required');
            jQuery(this).parent('.required').find('.error-msg').css('color', 'red');
            return false;
          }
        } else {
          jQuery(this).parent('.required').find('.error-msg').text('This field is required');
          jQuery(this).parent('.required').find('.error-msg').css('color', 'red');
          return false;
        }
      } else {
        jQuery(this).parent('.required').find('.error-msg').text('');
      }
    }

    if (animating) return false;
    animating = true;
    current_fs = jQuery(this).parent();
    next_fs = jQuery(this).parent().next();
    //activate next step on progressbar using the index of next_fs
    jQuery("#progressbar li").eq(jQuery("fieldset").index(next_fs)).addClass("active");
    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({
      opacity: 0
    }, {
      step: function(now, mx) {
        //as the opacity of current_fs reduces to 0 - stored in "now"
        //1. scale current_fs down to 80%
        scale = 1 - (1 - now) * 0.2;
        //2. bring next_fs from the right(50%)
        left = (now * 50) + "%";
        //3. increase opacity of next_fs to 1 as it moves in
        opacity = 1 - now;
        current_fs.css({
          'transform': 'scale(' + scale + ')'
        });
        next_fs.css({
          'left': left,
          'opacity': opacity
        });
      },
      duration: 500,
      complete: function() {
        current_fs.hide();
        animating = false;
      },
      //this comes from the custom easing plugin
      easing: 'easeOutQuint'
    });
  });

  jQuery(".previous").click(function() {
    if (animating) return false;
    animating = true;

    current_fs = jQuery(this).parent();
    previous_fs = jQuery(this).parent().prev();

    //de-activate current step on progressbar
    jQuery("#progressbar li").eq(jQuery("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({
      opacity: 0
    }, {
      step: function(now, mx) {
        //as the opacity of current_fs reduces to 0 - stored in "now"
        //1. scale previous_fs from 80% to 100%
        scale = 0.8 + (1 - now) * 0.2;
        //2. take current_fs to the right(50%) - from 0%
        left = ((1 - now) * 50) + "%";
        //3. increase opacity of previous_fs to 1 as it moves in
        opacity = 1 - now;
        current_fs.css({
          'left': left
        });
        previous_fs.css({
          'transform': 'scale(' + scale + ')',
          'opacity': opacity
        });
      },
      duration: 500,
      complete: function() {
        current_fs.hide();
        animating = false;
      },
      //this comes from the custom easing plugin
      easing: 'easeOutQuint'
    });
  });


  var pageURL = jQuery(location).attr("href");
  var parts = pageURL.split("/");
  var last_part = parts[parts.length - 2];
  var current_color, next_color;

  jQuery(".restart").click(function() {
    if (confirm('You can start over at any time, just remember this will remove your answers!')) {
      location.reload(true);
    }
  });

  jQuery("fieldset .previous").click(function() {
    jQuery(this).parent('fieldset').prev().find(".next").css({
      "visibility": "visible",
      "width": "100px",
      "padding": "10px 5px",
      "height": "auto"
    });
    //  console.log(check);  
  });

  jQuery("input[name='color-shade'],input[name='color-type'],input[name='last-coloring'],input[name='hair-length'],input[name='much-gray'],input[name='covering-trouble'],input[name='natural-shade'],input[name='hair-condition']").click(function() {
    //  jQuery("fieldset .previous").attr('disabled', false);
    validStepsForm();

    jQuery(this).parents('fieldset').find(".next").click();
  });

  /*****COLOR TYPE 'Root Touch Up or All Over Color'******/
  jQuery(".color-type .last").click(function() {
    jQuery(" .select-hair-length").removeClass('hidden');
    jQuery(".last-color").addClass('hidden');
  });

  jQuery(" .color-type .first").click(function() {
    jQuery(" .last-color").removeClass('hidden');
    jQuery(" .select-hair-length").addClass('hidden');

  });
  /*****SERVICES *****/
  jQuery(".services input.last").click(function(e) {
    jQuery(".services input:not(.last)").prop('checked', false);
    //  jQuery("fieldset .previous").attr('disabled',true);
    jQuery(this).parents('fieldset').find(".next").click();
    console.log('click', {
      event: e.target
    });

  });

  jQuery(".services input:not(.last)").click(function() {

    jQuery.each(jQuery(".services input[type='checkbox']:checked"), function() {
      if (jQuery(this).val() == "All Over Bleach") {
        jQuery(".service-checkbox .warning-section").removeClass("hidden");
      } else {
        jQuery(".service-checkbox .warning-section").addClass("hidden");
      }
    });
    jQuery(".services input.last").prop('checked', false);
  });

  /*jQuery(document).on('change','.brand-name select',function()
  {
    if(jQuery(".brand-name select").val() != 'Select Brand')
      {
        jQuery(".brand-shade").removeClass("hidden");
      //  var html=[];
        jQuery.each( color_swatches, function( key, value ) {
        var  html='<label><input type="radio" name="color-shade" class="" value="'+key+'"><span class="input-label" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/images/level-1.jpg) !important;">'+key+'</span></label>';
          jQuery('#color-shade').append(html);
        });
        
        
      //  jQuery('.brand-shade select').parents('.required').find('.next').click();
      }
  });*/

  jQuery(document).on('change', '.brand-name select', function() {
    if (jQuery(".brand-name select").val() != 'Select Brand') {
      jQuery(".brand-shade").removeClass("hidden");
      jQuery(".brand-color div").remove();
      jQuery('#color-shade select option').each(function() {
        jQuery(this).remove();
      });
      var brand_id = jQuery(this).val();
      jQuery.ajax({
        url: '<?php echo get_stylesheet_directory_uri(); ?>/brand-ajax.php',
        type: 'post',
        dataType: 'json',
        data: {
          brand_id: brand_id
        },
        success: function(data) {
          console.log(data);
          var html = '<option>Select Product</option>';
          jQuery.each(data, function(key, value) {
            html += '<option  coloronly-shade=' + value[3] + '>' + value[1] + '</option>'
          });
          jQuery('#color-shade').html(html);
          validStepsForm();
        }
      });
    }
  });

  jQuery(document).on('change', '#color-shade', function() {
    jQuery(".brand-section .next").removeClass("hidden");
    jQuery(".brand-color div").remove();
    var color_shade = jQuery('option:selected', this).attr('coloronly-shade');
    var img_name = color_shade + "_edit.png";
    console.log(img_name);
    if (bg_img.indexOf(img_name) !== -1) {
      console.log(color_shade);
      var url = base + '/wp-content/uploads/color_swatches/' + img_name;
      console.log(url);
      jQuery(".brand-color").append('<div class="swatch-with-name" style="color:#0395a7;padding-top:30px;font-weight:bold;width:200px;height:200px;margin:0 auto;background-image:url(' + url + ');background-size:cover;">' + color_shade + '</div>');
      //   jQuery(".multi-swatches ul").append('<li><label class="no-hover"><input type="radio" name="color-swatches" title="'+image_name+'" value="'+value+'"><span class="input-label" style="background-image:url('+url+')!important;">'+value+'</span></label></li>');
    }
    validStepsForm();
  });

  /******Color Shade Selection*******/
  /*jQuery(document).on('change','#color-shade input',function()
  {
    
    jQuery(".swatches div.multi-swatches ul li,.selected-swatch img,.color-preview img,.selected-color").remove();
    var value_arr=[];var data_arr=[];var final_arr=[];
    var shade_id = jQuery(this).val();
    if(shade_id in color_swatches)
    {
        jQuery.each(color_swatches[shade_id], function( key, value ) 
        {
        value_arr=value;  
      });
      final_arr=jQuery.merge(data_arr,value_arr);
        jQuery.each(final_arr, function( key, value ) 
        {   
          image_name=value;
      var img_name = value +"_edit.png";  
      if(bg_img.indexOf(img_name) !== -1)
      {
          var url= base+'/wp-content/uploads/color_swatches/'+img_name;
            jQuery(".multi-swatches ul").append('<li><label class="no-hover"><input type="radio" name="color-swatches" title="'+image_name+'" value="'+value+'"><span class="input-label" style="background-image:url('+url+')!important;">'+value+'</span></label></li>');
        } 
      });
      jQuery(".multi-swatches ul li:first-child").addClass("active");
      var color = jQuery(".color-listing ul li.active input").attr("title");
        var active_color=jQuery(".multi-swatches ul li.active span").css('background-image');
        active_color = active_color.replace('url(','').replace(')','').replace(/\"/gi, "");
         jQuery(".selected-swatch").append("<p class='selected-color'>"+color+"</p><img src='"+ active_color+"'>");
        jQuery(".preview .edit-color .color-preview").append("<p class='selected-color'>"+color+"</p><img src='"+ active_color+"'>");
         jQuery(".preview .edit-color .color-listing").append(jQuery(".swatch-selection .multi-swatches .listing").html());
        jQuery(".multi-swatches ul li.active input").click();
    }
    jQuery('#color-shade').parents('fieldset').find('.next').click();
    jQuery("fieldset .previous").attr('disabled', false);
    
  });*/

  /******Select color swatch from listing********/
  jQuery(document).on('click', ".swatches .multi-swatches input", function() {
    jQuery(".preview .edit-color .color-listing ul").remove();
    jQuery(".multi-swatches ul li,.color-listing ul li").removeClass("active");
    jQuery(this).parent().parent().addClass("active");
    var color = jQuery(".multi-swatches ul li.active input").attr("title");
    current_color = jQuery(".swatch-selection ul li.active input").val();
    next_color = jQuery(".swatch-selection ul li.active").next("li").find("input").val();
    var active_color = jQuery(".multi-swatches ul li.active span").css('background-image');
    active_color = active_color.replace('url(', '').replace(')', '').replace(/\"/gi, "");
    jQuery(".selected-swatch p.selected-color,.color-preview p.selected-color").text(color);
    jQuery(".selected-swatch img,.color-preview img").attr("src", active_color);
    if (jQuery('.multi-swatches ul li').length > 1) {
      jQuery(".preview .edit-color h6").text("Based on the answers, this is the suggested color swatch. Please look at the other color swatches and if you find one that you like better,Please select that ");
      jQuery(".preview .col-sm-6").css("width", "50%");
      jQuery(".preview .edit-color .color-listing").append(jQuery(".swatch-selection .multi-swatches .listing").html());
    }
  });

  /*****Preview Section******/
  jQuery(document).on('click', ".preview .color-listing li input", function() {
    //alert("hello");
    jQuery(".color-listing ul li").removeClass("active");
    jQuery(this).parent().parent().addClass("active");
    var color = jQuery(".color-listing ul li.active input").attr("title");
    var active_color = jQuery(".color-listing ul li.active span").css('background-image');
    active_color = active_color.replace('url(', '').replace(')', '').replace(/\"/gi, "");
    jQuery(".color-preview img").attr("src", active_color);
    jQuery(".color-preview p.selected-color").text(color);
  });

  /***Questionnaire Summary****/
  jQuery(".preview .action-button").click(function() {
    var dispanse_amt, dying_service, hair_type, keratin, porous;
    var services = [];
    var d = new Date();
    var date = d.getDate();
    var month = d.getMonth() + 1; // Since getMonth() returns month from 0-11 not 1-12
    var year = d.getFullYear();
    var current_date = month + "/" + date + "/" + year;
    if (jQuery(".preview ul li.active input").val() != current_color) {
      current_color = jQuery(".preview ul li.active input").val();
    }
    var formula = current_color;

    if (jQuery("input[name='hair-length']:checked").val() == 'Short') {
      dispanse_amt = "1.5oz";
    }
    if (jQuery("input[name='hair-length']:checked").val() == 'Medium') {
      dispanse_amt = "2oz";
    }
    if (jQuery("input[name='hair-length']:checked").val() == 'Long') {
      dispanse_amt = "3oz";
    }
    jQuery.each(jQuery(".services input[type='checkbox']:checked"), function() {
      services.push(jQuery(this).val());
    });

    if (services.includes("Relaxer or KERATIN")) {
      keratin = "Yes";
    } else {
      keratin = "No";
    }
    if (jQuery("input[name='hair-condition']:checked").val() == 'Porous') {
      porous = "Porous Alert!";
      jQuery(".porous").css("color", "red");
      jQuery(".warning").append("<img src='<?php echo get_stylesheet_directory_uri(); ?>/images/danger.png'>");
    } else {
      porous = "Not Porous";
      jQuery(".warning").append("<span class='no-warning' style='font-size:18px;'>No Warnings</span>");
    }
    if (jQuery("input[name='covering-trouble']:checked").val() == 'Yes') {
      resistant = "Resistant";
      hair_type = "Resistant";
    } else {
      resistant = "Not Resistant";
    }
    //All Over Color with bleach
    if (jQuery("input[name='color-type']:checked").val() == 'All Over Color' && services.includes("All Over Bleach") && jQuery("input[name='covering-trouble']:checked").val() == 'No') {
      hair_type = "Bleached";
    }
    //All Over color without bleach and resistant
    if ((jQuery("input[name='color-type']:checked").val() == 'All Over Color') && (services.indexOf("All Over Bleach") === -1) && jQuery("input[name='covering-trouble']:checked").val() == 'No') {
      hair_type = "Normal";
    }
    if ((jQuery("input[name='color-type']:checked").val() == 'All Over Color') && (services.indexOf("All Over Bleach")) && jQuery("input[name='covering-trouble']:checked").val() == 'Yes') {
      hair_type = "Resistant";
    }
    //Root Touch Up with bleach/resistant or with out bleach/resistant
    if (jQuery("input[name='color-type']:checked").val() == 'Root Touch Up') {
      hair_type = "Virgin";
    }

    if (jQuery("input[name='last-coloring']:checked").val() == '2 WKS') {
      dispanse_amt = "0.5oz";
    }
    if (jQuery("input[name='last-coloring']:checked").val() == '4 WKS') {
      dispanse_amt = "1oz";
    }
    if (jQuery("input[name='last-coloring']:checked").val() == '6 WKS') {
      dispanse_amt = "1.5oz";
    }
    if (jQuery("input[name='last-coloring']:checked").val() == '8 WKS') {
      dispanse_amt = "2oz";
    }
    if (jQuery("input[name='last-coloring']:checked").val() == '6 MONTHS') {
      dispanse_amt = "6oz";
    }
    dying_service = jQuery("input[name='color-type']:checked").val();
    jQuery(".resistant").text(resistant);
    jQuery(".current_date").text(current_date);
    jQuery(".formula").text(formula);
    jQuery(".hair_type").text(hair_type);
    jQuery(".keratin").text(keratin);
    jQuery(".porous").text(porous);
    jQuery(".color_type").text(dying_service);
    jQuery(".clr_amt").text(dispanse_amt);
  });
  /*******F O R M   S E R V I C E S*****/
  jQuery(".que_sum").click(function() {
    jQuery(".name").text(jQuery("input[name='user-name']").val());
    jQuery(".phone").text(jQuery("input[name='mobile-number']").val());
  });
</script>
<style>
  .restart {
    position: absolute;
    right: 40px;
    z-index: 1;
    top: -20px;
  }
</style>
}