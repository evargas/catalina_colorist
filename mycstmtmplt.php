<?php
/*
* Template Name: Custom Full Width
* description: >-
  Page template without sidebar
*/
get_header(); ?>
    <div class="<?php echo esc_attr( visualcomposerstarter_get_content_container_class() ); ?>">
        <div class="content-wrapper">
            <div class="row">
                <div class="<?php echo esc_attr( visualcomposerstarter_get_maincontent_block_class() ); ?>">
                    <div class="main-content">
                        <?php
                        // Additional code goes here...
                        $args = array(
                            'number'     => $number,
                            'orderby'    => 'title',
                            'order'      => 'ASC',
                            'hide_empty' => $hide_empty,
                            'include'    => $ids,
                            'parent'     => 0
                        );
                        $product_categories = get_terms( 'product_cat', $args );
                        $count = count($product_categories);
                        if ( $count > 0 ){
                            foreach ( $product_categories as $product_category ) {
                                echo '<h4 style="margin-top:35px;font-weight:bold;"><a href="' . get_term_link( $product_category ) . '">' . $product_category->name . '</a></h4>';
                                $args = array(
                                    'posts_per_page' => -1,
                                    'tax_query' => array(
                                        'relation' => 'AND',
                                        array(
                                            'taxonomy' => 'product_cat',
                                            'number'     => 4,
                                            'field' => 'slug',
                                            // 'terms' => 'white-wines'
                                            'terms' => $product_category->slug
                                        )
                                    ),
                                    'post_type' => 'product',
                                    'orderby' => 'title,'
                                );
                                $products = new WP_Query( $args );
                               echo do_shortcode('[products limit="4" columns="4" category="'.$product_category->slug.'"]');
                            }
                        }
                        ?>
                    </div><!--.main-content-->
                </div><!--.<?php echo esc_html( visualcomposerstarter_get_maincontent_block_class() ); ?>-->
                <?php if ( visualcomposerstarter_get_sidebar_class() ) : ?>
                    <?php get_sidebar(); ?>
                <?php endif; ?>
            </div><!--.row-->
        </div><!--.content-wrapper-->
    </div><!--.<?php echo esc_html( visualcomposerstarter_get_content_container_class() ); ?>-->
<?php get_footer();
