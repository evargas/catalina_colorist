<?php
session_start();
/**
 * Template Name:Demo Template
 *
 * 
 */
get_header(); ?>
<?php
global $wpdb;
$master_tones_list = array();
$master_tones_list['Natural Black'] = array();
$master_tones_list['Violet Black'] = array();
$master_tones_list['Blue Black'] = array();
$master_tones_list['Mahogany Brown'] = array();
$master_tones_list['Cool Brown'] = array();
$master_tones_list['Natural Brown'] = array();
$master_tones_list['Golden Brown'] = array();
$master_tones_list['True Brown'] = array();
$master_tones_list['Strawberry Blonde'] = array();
$master_tones_list['Cool Blonde'] = array();
$master_tones_list['Golden Blonde'] = array();
$master_tones_list['Natural Blonde'] = array();
$master_tones_list['True Red'] = array();
$master_tones_list['Copper'] = array();
$master_tones_list['Violet Red'] = array();
$master_tones_list['Mahogany Red'] = array();
$master_tones_list['Natural Black'][1] = ['1N', '2N', '3N', '4N', '2.5NNN', '3.5NNN', '4.5NNN'];
$master_tones_list['Natural Black'][2] = ['1N', '2N', '3N', '4N', '2.5NNN', '3.5NNN', '4.5NNN'];
$master_tones_list['Natural Black'][3] = ['1N', '2N', '3N', '4N', '2.5NNN', '3.5NNN', '4.5NNN'];
$master_tones_list['Natural Black'][4] = ['1N', '2N', '3N', '4N', '2.5NNN', '3.5NNN', '4.5NNN'];
$master_tones_list['Violet Black'][1] = ['3NVV'];
$master_tones_list['Violet Black'][2] = ['3NVV'];
$master_tones_list['Violet Black'][3] = ['3NVV'];
$master_tones_list['Violet Black'][4] = ['3NVV'];
$master_tones_list['Blue Black'][1] = ['1NAA', '2NAA', '3NAA', '4NAA', '2NNA', '3NNA'];
$master_tones_list['Blue Black'][2] = ['1NAA', '2NAA', '3NAA', '4NAA', '2NNA', '3NNA'];
$master_tones_list['Blue Black'][3] = ['1NAA', '2NAA', '3NAA', '4NAA', '2NNA', '3NNA'];
$master_tones_list['Blue Black'][4] = ['1NAA', '2NAA', '3NAA', '4NAA', '2NNA', '3NNA'];
$master_tones_list['Mahogany Brown'][2] = ['4NGM', '3NMG', '4NMG'];
$master_tones_list['Mahogany Brown'][3] = ['4NGM', '5NGM', '3NMG', '4NMG', '5NMG'];
$master_tones_list['Mahogany Brown'][4] = ['4NGM', '5NGM', '6NGM', '3NMG', '4NMG', '5NMG', '6NMG'];
$master_tones_list['Mahogany Brown'][5] = ['4NGM', '5NGM', '6NGM', '7NGM', '3NMG', '4NMG', '5NMG', '6NMG', '7NMG'];
$master_tones_list['Mahogany Brown'][6] = ['4NGM', '5NGM', '6NGM', '7NGM', '4NMG', '5NMG', '6NMG', '7NMG'];
$master_tones_list['Mahogany Brown'][7] = ['5NGM', '6NGM', '7NGM', '5NMG', '6NMG', '7NMG'];
$master_tones_list['Cool Brown'][2] = ['2N', '3N', '4N', '2.5NNN', '3.5NNN', '4.5NNN', '4NA', '2NNA', '3NNA', '2NAA', '3NAA', '4NAA'];
$master_tones_list['Cool Brown'][3] = ['2N', '3N', '4N', '5N', '2.5NNN', '3.5NNN', '4.5NNN', '5.5NNN', '4NA', '5NA', '2NNA', '3NNA', '2NAA', '3NAA', '4NAA', '5NAA'];
$master_tones_list['Cool Brown'][4] = ['2N', '3N', '4N', '5N', '6N', '2.5NNN', '3.5NNN', '4.5NNN', '5.5NNN', '6.5NNN', '4NA', '5NA', '6NA', '2NNA', '3NNA', '2NAA', '3NAA', '4NAA', '5NAA', '6NAA'];
$master_tones_list['Cool Brown'][5] = ['3N', '4N', '5N', '6N', '7N', '3.5NNN', '4.5NNN', '5.5NNN', '6.5NNN', '4NA', '5NA', '6NA', '7NA', '3NNA', '3NAA', '4NAA', '5NAA', '6NAA', '7NAA'];
$master_tones_list['Cool Brown'][6] = ['4N', '5N', '6N', '7N', '4.5NNN', '5.5NNN', '6.5NNN', '4NA', '5NA', '6NA', '7NA', 'NNA', '4NAA', '5NAA', '6NAA', '7NAA'];
$master_tones_list['Cool Brown'][7] = ['5N', '6N', '7N', '5.5NNN', '6.5NNN', '5NA', '6NA', '7NA', 'NNA', '5NAA', '6NAA', '7NAA'];
$master_tones_list['Natural Brown'][2] = ['2N', '3N', '4N', '2.5NNN', '3.5NNN', '4.5NNN', '2NAA', '3NAA', '4NAA', '4NA', '2NNA', '3NNA'];
$master_tones_list['Natural Brown'][3] = ['2N', '3N', '4N', '5N', '2.5NNN', '3.5NNN', '4.5NNN', '5.5NNN', '2NAA', '3NAA', '4NAA', '5NAA', '4NA', '5NA', '2NNA', '3NNA'];
$master_tones_list['Natural Brown'][4] = ['2N', '3N', '4N', '5N', '6N', '2.5NNN', '3.5NNN', '4.5NNN', '5.5NNN', '6.5NNN', '2NAA', '3NAA', '4NAA', '5NAA', '6NAA', '4NA', '5NA', '6NA', '2NNA', '3NNA'];
$master_tones_list['Natural Brown'][5] = ['3N', '4N', '5N', '6N', '7N', '3.5NNN', '4.5NNN', '5.5NNN', '6.5NNN', '3NAA', '4NAA', '5NAA', '6NAA', '7NAA', '4NA', '5NA', '6NA', '7NA', '3NNA'];
$master_tones_list['Natural Brown'][6] = ['4N', '5N', '6N', '7N', '4.5NNN', '5.5NNN', '6.5NNN', '4NAA', '5NAA', '6NAA', '7NAA', '4NA', '5NA', '6NA', '7NA', 'NNA'];
$master_tones_list['Natural Brown'][7] = ['5N', '6N', '7N', '5.5NNN', '6.5NNN', '5NAA', '6NAA', '7NAA', '5NA', '6NA', '7NA', 'NNA'];
$master_tones_list['Golden Brown'][2] = ['4G', '4GA', '4NGM'];
$master_tones_list['Golden Brown'][3] = ['4G', '5G', '4GA', '5GA', '4NGM', '5NGM'];
$master_tones_list['Golden Brown'][4] = ['4G', '5G', '6G', '4GA', '5GA', '6GA', '4NGM', '5NGM', '6NGM'];
$master_tones_list['Golden Brown'][5] = ['4G', '5G', '6G', '7G', '4GA', '5GA', '6GA', '7GA', '4NGM', '5NGM', '6NGM', '7NGM'];
$master_tones_list['Golden Brown'][6] = ['4G', '5G', '6G', '7G', '4GA', '5GA', '6GA', '7GA', '4NGM', '5NGM', '6NGM', '7NGM'];
$master_tones_list['Golden Brown'][7] = ['5G', '6G', '7G', '5GA', '6GA', '7GA', '5NGM', '6NGM', '7NGM'];
$master_tones_list['True Brown'][2] = ['2N', '3N', '4N', 'AV'];
$master_tones_list['True Brown'][3] = ['2N', '3N', '4N', '5N', 'AV'];
$master_tones_list['True Brown'][4] = ['2N', '3N', '4N', '5N', '6N', 'AV'];
$master_tones_list['True Brown'][5] = ['3N', '4N', '5N', '6N', '7N', 'AV'];
$master_tones_list['True Brown'][6] = ['4N', '5N', '6N', '7N', 'AV'];
$master_tones_list['True Brown'][7] = ['5N', '6N', '7N', 'AV'];
$master_tones_list['Strawberry Blonde'][7] = ['7NRR', '8NRR', '9NRR', '7CC', '8CC', '9CC', '10NV', '7NV', '8NV', '9NV', '10NVA', '7NVA', '8NVA', '9NVA'];
$master_tones_list['Strawberry Blonde'][8] = ['7NRR', '8NRR', '9NRR', '7CC', '8CC', '9CC', '10NV', '7NV', '8NV', '9NV', '10NVA', '7NVA', '8NVA', '9NVA'];
$master_tones_list['Strawberry Blonde'][9] = ['7NRR', '8NRR', '9NRR', '7CC', '8CC', '9CC', '10NV', '7NV', '8NV', '9NV', '10NVA', '7NVA', '8NVA', '9NVA'];
$master_tones_list['Strawberry Blonde'][10] = ['7NRR', '8NRR', '9NRR', '7CC', '8CC', '9CC', '10NV', '7NV', '8NV', '9NV', '10NVA', '7NVA', '8NVA', '9NVA'];
$master_tones_list['Strawberry Blonde'][11] = ['7NRR', '8NRR', '9NRR', '7CC', '8CC', '9CC', '10NV', '7NV', '8NV', '9NV', '10NVA', '7NVA', '8NVA', '9NVA'];
$master_tones_list['Cool Blonde'][7] = ['10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA'];
$master_tones_list['Cool Blonde'][8] = ['10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA'];
$master_tones_list['Cool Blonde'][9] = ['10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA'];
$master_tones_list['Cool Blonde'][10] = ['10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA'];
$master_tones_list['Cool Blonde'][11] = ['10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA'];
$master_tones_list['Golden Blonde'][7] = ['10G', '7G', '8G', '9G', '10GA', '11GA', '7GA', '8GA', '9GA'];
$master_tones_list['Golden Blonde'][8] = ['10G', '7G', '8G', '9G', '10GA', '11GA', '7GA', '8GA', '9GA'];
$master_tones_list['Golden Blonde'][9] = ['10G', '7G', '8G', '9G', '10GA', '11GA', '7GA', '8GA', '9GA'];
$master_tones_list['Golden Blonde'][10] = ['10G', '7G', '8G', '9G', '10GA', '11GA', '7GA', '8GA', '9GA'];
$master_tones_list['Golden Blonde'][11] = ['10G', '7G', '8G', '9G', '10GA', '11GA', '7GA', '8GA', '9GA'];
$master_tones_list['Natural Blonde'][7] = ['10N', '11N', '7N', '8N', '9N', '10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA', '7NGM', '8NGM', '7NMG'];
$master_tones_list['Natural Blonde'][8] = ['10N', '11N', '7N', '8N', '9N', '10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA', '7NGM', '8NGM', '7NMG'];
$master_tones_list['Natural Blonde'][9] = ['10N', '11N', '7N', '8N', '9N', '10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA', '7NGM', '8NGM', '7NMG'];
$master_tones_list['Natural Blonde'][10] = ['10N', '11N', '7N', '8N', '9N', '10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA', '7NGM', '8NGM', '7NMG'];
$master_tones_list['Natural Blonde'][11] = ['10N', '11N', '7N', '8N', '9N', '10NAA', '7NAA', '8NAA', '9NAA', '10NA', '11NA', '7NA', '8NA', '9NA', '7NGM', '8NGM', '7NMG'];
$master_tones_list['True Red'][3] = ['5RC', '5NCR', '5NRR', '5RR'];
$master_tones_list['True Red'][4] = ['5RC', '6RC', '5NCR', '6NCR', '5NRR', '6NRR', '5RR', '6RR'];
$master_tones_list['True Red'][5] = ['5RC', '6RC', '7RC', '5NCR', '6NCR', '7NCR', '5NRR', '6NRR', '7NRR', '5RR', '6RR', '7RR'];
$master_tones_list['True Red'][6] = ['5RC', '6RC', '7RC', '8RC', '5NCR', '6NCR', '7NCR', '8NCR', '5NRR', '6NRR', '7NRR', '8NRR', '5RR', '6RR', '7RR', '8RR'];
$master_tones_list['True Red'][7] = ['5RC', '6RC', '7RC', '8RC', '5NCR', '6NCR', '7NCR', '8NCR', '5NRR', '6NRR', '7NRR', '8NRR', '9NRR', '5RR', '6RR', '7RR', '8RR'];
$master_tones_list['True Red'][8] = ['6RC', '7RC', '8RC', '6NCR', '7NCR', '8NCR', '6NRR', '7NRR', '8NRR', '9NRR', '6RR', '7RR', '8RR'];
$master_tones_list['True Red'][9] = ['7RC', '8RC', '7NCR', '8NCR', '7NRR', '8NRR', '9NRR', '7RR', '8RR'];
$master_tones_list['True Red'][10] = ['8RC', '8NCR', '8NRR', '9NRR', '8RR'];
$master_tones_list['True Red'][11] = ['RC', 'NCR', '9NRR', 'RR'];
$master_tones_list['Copper'][3] = ['4NCG', '5NCG', '5CC', '5RC', '5NCR'];
$master_tones_list['Copper'][4] = ['4NCG', '5NCG', '6NCG', '5CC', '6CC', '5RC', '6RC', '5NCR', '6NCR'];
$master_tones_list['Copper'][5] = ['4NCG', '5NCG', '6NCG', '7NCG', '5CC', '6CC', '7CC', '5RC', '6RC', '7RC', '5NCR', '6NCR', '7NCR'];
$master_tones_list['Copper'][6] = ['4NCG', '5NCG', '6NCG', '7NCG', '8NCG', '5CC', '6CC', '7CC', '8CC', '5RC', '6RC', '7RC', '8RC', '5NCR', '6NCR', '7NCR', '8NCR'];
$master_tones_list['Copper'][7] = ['5NCG', '6NCG', '7NCG', '8NCG', '5CC', '6CC', '7CC', '8CC', '9CC', '5RC', '6RC', '7RC', '8RC', '5NCR', '6NCR', '7NCR', '8NCR'];
$master_tones_list['Copper'][8] = ['6NCG', '7NCG', '8NCG', '6CC', '7CC', '8CC', '9CC', '6RC', '7RC', '8RC', '6NCR', '7NCR', '8NCR'];
$master_tones_list['Copper'][9] = ['7NCG', '8NCG', '7CC', '8CC', '9CC', '7RC', '8RC', '7NCR', '8NCR'];
$master_tones_list['Copper'][10] = ['8NCG', '8CC', '9CC', '8RC', '8NCR'];
$master_tones_list['Copper'][11] = ['NCG', '9CC', 'RC', 'NCR'];
$master_tones_list['Violet Red'][3] = ['5RV', 'NVA', 'NV'];
$master_tones_list['Violet Red'][4] = ['5RV', '6RV', '6NVA', 'NV'];
$master_tones_list['Violet Red'][5] = ['5RV', '6RV', '7RV', '6NVA', '7NVA', '7NV'];
$master_tones_list['Violet Red'][6] = ['5RV', '6RV', '7RV', '6NVA', '7NVA', '8NVA', '7NV', '8NV'];
$master_tones_list['Violet Red'][7] = ['5RV', '6RV', '7RV', '6NVA', '7NVA', '8NVA', '9NVA', '7NV', '8NV', '9NV'];
$master_tones_list['Violet Red'][8] = ['6RV', '7RV', '10NVA', '6NVA', '7NVA', '8NVA', '9NVA', '10NV', '7NV', '8NV', '9NV'];
$master_tones_list['Violet Red'][9] = ['7RV', '10NVA', '7NVA', '8NVA', '9NVA', '10NV', '7NV', '8NV', '9NV'];
$master_tones_list['Violet Red'][10] = ['RV', '10NVA', '8NVA', '9NVA', '10NV', '8NV', '9NV'];
$master_tones_list['Violet Red'][11] = ['RV', '10NVA', '9NVA', '10NV', '9NV'];
$master_tones_list['Mahogany Red'][3] = ['4NGM', '5NGM', '3NMG', '4NMG', '5NMG'];
$master_tones_list['Mahogany Red'][4] = ['4NGM', '5NGM', '6NGM', '3NMG', '4NMG', '5NMG', '6NMG'];
$master_tones_list['Mahogany Red'][5] = ['4NGM', '5NGM', '6NGM', '7NGM', '3NMG', '4NMG', '5NMG', '6NMG', '7NMG'];
$master_tones_list['Mahogany Red'][6] = ['4NGM', '5NGM', '6NGM', '7NGM', '8NGM', '4NMG', '5NMG', '6NMG', '7NMG'];
$master_tones_list['Mahogany Red'][7] = ['5NGM', '6NGM', '7NGM', '8NGM', '5NMG', '6NMG', '7NMG'];
$master_tones_list['Mahogany Red'][8] = ['6NGM', '7NGM', '8NGM', '6NMG', '7NMG'];
$master_tones_list['Mahogany Red'][9] = ['7NGM', '8NGM', '7NMG'];
$master_tones_list['Mahogany Red'][10] = ['8NGM', 'NMG'];
$master_tones_list['Mahogany Red'][11] = ['NGM', 'NMG'];


$uploads = wp_upload_dir();

if ($dir = opendir($uploads['basedir'] . '/color_swatches')) {
  $images = array();
  while (false !== ($file = readdir($dir))) {
    if ($file != "." && $file != "..") {
      /*  $newf = str_replace("__edit","edit",$file);
      rename($uploads['basedir'].'/color_swatches/'. $file,$uploads['basedir'].'/color_swatches/' . $newf);*/
      $images[] = $file;
    }
  }
  closedir($dir);
}
//echo '<pre>';
//print_r($master_tones_list);
?>
<?php
global $wpdb;
?>
<div class="<?php echo esc_attr(visualcomposerstarter_get_content_container_class()); ?>">
  <div class="content-wrapper">
    <div class="row">
      <div class="<?php echo esc_attr(visualcomposerstarter_get_maincontent_block_class()); ?>">
        <div class="main-content">
          <form id="questionaire-form" class="questionaire-form">
            <div class="row main-form dont-know-color-brand">
              <!-- progressbar -->
              <ul id="progressbar">
                <li class="active"></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
              </ul>
              <input type="button" name="restart" class="action-button restart" value="Restart Quiz">
              <fieldset class="required brand-section screen-step screen-step1">
                <div class="bg brand-name">
                  <h6 class="que">What is the brand of your current hair color?</h6>
                  <label>
                    <select id="brand-name" name="brand-name">
                      <option>Select Brand</option>
                      <?php
                      $customers = $wpdb->get_results("SELECT * FROM brand_name");
                      foreach ($customers as $rows) {
                        echo "<option value='" . $rows->Brand_Id . "'>" . $rows->Brand_Name . "</option>";
                      }
                      ?>
                      <option value='999'>Other</option>
                    </select>

                  </label>
                </div>
                <div class="bg brand-name-product hidden">
                  <h6 class="que">What is the shade of the color you use?</h6>
                  <label>
                    <select id="color-shade-product" name="color-shade-product">
                      <option value='0'>Select Shade</option>
                    </select>
                  </label>
                </div>
                <input type="button" name="next" class="next action-button hidden" value="Confirm" />
              </fieldset>

              <fieldset class="required touch-or-all screen-step screen-step2">
                <div class="bg color-type q-1">
                  <h6 class="que">Select root touch up or all over color?</h6>
                  <div>
                    <label>
                      <input type="radio" name="color-type" class="first" value="Root Touch Up">
                      <span class="input-label">Root Touch Up</span>
                    </label>
                    <label>
                      <input type="radio" name="color-type" class="last" value="All Over Color">
                      <span class="input-label">All Over Color</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>

              <fieldset class="required  screen-step screen-step3">
                <div class="currently-color sub-que">
                  <div class="bg no-count currently-coloring">
                    <h6 class="que  que2A">What type of hair color do you currently have?</h6>
                    <div>
                      <label>
                        <input type="radio" name="currently-coloring" class="first" value="All Over">
                        <span class="input-label">All Over</span>
                      </label>

                      <label>
                        <input type="radio" name="currently-coloring" class="4-wk" value="All Over Lightening">
                        <span class="input-label">All Over Lightening</span>
                      </label>

                      <label>
                        <input type="radio" name="currently-coloring" class="6-wk" value="Henna">
                        <span class="input-label">Henna</span>
                      </label>

                      <label>
                        <input type="radio" name="currently-coloring" class="8-wk" value="Color & High Lights">
                        <span class="input-label">Color & High Lights</span>
                      </label>

                      <label>
                        <input type="radio" name="currently-coloring" class="6-month currently" value="High Lights Only">
                        <span class="input-label">High Lights Only</span>
                      </label>
                      <label>
                        <input type="radio" name="currently-coloring" class="6-month last" value="Virgin">
                        <span class="input-label">None Virgin</span>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="no-current-color sub-que hidden">
                  <h6 class="que  que2B">Are you sure you have no color in your hair?</h6>
                  <div>
                    <label>
                      <span class="input-label have-some-color">No, I have some color</span>
                    </label>
                    <label>
                      <span class="input-label no-color-sure">Yes, I'm sure</span>
                    </label>
                  </div>
                </div>

                <input type="button" name="previous" class="action-button previous" value="Previous" />
                <input type="button" name="next" class="action-button next" id="currently-coloring-btn-next" value="Confirm" />
              </fieldset>

              <fieldset class="required touch-or-all screen-step screen-step4">
                <div class="bg color-type q-1">
                  <h6 class="que">Do you want to keep you highlights or cover them?</h6>
                  <div>
                    <label>
                      <input type="radio" name="highlights" class="first" value="Keep">
                      <span class="input-label">Keep</span>
                    </label>
                    <label>
                      <input type="radio" name="highlights" class="last" value="Cover">
                      <span class="input-label">Cover</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next next3 action-button" value="Next" />
              </fieldset>

              <fieldset class="service-checkbox screen-step screen-step5">
                <div class="bg services q-2">
                  <h6 class="que">Have you used any of the following treatment within the last 2 months?</h6>
                  <div class="note">Choose most recent.</div>
                  <div>
                    <label>
                      <input type="checkbox" name="recent-services" value="Keratin" class="recent-services">
                      <span class="input-label">Keratin</span>
                    </label>
                    <label>
                      <input type="checkbox" name="recent-services" value="Hair Color Remover" class="recent-services">
                      <span class="input-label">Hair Color Remover</span>
                    </label>

                    <label>
                      <input type="checkbox" name="recent-services" class="recent-services check-uncheck" value="Relaxer">
                      <span class="input-label">Relaxer</span>
                    </label>
                    <label>
                      <input type="checkbox" name="recent-services" class="recent-services" value="Perm">
                      <span class="input-label">Perm</span>
                    </label>
                    <label>
                      <input type="checkbox" name="recent-services" class="recent-services" value="None">
                      <span class="input-label">None</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Confirm" />
              </fieldset>

              <fieldset class="required  screen-step screen-step6">
                <div class="bg no-count last-coloring">
                  <h6 class="que  que2A"> When was the last time you did a root touch up?</h6>
                  <div>
                    <label>
                      <input type="radio" name="last-coloring" class="first" value="2 WKS">
                      <span class="input-label">2 Weeks</span>
                    </label>

                    <label>
                      <input type="radio" name="last-coloring" class="4-wk" value="4 WKS">
                      <span class="input-label">4 Weeks</span>
                    </label>

                    <label>
                      <input type="radio" name="last-coloring" class="6-wk" value="6 WKS">
                      <span class="input-label"> 6 Weeks</span>
                    </label>

                    <label>
                      <input type="radio" name="last-coloring" class="8-wk" value="8 WKS">
                      <span class="input-label"> 8 Weeks</span>
                    </label>
                  </div>
                </div>

                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Confirm" />
              </fieldset>

              <fieldset class="required screen-step screen-step7">
                <div class="bg q-10">
                  <h6 class="que">What is the condition of your hair?</h6>
                  <div class="custom-tooltips">
                    <label>
                      <input type="radio" name="hair-condition" class="first" value="Healthy">
                      <span class="input-label">Healthy</span>
                    </label>
                    <label class="got-custom-tooltip">
                      <input type="radio" name="hair-condition" class="sec" value="Porous">
                      <span class="input-label">Porous</span>
                    </label>
                    <div class="custom-tooltip">Refers to how well your hair is able to absorb and hold moisture; porous hair has a hard time holding in color.</div>
                  </div>
                  <div class="custom-tooltips">
                    <label class="got-custom-tooltip">
                      <input type="radio" name="hair-condition" class="last" value="Damaged">
                      <span class="input-label">Damaged</span>
                    </label>
                    <div class="custom-tooltip">this may be caused by too many chemical hair services; hair breaks from being too brittle or dry.</div>
                  </div>
                </div>
                <div class="hair-condition-damaged sub-que hidden">
                  <h6 class="que  que2B">Is your hair?</h6>
                  <div>
                    <label>
                      <input type="radio" name="hair-condition-damaged" class="last" value="Dry">
                      <span class="input-label hair-condition-dry">Dry</span>
                    </label>
                    <label>
                      <input type="radio" name="hair-condition-damaged" class="last" value="Breakage">
                      <span class="input-label hair-condition-breakage">Breakage</span>
                    </label>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>

              <fieldset class="required  screen-step screen-step8">

                <div class="select-hair-length">
                  <div class="bg hair-length no-count">
                    <h6 class="que  que2B"> Select length of hair?</h6>
                    <div>
                      <label>
                        <input type="radio" name="hair-length" class="first short" value="Very Short">
                        <span class="input-label">Very Short</span>
                      </label>
                      <label>
                        <input type="radio" name="hair-length" class="first short" value="Short">
                        <span class="input-label">Short</span>
                      </label>
                      <label>
                        <input type="radio" name="hair-length" class="mid medium" value="Shoulder Length">
                        <span class="input-label">Shoulder Length</span>
                      </label>
                      <label>
                        <input type="radio" name="hair-length" class="last long" value="Long">
                        <span class="input-label">Long</span>
                      </label>
                      <label>
                        <input type="radio" name="hair-length" class="last long" value="Very Long">
                        <span class="input-label">Very Long</span>
                      </label>
                    </div>
                  </div>
                </div>

                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Confirm" />
              </fieldset>

              <fieldset class="required screen-step screen-step9">
                <div class="bg how-much-gray">
                  <h6 class="que mb-1">How much gray do you have?</h6>
                  <div class="note">To be specific - look at your hair at the roots as it grows in.</div>
                  <div>
                    <label>
                      <input type="radio" name="much-gray" class="first" value="None">
                      <span class="input-label">None or hardly any</span>
                    </label>
                    <label>
                      <input type="radio" name="much-gray" class="sec" value="Few">
                      <span class="input-label"">Some</span>
                    </label>
                  </div>
                  <div>
                    <label>
                      <input type=" radio" name="much-gray" class="neutral" value="Lots">
                        <span class="input-label">Mostly Gray</span>
                    </label>
                    <label>
                      <input type="radio" name="much-gray" class="last" value="All">
                      <span class="input-label">All Gray</span>
                    </label>
                  </div>
                  <div class="error-msg" id="err2"></div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>

              <fieldset class="required  screen-step screen-step10">
                <div class="hair-resistant">
                  <h6 class="que  que2B"> Is your hair/gray resistance to color?</h6>
                  <div>
                    <label>
                      <input type="radio" name="hair-resistant" class="first resistant" value="Yes">
                      <span class="input-label">Yes</span>
                    </label>
                    <label>
                      <input type="radio" name="hair-resistant" class="first not-resistant" value="No">
                      <span class="input-label">No</span>
                    </label>
                  </div>
                </div>

                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Confirm" />
              </fieldset>

              <fieldset class="required  screen-step screen-step11">
                <div class="natural-texture sub-que">
                  <h6 class="que  que2B"> What is your natural texture?</h6>
                  <div>
                    <label>
                      <input type="radio" name="natural-texture" class="first resistant" value="Fine">
                      <span class="input-label">Fine</span>
                    </label>
                    <label>
                      <input type="radio" name="natural-texture" class="first not-resistant" value="Normal">
                      <span class="input-label">Normal</span>
                    </label>
                    <label>
                      <input type="radio" name="natural-texture" class="first resistant" value="Coarse">
                      <span class="input-label">Coarse</span>
                    </label>
                    <label>
                      <input type="radio" name="natural-texture" class="first not-resistant" value="Very Coarse">
                      <span class="input-label">Very Coarse</span>
                    </label>
                  </div>
                </div>

                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button preview" value="Confirm" />
              </fieldset>

              <fieldset class="summary summary1 screen-step screen-step14">
                <h5 style="color:#000;margin-bottom:20px;">Questionnaire Summary</h5>
                <div class="row">
                  <div class="col-sm-4">
                    <div class="summary-field">Date:<span class="current_date"></span></div>
                    <div class="summary-field">Kit Choice:<span class="kit_choice"></span></div>
                    <div class="summary-field">Formula:<span class="formula"></span></div>
                  </div>
                  <div class="col-sm-4">
                    <input placeholder="What is your name?" id="user-name" name="user-name" type="text" required>
                    <input placeholder="Mobile number?" id="user-phone" name="user-phone" type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required>
                    <input placeholder="Email" id="user-email" name="user-email" type="email" required>
                  </div>
                  <div class="col-sm-4">
                    <input type="button" id="user-fields" name="submit" class="text-center action-button" value="Submit" />
                  </div>
                </div>
              </fieldset>

              <fieldset class="summary screen-step screen-step15">
                <h5 style="color:#000;margin-bottom:20px;">Questionnaire Summary</h5>
                <div class="row">
                  <div class="col-sm-4">
                    <div class="summary-field">Date:<span class="current_date"></span></div>
                    <div class="summary-field">Kit Choice:<span class="kit_choice"></span></div>
                    <div class="summary-field">Formula:<span class="formula"></span></div>
                  </div>
                  <div class="col-sm-4">
                    <div class="summary-field">Name:<span class="user-name"></span></div>
                    <div class="summary-field">Phone:<span class="user-phone"></span></div>
                    <div class="summary-field">Email:<span class="user-email"></span></div>
                  </div>
                  <div class="col-sm-4">
                    <div class="alert alert-success" role="alert">
                      <strong>Well done!</strong>
                      You successfully read this important alert message
                    </div>
                  </div>
                </div>
                <fieldset>
                  <legend>Just For Dev</legend>
                  <div id="json_output"></div>
                </fieldset>
              </fieldset>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" type="text/javascript"></script>
<script>
  //var pageId="<?php echo get_the_ID(); ?>";
  //console.log(pageId);
  document.getElementById("questionaire-form").reset();
  /* color formula */
  var state = {};
  var master_tones_list = {};
  master_tones_list['Natural Black'] = [];
  master_tones_list['Natural Black'][0] = [];
  master_tones_list['Natural Black'][1] = [];
  master_tones_list['Violet Black'] = [];
  master_tones_list['Violet Black'][0] = [];
  master_tones_list['Violet Black'][1] = [];
  master_tones_list['Blue Black'] = [];
  master_tones_list['Blue Black'][0] = [];
  master_tones_list['Blue Black'][1] = [];
  master_tones_list['Mahogany Brown'] = [];
  master_tones_list['Mahogany Brown'][0] = [];
  master_tones_list['Mahogany Brown'][1] = [];
  master_tones_list['Golden Brown'] = [];
  master_tones_list['Golden Brown'][0] = [];
  master_tones_list['Golden Brown'][1] = [];
  master_tones_list['True Brown'] = [];
  master_tones_list['True Brown'][0] = [];
  master_tones_list['True Brown'][1] = [];
  master_tones_list['Natural Brown'] = [];
  master_tones_list['Natural Brown'][0] = [];
  master_tones_list['Natural Brown'][1] = [];
  master_tones_list['Cool Brown'] = [];
  master_tones_list['Cool Brown'][0] = [];
  master_tones_list['Cool Brown'][1] = [];
  master_tones_list['Golden Blonde'] = [];
  master_tones_list['Golden Blonde'][0] = [];
  master_tones_list['Golden Blonde'][1] = [];
  master_tones_list['Cool Blonde'] = [];
  master_tones_list['Cool Blonde'][0] = [];
  master_tones_list['Cool Blonde'][1] = [];
  master_tones_list['Natural Blonde'] = [];
  master_tones_list['Natural Blonde'][0] = [];
  master_tones_list['Natural Blonde'][1] = [];
  master_tones_list['Strawberry Blonde'] = [];
  master_tones_list['Strawberry Blonde'][0] = [];
  master_tones_list['Strawberry Blonde'][1] = [];
  master_tones_list['True Red'] = [];
  master_tones_list['True Red'][0] = [];
  master_tones_list['True Red'][1] = [];
  master_tones_list['Copper'] = [];
  master_tones_list['Copper'][0] = [];
  master_tones_list['Copper'][1] = [];
  master_tones_list['Mahogany'] = [];
  master_tones_list['Mahogany'][0] = [];
  master_tones_list['Mahogany'][1] = [];
  master_tones_list['Violet Red'] = [];
  master_tones_list['Violet Red'][0] = [];
  master_tones_list['Violet Red'][1] = [];
  master_tones_list['Natural Black'][0][1] = ['1N', '2N', '3N', '2.5NNN', '3.5NNN'];
  master_tones_list['Natural Black'][1][1] = ['1N'];
  master_tones_list['Natural Black'][0][2] = ['1N', '2N', '3N', '2.5NNN', '3.5NNN'];
  master_tones_list['Natural Black'][1][2] = ['1N', '2N', '2.5NNN'];
  master_tones_list['Natural Black'][0][3] = ['1N', '2N', '3N', '2.5NNN', '3.5NNN'];
  master_tones_list['Natural Black'][1][3] = ['1N', '2N', '3N', '2.5NNN', '3.5NNN'];
  master_tones_list['Violet Black'][0][1] = ['3NVV'];
  master_tones_list['Violet Black'][1][1] = [];
  master_tones_list['Violet Black'][0][2] = ['3NVV'];
  master_tones_list['Violet Black'][1][2] = [];
  master_tones_list['Violet Black'][0][3] = ['3NVV'];
  master_tones_list['Violet Black'][1][3] = ['3NVV'];
  master_tones_list['Blue Black'][0][1] = ['2NNA', '3NNA'];
  master_tones_list['Blue Black'][1][1] = [];
  master_tones_list['Blue Black'][0][2] = ['2NNA', '3NNA'];
  master_tones_list['Blue Black'][1][2] = ['2NNA'];
  master_tones_list['Blue Black'][0][3] = ['2NNA', '3NNA'];
  master_tones_list['Blue Black'][1][3] = ['2NNA', '3NNA'];
  master_tones_list['Mahogany Brown'][0][4] = ['4NGM', '5NGM', '6NGM', '4NMG', '5NMG', '6NMG'];
  master_tones_list['Mahogany Brown'][1][4] = ['4NGM', '4NMG'];
  master_tones_list['Mahogany Brown'][0][5] = ['4NGM', '5NGM', '6NGM', '7NGM', '4NMG', '5NMG', '6NMG', '7NMG'];
  master_tones_list['Mahogany Brown'][1][5] = ['4NGM', '5NGM', '4NMG', '5NMG'];
  master_tones_list['Mahogany Brown'][0][6] = ['4NGM', '5NGM', '6NGM', '7NGM', '4NMG', '5NMG', '6NMG', '7NMG'];
  master_tones_list['Mahogany Brown'][1][6] = ['4NGM', '5NGM', '6NGM', '4NMG', '5NMG', '6NMG'];
  master_tones_list['Mahogany Brown'][0][7] = ['5NGM', '6NGM', '7NGM', '5NMG', '6NMG', '7NMG'];
  master_tones_list['Mahogany Brown'][1][7] = ['5NGM', '6NGM', '7NGM', '5NMG', '6NMG', '7NMG'];
  master_tones_list['Golden Brown'][0][4] = ['4G', '5G', '6G', '4GA', '5GA', '6GA', '4NGM', '5NGM', '6NGM'];
  master_tones_list['Golden Brown'][1][4] = ['4G', '4GA', '4NGM'];
  master_tones_list['Golden Brown'][0][5] = ['4G', '5G', '6G', '7G', '4GA', '5GA', '6GA', '7GA', '4NGM', '5NGM', '6NGM', '7NGM'];
  master_tones_list['Golden Brown'][1][5] = ['4G', '5G', '4GA', '5GA', '4NGM', '5NGM'];
  master_tones_list['Golden Brown'][0][6] = ['4G', '5G', '6G', '7G', '4GA', '5GA', '6GA', '7GA', '4NGM', '5NGM', '6NGM', '7NGM'];
  master_tones_list['Golden Brown'][1][6] = ['4G', '5G', '6G', '4GA', '5GA', '6GA', '4NGM', '5NGM', '6NGM'];
  master_tones_list['Golden Brown'][0][7] = ['5G', '6G', '7G', '5GA', '6GA', '7GA', '5NGM', '6NGM', '7NGM'];
  master_tones_list['Golden Brown'][1][7] = ['5G', '6G', '7G', '5GA', '6GA', '7GA', '5NGM', '6NGM', '7NGM'];
  master_tones_list['True Brown'][0][4] = ['4N', '5N', '6N'];
  master_tones_list['True Brown'][1][4] = ['4N'];
  master_tones_list['True Brown'][0][5] = ['4N', '5N', '6N', '7N'];
  master_tones_list['True Brown'][1][5] = ['4N', '5N'];
  master_tones_list['True Brown'][0][6] = ['4N', '5N', '6N', '7N'];
  master_tones_list['True Brown'][1][6] = ['4N', '5N', '6N'];
  master_tones_list['True Brown'][0][7] = ['5N', '6N', '7N'];
  master_tones_list['True Brown'][1][7] = ['5N', '6N', '7N'];
  master_tones_list['Natural Brown'][0][4] = ['4N', '5N', '6N', '4.5NNN', '5.5NNN', '6.5NNN', '4NAA', '5NAA', '6NAA', '4NA', '5NA', '6NA'];
  master_tones_list['Natural Brown'][1][4] = ['4N', '4.5NNN', '4NAA', '4NA'];
  master_tones_list['Natural Brown'][0][5] = ['4N', '5N', '6N', '7N', '4.5NNN', '5.5NNN', '6.5NNN', '4NAA', '5NAA', '6NAA', '7NAA', '4NA', '5NA', '6NA', '7NA'];
  master_tones_list['Natural Brown'][1][5] = ['4N', '5N', '4.5NNN', '5.5NNN', '4NAA', '5NAA', '4NA', '5NA'];
  master_tones_list['Natural Brown'][0][6] = ['4N', '5N', '6N', '7N', '4.5NNN', '5.5NNN', '6.5NNN', '4NAA', '5NAA', '6NAA', '7NAA', '4NA', '5NA', '6NA', '7NA'];
  master_tones_list['Natural Brown'][1][6] = ['4N', '5N', '6N', '4.5NNN', '5.5NNN', '6.5NNN', '4NAA', '5NAA', '6NAA', '4NA', '5NA', '6NA'];
  master_tones_list['Natural Brown'][0][7] = ['5N', '6N', '7N', '5.5NNN', '6.5NNN', '5NAA', '6NAA', '7NAA', '5NA', '6NA', '7NA'];
  master_tones_list['Natural Brown'][1][7] = ['5N', '6N', '7N', '5.5NNN', '6.5NNN', '5NAA', '6NAA', '7NAA', '5NA', '6NA', '7NA'];
  master_tones_list['Cool Brown'][0][4] = ['4N', '5N', '6N', '4.5NNN', '5.5NNN', '6.5NNN', '4NA', '5NA', '6NA', '4NAA', '5NAA', '6NAA'];
  master_tones_list['Cool Brown'][1][4] = ['4N', '4.5NNN', '4NA', '4NAA'];
  master_tones_list['Cool Brown'][0][5] = ['4N', '5N', '6N', '7N', '4.5NNN', '5.5NNN', '6.5NNN', '4NA', '5NA', '6NA', '7NA', '4NAA', '5NAA', '6NAA', '7NAA'];
  master_tones_list['Cool Brown'][1][5] = ['4N', '5N', '4.5NNN', '5.5NNN', '4NA', '5NA', '4NAA', '5NAA'];
  master_tones_list['Cool Brown'][0][6] = ['4N', '5N', '6N', '7N', '4.5NNN', '5.5NNN', '6.5NNN', '4NA', '5NA', '6NA', '7NA', '4NAA', '5NAA', '6NAA', '7NAA'];
  master_tones_list['Cool Brown'][1][6] = ['4N', '5N', '6N', '4.5NNN', '5.5NNN', '6.5NNN', '4NA', '5NA', '6NA', '4NAA', '5NAA', '6NAA'];
  master_tones_list['Cool Brown'][0][7] = ['5N', '6N', '7N', '5.5NNN', '6.5NNN', '5NA', '6NA', '7NA', '5NAA', '6NAA', '7NAA'];
  master_tones_list['Cool Brown'][1][7] = ['5N', '6N', '7N', '5.5NNN', '6.5NNN', '5NA', '6NA', '7NA', '5NAA', '6NAA', '7NAA'];
  master_tones_list['Golden Blonde'][0][8] = ['10G', '8G', '9G', '10GA', '8GA', '9GA'];
  master_tones_list['Golden Blonde'][1][8] = ['8G', '8GA'];
  master_tones_list['Golden Blonde'][0][9] = ['10G', '8G', '9G', '10GA', '11GA', '8GA', '9GA'];
  master_tones_list['Golden Blonde'][1][9] = ['8G', '9G', '8GA', '9GA'];
  master_tones_list['Golden Blonde'][1][9] = [];
  master_tones_list['Golden Blonde'][0][10] = ['10G', '8G', '9G', '10GA', '11GA', '8GA', '9GA'];
  master_tones_list['Golden Blonde'][1][10] = ['10G', '8G', '9G', '10GA', '8GA', '9GA'];
  master_tones_list['Golden Blonde'][0][11] = ['10G', '9G', '10GA', '11GA', '9GA'];
  master_tones_list['Golden Blonde'][1][11] = ['10G', '9G', '10GA', '11GA', '9GA'];
  master_tones_list['Cool Blonde'][0][8] = ['10NAA', '8NAA', '9NAA', '10NA', '8NA', '9NA'];
  master_tones_list['Cool Blonde'][1][8] = ['8NAA', '8NA'];
  master_tones_list['Cool Blonde'][0][9] = ['10NAA', '8NAA', '9NAA', '10NA', '11NA', '8NA', '9NA'];
  master_tones_list['Cool Blonde'][1][9] = ['8NAA', '9NAA', '8NA', '9NA'];
  master_tones_list['Cool Blonde'][0][10] = ['10NAA', '8NAA', '9NAA', '10NA', '11NA', '8NA', '9NA'];
  master_tones_list['Cool Blonde'][1][10] = ['10NAA', '8NAA', '9NAA', '10NA', '8NA', '9NA'];
  master_tones_list['Cool Blonde'][0][11] = ['10NAA', '9NAA', '10NA', '11NA', '9NA'];
  master_tones_list['Cool Blonde'][1][11] = ['10NAA', '9NAA', '10NA', '11NA', '9NA'];
  master_tones_list['Natural Blonde'][0][8] = ['10N', '8N', '9N', '10NAA', '8NAA', '9NAA', '10NA', '8NA', '9NA', '8NGM'];
  master_tones_list['Natural Blonde'][1][8] = ['8N', '8NAA', '8NA', '8NGM'];
  master_tones_list['Natural Blonde'][0][9] = ['10N', '11N', '8N', '9N', '10NAA', '8NAA', '9NAA', '10NA', '11NA', '8NA', '9NA', '8NGM'];
  master_tones_list['Natural Blonde'][1][9] = ['8N', '9N', '8NAA', '9NAA', '8NA', '9NA', '8NGM'];
  master_tones_list['Natural Blonde'][0][10] = ['10N', '11N', '8N', '9N', '10NAA', '8NAA', '9NAA', '10NA', '11NA', '8NA', '9NA', '8NGM'];
  master_tones_list['Natural Blonde'][1][10] = ['10N', '8N', '9N', '10NAA', '8NAA', '9NAA', '10NA', '8NA', '9NA', '8NGM'];
  master_tones_list['Natural Blonde'][0][11] = ['10N', '11N', '9N', '10NAA', '9NAA', '10NA', '11NA', '9NA'];
  master_tones_list['Natural Blonde'][1][11] = ['10N', '11N', '9N', '10NAA', '9NAA', '10NA', '11NA', '9NA'];
  master_tones_list['Strawberry Blonde'][0][8] = ['8NRR', '9NRR', '8CC', '9CC', '10NV', '8NV', '9NV', '10NVA', '8NVA', '9NVA', '8NRR', '9NRR'];
  master_tones_list['Strawberry Blonde'][1][8] = ['8NRR', '8CC', '8NV', '8NVA', '8NRR'];
  master_tones_list['Strawberry Blonde'][0][9] = ['8NRR', '9NRR', '8CC', '9CC', '10NV', '8NV', '9NV', '10NVA', '8NVA', '9NVA', '8NRR', '9NRR'];
  master_tones_list['Strawberry Blonde'][1][9] = ['8NRR', '9NRR', '8CC', '9CC', '8NV', '9NV', '8NVA', '9NVA', '8NRR', '9NRR'];
  master_tones_list['Strawberry Blonde'][0][10] = ['8NRR', '9NRR', '8CC', '9CC', '10NV', '8NV', '9NV', '10NVA', '8NVA', '9NVA', '8NRR', '9NRR'];
  master_tones_list['Strawberry Blonde'][1][10] = ['8NRR', '9NRR', '8CC', '9CC', '10NV', '8NV', '9NV', '10NVA', '8NVA', '9NVA', '8NRR', '9NRR'];
  master_tones_list['Strawberry Blonde'][0][11] = ['9NRR', '9CC', '10NV', '9NV', '10NVA', '9NVA', '9NRR'];
  master_tones_list['Strawberry Blonde'][1][11] = ['9NRR', '9CC', '10NV', '9NV', '10NVA', '9NVA', '9NRR'];
  master_tones_list['True Red'][0][1] = [];
  master_tones_list['True Red'][1][1] = [];
  master_tones_list['True Red'][0][2] = [];
  master_tones_list['True Red'][1][2] = [];
  master_tones_list['True Red'][0][3] = ['5RC', '5NCR', '5NRR', '5RR'];
  master_tones_list['True Red'][1][3] = [];
  master_tones_list['True Red'][0][4] = ['5RC', '6RC', '5NCR', '6NCR', '5NRR', '6NRR', '5RR', '6RR'];
  master_tones_list['True Red'][1][4] = [];
  master_tones_list['True Red'][0][5] = ['5RC', '6RC', '7RC', '5NCR', '6NCR', '7NCR', '5NRR', '6NRR', '7NRR', '5RR', '6RR', '7RR'];
  master_tones_list['True Red'][1][5] = ['5RC', '5NCR', '5NRR', '5RR'];
  master_tones_list['True Red'][0][6] = ['5RC', '6RC', '7RC', '8RC', '5NCR', '6NCR', '7NCR', '8NCR', '5NRR', '6NRR', '7NRR', '8NRR', '5RR', '6RR', '7RR', '8RR'];
  master_tones_list['True Red'][1][6] = ['5RC', '6RC', '5NCR', '6NCR', '5NRR', '6NRR', '5RR', '6RR'];
  master_tones_list['True Red'][0][7] = ['5RC', '6RC', '7RC', '8RC', '5NCR', '6NCR', '7NCR', '8NCR', '5NRR', '6NRR', '7NRR', '8NRR', '9NRR', '5RR', '6RR', '7RR', '8RR'];
  master_tones_list['True Red'][1][7] = ['5RC', '6RC', '7RC', '5NCR', '6NCR', '7NCR', '5NRR', '6NRR', '7NRR', '5RR', '6RR', '7RR'];
  master_tones_list['True Red'][0][8] = ['6RC', '7RC', '8RC', '6NCR', '7NCR', '8NCR', '6NRR', '7NRR', '8NRR', '9NRR', '6RR', '7RR', '8RR'];
  master_tones_list['True Red'][1][8] = ['6RC', '7RC', '8RC', '6NCR', '7NCR', '8NCR', '6NRR', '7NRR', '8NRR', '6RR', '7RR', '8RR'];
  master_tones_list['True Red'][0][9] = ['7RC', '8RC', '7NCR', '8NCR', '7NRR', '8NRR', '9NRR', '7RR', '8RR'];
  master_tones_list['True Red'][1][9] = ['7RC', '8RC', '7NCR', '8NCR', '7NRR', '8NRR', '9NRR', '7RR', '8RR'];
  master_tones_list['True Red'][0][10] = ['8RC', '8NCR', '8NRR', '9NRR', '8RR'];
  master_tones_list['True Red'][1][10] = ['8RC', '8NCR', '8NRR', '9NRR', '8RR'];
  master_tones_list['True Red'][0][11] = ['9NRR'];
  master_tones_list['True Red'][1][11] = ['9NRR'];
  master_tones_list['Copper'][0][1] = [];
  master_tones_list['Copper'][1][1] = [];
  master_tones_list['Copper'][0][2] = ['4NCG'];
  master_tones_list['Copper'][1][2] = [];
  master_tones_list['Copper'][0][3] = ['4NCG', '5NCG', '5CC', '5RC', '5NCR'];
  master_tones_list['Copper'][1][3] = [];
  master_tones_list['Copper'][0][4] = ['4NCG', '5NCG', '6NCG', '5CC', '6CC', '5RC', '6RC', '5NCR', '6NCR'];
  master_tones_list['Copper'][1][4] = ['4NCG'];
  master_tones_list['Copper'][0][5] = ['4NCG', '5NCG', '6NCG', '7NCG', '5CC', '6CC', '7CC', '5RC', '6RC', '7RC', '5NCR', '6NCR', '7NCR'];
  master_tones_list['Copper'][1][5] = ['4NCG', '5NCG', '5CC', '5RC', '5NCR'];
  master_tones_list['Copper'][0][6] = ['4NCG', '5NCG', '6NCG', '7NCG', '8NCG', '5CC', '6CC', '7CC', '8CC', '5RC', '6RC', '7RC', '8RC', '5NCR', '6NCR', '7NCR', '8NCR'];
  master_tones_list['Copper'][1][6] = ['4NCG', '5NCG', '6NCG', '5CC', '6CC', '5RC', '6RC', '5NCR', '6NCR'];
  master_tones_list['Copper'][0][7] = ['5NCG', '6NCG', '7NCG', '8NCG', '5CC', '6CC', '7CC', '8CC', '9CC', '5RC', '6RC', '7RC', '8RC', '5NCR', '6NCR', '7NCR', '8NCR'];
  master_tones_list['Copper'][1][7] = ['5NCG', '6NCG', '7NCG', '5CC', '6CC', '7CC', '5RC', '6RC', '7RC', '5NCR', '6NCR', '7NCR'];
  master_tones_list['Copper'][0][8] = ['6NCG', '7NCG', '8NCG', '6CC', '7CC', '8CC', '9CC', '6RC', '7RC', '8RC', '6NCR', '7NCR', '8NCR'];
  master_tones_list['Copper'][1][8] = ['6NCG', '7NCG', '8NCG', '6CC', '7CC', '8CC', '6RC', '7RC', '8RC', '6NCR', '7NCR', '8NCR'];
  master_tones_list['Copper'][0][9] = ['7NCG', '8NCG', '7CC', '8CC', '9CC', '7RC', '8RC', '7NCR', '8NCR'];
  master_tones_list['Copper'][1][9] = ['7NCG', '8NCG', '7CC', '8CC', '9CC', '7RC', '8RC', '7NCR', '8NCR'];
  master_tones_list['Copper'][0][10] = ['8NCG', '8CC', '9CC', '8RC', '8NCR'];
  master_tones_list['Copper'][1][10] = ['8NCG', '8CC', '9CC', '8RC', '8NCR'];
  master_tones_list['Copper'][0][11] = ['9CC'];
  master_tones_list['Copper'][1][11] = ['9CC'];
  master_tones_list['Mahogany'][0][1] = [];
  master_tones_list['Mahogany'][1][1] = [];
  master_tones_list['Mahogany'][0][2] = ['4NGM'];
  master_tones_list['Mahogany'][1][2] = [];
  master_tones_list['Mahogany'][0][3] = ['4NGM', '5NGM'];
  master_tones_list['Mahogany'][1][3] = [];
  master_tones_list['Mahogany'][0][4] = ['4NGM', '5NGM', '6NGM'];
  master_tones_list['Mahogany'][1][4] = ['4NGM'];
  master_tones_list['Mahogany'][0][5] = ['4NGM', '5NGM', '6NGM', '7NGM'];
  master_tones_list['Mahogany'][1][5] = ['4NGM', '5NGM'];
  master_tones_list['Mahogany'][0][6] = ['4NGM', '5NGM', '6NGM', '7NGM', '8NGM'];
  master_tones_list['Mahogany'][1][6] = ['4NGM', '5NGM', '6NGM'];
  master_tones_list['Mahogany'][0][7] = ['5NGM', '6NGM', '7NGM', '8NGM'];
  master_tones_list['Mahogany'][1][7] = ['5NGM', '6NGM', '7NGM'];
  master_tones_list['Mahogany'][0][8] = ['6NGM', '7NGM', '8NGM'];
  master_tones_list['Mahogany'][1][8] = ['6NGM', '7NGM', '8NGM'];
  master_tones_list['Mahogany'][0][9] = ['7NGM', '8NGM'];
  master_tones_list['Mahogany'][1][9] = ['7NGM', '8NGM'];
  master_tones_list['Mahogany'][0][10] = ['8NGM'];
  master_tones_list['Mahogany'][1][10] = ['8NGM'];
  master_tones_list['Mahogany'][0][11] = [];
  master_tones_list['Mahogany'][1][11] = [];
  master_tones_list['Violet Red'][0][1] = [];
  master_tones_list['Violet Red'][1][1] = [];
  master_tones_list['Violet Red'][0][2] = [];
  master_tones_list['Violet Red'][1][2] = [];
  master_tones_list['Violet Red'][0][3] = ['5RV'];
  master_tones_list['Violet Red'][1][3] = [];
  master_tones_list['Violet Red'][0][4] = ['5RV', '6RV', '6NVA'];
  master_tones_list['Violet Red'][1][4] = [];
  master_tones_list['Violet Red'][0][5] = ['5RV', '6RV', '7RV', '6NVA', '7NVA', '7NV'];
  master_tones_list['Violet Red'][1][5] = ['5RV'];
  master_tones_list['Violet Red'][0][6] = ['5RV', '6RV', '7RV', '6NVA', '7NVA', '8NVA', '7NV', '8NV'];
  master_tones_list['Violet Red'][1][6] = ['5RV', '6RV', '6NVA'];
  master_tones_list['Violet Red'][0][7] = ['5RV', '6RV', '7RV', '6NVA', '7NVA', '8NVA', '9NVA', '7NV', '8NV', '9NV'];
  master_tones_list['Violet Red'][1][7] = ['5RV', '6RV', '7RV', '6NVA', '7NVA', '7NV'];
  master_tones_list['Violet Red'][0][8] = ['6RV', '7RV', '10NVA', '6NVA', '7NVA', '8NVA', '9NVA', '10NV', '7NV', '8NV', '9NV'];
  master_tones_list['Violet Red'][1][8] = ['6RV', '7RV', '6NVA', '7NVA', '8NVA', '7NV', '8NV'];
  master_tones_list['Violet Red'][0][9] = ['7RV', '10NVA', '7NVA', '8NVA', '9NVA', '10NV', '7NV', '8NV', '9NV'];
  master_tones_list['Violet Red'][1][9] = ['7RV', '7NVA', '8NVA', '9NVA', '7NV', '8NV', '9NV'];
  master_tones_list['Violet Red'][0][10] = ['10NVA', '8NVA', '9NVA', '10NV', '8NV', '9NV'];
  master_tones_list['Violet Red'][1][10] = ['10NVA', '8NVA', '9NVA', '10NV', '8NV', '9NV'];
  master_tones_list['Violet Red'][0][11] = ['10NVA', '9NVA', '10NV', '9NV'];
  master_tones_list['Violet Red'][1][11] = ['10NVA', '9NVA', '10NV', '9NV'];


  var validUserTone = user_tone => master_tones_list[user_tone];
  var validUserToneAllOver = (user_tone, all_over) => master_tones_list[user_tone][parseInt(all_over)];
  var validUserToneLevel = (user_tone, all_over, level) => master_tones_list[user_tone][parseInt(all_over)][parseInt(level)];

  var colorFormula = (user_tone, all_over, level) => {
    let listColorsResult = [];
    let message = null;
    let validRoot = (element, level) => {
      if (element[0][level]) {
        if (element[0][level].length > 0) {
          return element[0][level];
        }
      }
      return []
    }
    if (validUserTone(user_tone) &&
      validUserToneAllOver(user_tone, all_over) &&
      validUserToneLevel(user_tone, all_over, level)) {

      if (!master_tones_list[user_tone][all_over][level]) {
        listColorsResult = validRoot();
        message = listColorsResult.length > 0 ? 'all over down to root' : null;
      } else {
        if (master_tones_list[user_tone][all_over][level].length === 0) {
          if (!master_tones_list[user_tone][0][level] || master_tones_list[user_tone][0][level].length === 0) {
            if (master_tones_list[user_tone][0][level + 1] && master_tones_list[user_tone][0][level + 1].length !== 0) {
              listColorsResult = master_tones_list[user_tone][0][level + 1];
              message = 'all over down to root && tone up +1';
            } else {
              // if (master_tones_list[user_tone][0][level + 2] && master_tones_list[user_tone][0][level + 2].length !== 0){
              //       listColorsResult = master_tones_list[user_tone][0][level + 2];
              //       message = 'all over down to root && tone up +2';
              //     }
            }
          } else {
            listColorsResult = master_tones_list[user_tone][0][level];
            message = 'all over down to root';
          }
        } else {
          listColorsResult = master_tones_list[user_tone][all_over][level];
        }
      }


    }
    return {
      listColorsResult,
      message
    }
  }

  var getAmountOfProduct = () => {
    var amount = 0;

    if (getInputValue("color-type") === 'All Over Color') {
      amount += 2.5;
    } else {
      amount += 1;
    }
    if (getInputValue("last-coloring")) {
      var lastColoringInt = parseInt(getInputValue("last-coloring").substr(0, 1));
      if (!isNaN(lastColoringInt)) {
        amount = amount + lastColoringInt * 0.25;
      }
    }
    var hairLength = getInputValue("hair-length");
    switch (hairLength) {
      case 'Shoulder Length':
        amount = amount + 1;
        break;
      case 'Long':
        amount = amount + 1.5;
        break;
      case 'Very Long':
        amount = amount + 2;
        break;

      default:
        break;
    }
    var texture = getInputValue("natural-texture");
    switch (texture) {
      case 'Coarse':
      case 'Very Coarse':
        amount = amount + 1;
        break;

      default:
        break;
    }

    return amount;
  }

  var getInputValue = inputName => jQuery(`input[name='${inputName}']:checked`).val();
  var getSelectValue = selectName => jQuery(selectName).val();

  var getFormInfo = () => {
    var newState = {
      "natural-color-shades": getInputValue("natural-color-shades"),
      "natural-color-level": getInputValue("natural-color-level"),
      "natural-color-shade": getInputValue("natural-color-shade"),
      "brand-name": getSelectValue("#brand-name"),
      "color-shade-product": getSelectValue("#color-shade-product"),
      "color-shades": getInputValue("color-shades"),
      "color-level": getInputValue("color-level"),
      "color-shade": getInputValue("color-shade"),
      "color-type": getInputValue("color-type"),
      "currently-coloring": getInputValue("currently-coloring"),
      "highlights": getInputValue("highlights"),
      "recent-services": getInputValue("recent-services"),
      "last-coloring": getInputValue("last-coloring"),
      "hair-condition": getInputValue("hair-condition"),
      "hair-length": getInputValue("hair-length"),
      "much-gray": getInputValue("much-gray"),
      "hair-resistant": getInputValue("hair-resistant"),
      "natural-texture": getInputValue("natural-texture"),
      "color-shades": getInputValue("color-shades"),
      "color-level": getInputValue("color-level"),
      "color-shade": getInputValue("color-shade"),
      "color-swatches": getInputValue("color-swatches"),
      "user-name": getSelectValue("#user-name"),
      "mobile-number": getSelectValue("#user-phone"),
      "email": getSelectValue("#user-email"),
      "keratin": getKeratin() ? 'Yes' : 'No',
      "porous": getPorous() ? 'Yes' : 'No',
      "recent_services": JSON.stringify(getRecentServices()),
      "amount": getAmountOfProduct() + '(oz)',
      "last-coloring": getInputValue('last-coloring')
    }


    state = newState;
    console.log('---------', {
      state
    });

    let output = jQuery('<ul/>', {
      'html': function() {
        let list = '';
        let keys = Object.keys(state);
        keys.forEach(item => {
          list = `${list} <li>${item}: ${state[item]}</li>`
        });
        return list;
      },
    });

    jQuery('#json_output').html(output);
    return state;
  }

  /* /color formula */
  var color_swatches = <?php echo json_encode($master_tones_list); ?>;
  var bg_img = <?php echo json_encode($images); ?>;
  var current_fs, next_fs, previous_fs; //fieldsets
  var left, opacity, scale; //fieldset properties which we will animate
  var animating, current_color, next_color, selected_color; //flag to prevent quick multi-click glitches


  //var pageURL = window.location.href;
  var base = "<?php echo get_home_url(); ?>";

  jQuery(document).on("change", ".required div input", function() {
    jQuery(this).parents().find('.bg .error-msg').text('');
  });

  jQuery(".next").click(function() {
    getFormInfo();
    current_fs = jQuery(this).parent();
    next_fs = jQuery(this).parent().next();
    jQuery("#progressbar li").eq(jQuery("fieldset").index(next_fs)).addClass("active");
    next_fs.fadeIn();
    current_fs.fadeOut();
  });

  jQuery(".previous").click(function() {
    current_fs = jQuery(this).parent();
    previous_fs = jQuery(this).parent().prev();
    jQuery("#progressbar li").eq(jQuery("fieldset").index(current_fs)).removeClass("active");
    previous_fs.fadeIn();
    current_fs.fadeOut()
  });

  jQuery(".restart").click(function() {
    if (confirm('You can start over at any time, just remember this will remove your answers!')) {
      location.reload(true);
    }
  });

  jQuery("fieldset .previous").click(function() {
    var find_input = jQuery(this).parent('fieldset').prev().prop("tagName");
    var check = find_input + ' input:checked';
    jQuery(this).parent('fieldset').prev().find(".next").css({
      "visibility": "visible",
      "width": "100px",
      "padding": "10px 5px",
      "height": "auto"
    });
    console.log(check);

  });
  /*****ON CLICK PREVIOUR MAKE ALL PREV DISABLED*****/
  /*jQuery("fieldset .previous").click(function(){
  	jQuery("fieldset .previous").attr('disabled',true);
  });*/

  var showNextBtn = () => {
    jQuery('.currently-color').parent().addClass('brand-section');
  }

  var goToStepUp = (from, to) => {
    jQuery(`.screen-step${from}`).slideUp();
    current_fs = jQuery(`.screen-step${to - 1}`);
    next_fs = current_fs.next();
    previous_fs = current_fs.prev();
    current_fs.slideUp();
    next_fs.slideDown();
    for (let index = 0; index < to; index++) {
      jQuery("#progressbar li").eq(index).addClass("active");
    }
  }
  var goToStepDown = (from, to) => {
    jQuery(`.screen-step${from}`).slideUp();
    current_fs = jQuery(`.screen-step${to + 1}`);
    next_fs = current_fs.next();
    previous_fs = current_fs.prev();
    current_fs.slideUp();
    previous_fs.slideDown();
    for (let index = 0; index < to; index++) {
      jQuery("#progressbar li").eq(index).addClass("active");
    }
  }

  var progressBarTo = step => {
    jQuery("#progressbar li").removeClass("active");
    jQuery("#progressbar li").each(function(index) {
      if (index < step) {
        jQuery("#progressbar li").eq(index).addClass("active");
      }
    })
  }

  var stopQuiz = () => {
    console.log('------------stop quiz, alert colorist');
  }
  var alertColorist = () => {
    console.log('------------ alert colorist');
  }
  var alertStaff = () => {
    console.log('------------alert staff');
  }
  var grayResistance = () => {
    console.log('------------ grayResistance');
  }

  var getBrandFormula = () => {
    let brand_id = jQuery('#brand-name').val();
    getBrandTone(brand_id);
    if (brand_id !== 'Select Brand') {
      jQuery('.brand-name-product').removeClass('hidden').fadeOut();
      jQuery('#color-shade-product').html('');
      jQuery('#color-shade select option').each(function() {
        jQuery(this).remove();
      });
      jQuery.ajax({
        url: '<?php echo get_stylesheet_directory_uri(); ?>/brand-ajax.php',
        type: 'post',
        dataType: 'json',
        data: {
          brand_id
        },
      }).done(function(data) {
        var html = "<option value='0'>Select Product</option>";
        jQuery.each(data, function(key, value) {
          html += `<option  value='${value[3]}'>${value[1]}</option>`;
        });
        html += "<option value='999'>Not Found</option>";
        jQuery('#color-shade-product').html(html);
        jQuery('.brand-name-product').fadeIn();
      }).fail(function(e) {
        console.error('----------the service is not working', {
          url: '<?php echo get_stylesheet_directory_uri(); ?>/brand-ajax.php',
          e
        });

      })
    }
  }

  /* ---------------------------------------step1 */
  jQuery("input[name='natural-color-shades']").click(function() {
    let naturalColor = getInputValue('natural-color-shades');
    jQuery('.natural-color-levels').addClass('hidden');
    jQuery('.natural-color-levels.' + naturalColor).removeClass('hidden');
    jQuery(this).parents('fieldset').find(".next").click();
  });
  /* ---------------------------------------step1 */

  /* ---------------------------------------step2 */
  jQuery("input[name='natural-color-level']").click(function() {
    let naturalColor = getInputValue('natural-color-shades');
    jQuery('.natural-color-shades').addClass('hidden');
    jQuery('.natural-color-shades.' + naturalColor).removeClass('hidden');
    jQuery(this).parents('fieldset').find(".next").click();
  });
  /* ---------------------------------------step2 */

  /* ---------------------------------------step3 */
  jQuery("input[name='natural-color-shade']").click(function() {
    jQuery(this).parents('fieldset').find(".next").click();
  });
  /* ---------------------------------------step3 */

  /* ---------------------------------------step4 */
  /* color-type */
  jQuery("#brand-name").on('change', function() {
    if (jQuery(this).val() === '999') {
      window.location.href = base + '/demo';
    } else {
      getBrandFormula();
    }
  });

  jQuery("#color-shade-product").on('change', function() {
    jQuery(this).parents('fieldset').find(".next").click();
  });
  /* ---------------------------------------step4 */

  /* ---------------------------------------step8 */
  /* color-type */
  jQuery("input[name='color-type']").click(function() {
    var colorType = jQuery(this).val();
    if (colorType === 'Root Touch Up') {
      goToStepUp(2, 5);
    } else {
      jQuery(this).parents('fieldset').find(".next").click();
    }
  });
  /* ---------------------------------------step8 */

  /* ---------------------------------------step9 */
  /* currently-coloring / virgin */
  jQuery("input[name='currently-coloring']").click(function() {
    var noCurrentColor = jQuery('.no-current-color');
    showNextBtn();
    noCurrentColor.addClass('hidden');
    var noCurrentColorValue = jQuery(this).val();
    switch (noCurrentColorValue) {
      case 'All Over':
        goToStepUp(3, 5);
        break;
      case 'All Over Lightening':
      case 'Henna':
        alertColorist();
        stopQuiz();
        break;
      case 'Color & High Lights':
      case 'High Lights Only':
        goToStepUp(3, 4);
        break;
      case 'Virgin':
        noCurrentColor.removeClass('hidden');
        break;

      default:
        break;
    }
  });
  /* ---------------------------------------step9 */
  /* currently-coloring / virgin / yes || not*/
  jQuery('.have-some-color').click(function() {
    goToStepDown(3, 2);
    progressBarTo(2);
  })
  jQuery('.no-color-sure').click(function() {
    goToStepUp(3, 7);
  })
  /* ---------------------------------------step9 */
  /* ---------------------------------------step3 */
  /* highlights*/
  jQuery("input[name = 'highlights']").click(function() {
    var colorType = jQuery(this).val();
    if (colorType === 'Keep') {
      alertStaff();
    }
    jQuery(this).parents('fieldset').find(".next").click();
  })
  /* ---------------------------------------step4 */
  /* recent-services*/
  // jQuery("input[name = 'recent-services']").click(function() {
  //   jQuery(this).parents('fieldset').find(".next").click();
  // })

  /* ---------------------------------------step5 */
  /* recent-services*/
  jQuery("input[name = 'last-coloring']").click(function() {
    jQuery(this).parents('fieldset').find(".next").click();
  })
  /* ---------------------------------------step6 */
  /* hair-condition */
  jQuery("input[name='hair-condition']").click(function() {
    var condition = jQuery(this).val();
    var damaged = jQuery('.hair-condition-damaged');
    showNextBtn();
    damaged.addClass('hidden');
    switch (condition) {
      case 'Healthy':
        jQuery(this).parents('fieldset').find(".next").click();
        break;
      case 'Porous':
        alertStaff();
        jQuery(this).parents('fieldset').find(".next").click();
        break;
      case 'Damaged':
        damaged.removeClass('hidden');
        break;

      default:
        break;
    }
  });
  /* hair-condition / virgin / Dry || Breakage*/
  jQuery('.hair-condition-dry').click(function() {
    goToStepUp(7, 8);
  })
  jQuery('.hair-condition-breakage').click(function() {
    alertStaff();
    goToStepUp(7, 8);
  })

  /* ---------------------------------------step7 */
  /* hair-length*/
  jQuery("input[name = 'hair-length']").click(function() {
    jQuery(this).parents('fieldset').find(".next").click();
    // jQuery('.screen-step8').fadeIn();
  })
  /* ---------------------------------------step8 */
  /* much-gray*/
  jQuery("input[name = 'much-gray']").click(function() {
    var gray = jQuery(this).val();
    switch (gray) {
      case 'None':
      case 'Few':
        break;
      case 'Lots':
      case 'All':
        grayResistance();
        break;

      default:
        break;
    }
    jQuery(this).parents('fieldset').find(".next").click();
  })

  /* ---------------------------------------step9 */
  /* hair - resistant*/
  jQuery("input[name = 'hair-resistant']").click(function() {
    var resistant = jQuery(this).val();
    if (resistant === 'Yes') {
      grayResistance();
    }
    jQuery(this).parents('fieldset').find(".next").click();
  })

  /* ---------------------------------------step10 */
  /* natural-texture*/
  jQuery("input[name = 'natural-texture']").click(function() {
    var texture = jQuery(this).val();
    switch (texture) {
      case 'Fine':
      case 'Normal':
        break;
      case 'Coarse':
      case 'Very Coarse':
        break;

      default:
        break;
    }
    jQuery(this).parents('fieldset').find(".next").click();
  })

  /****COLOR SHADE BLACK/Brown/Blonde/RED******/
  /* natural color */
  // jQuery(".natural-color-shades .black").click(function() {
  //   jQuery(".black-options.no-count").removeClass('hidden').fadeIn();
  //   jQuery(" .no-count.new-color:not(.black-options)").addClass('hidden').fadeOut();
  // });

  jQuery(".natural-color-shades .brown").click(function() {
    jQuery(".brown-options").removeClass('hidden').fadeIn();
    jQuery(" .no-count.new-color:not(.brown-options)").addClass('hidden').fadeOut();
  });

  jQuery(".natural-color-shades .blonde").click(function() {
    jQuery(".blonde-options").removeClass('hidden').fadeIn();
    jQuery(".no-count.new-color:not(.blonde-options)").addClass('hidden').fadeOut();
  });

  jQuery(".natural-color-shades .red").click(function() {
    jQuery(".red-options").removeClass('hidden').fadeIn();
    jQuery(" .no-count.new-color:not(.red-options)").addClass('hidden').fadeOut();
  });
  /* natural color */
  /* new color */
  jQuery(".color-shades .first").click(function() {
    jQuery(".black-options.no-count").removeClass('hidden');
    jQuery(" .no-count.new-color:not(.black-options)").addClass('hidden');
  });

  jQuery(".color-shades .brown").click(function() {
    jQuery(".brown-options").removeClass('hidden');
    jQuery(" .no-count.new-color:not(.brown-options)").addClass('hidden');
  });

  jQuery(".color-shades .blonde").click(function() {
    jQuery(".blonde-options").removeClass('hidden');
    jQuery(".no-count.new-color:not(.blonde-options)").addClass('hidden');
  });

  jQuery(".color-shades .last").click(function() {
    jQuery(".red-options").removeClass('hidden');
    jQuery(" .no-count.new-color:not(.red-options)").addClass('hidden');
  });
  /* new color */

  /****ON CHANGE COLOR SHADE****/
  var level_val;
  jQuery(".new-color input[name='color-level']").on("change", function() {
    level_val = jQuery(".new-color input[name='color-level']:checked").val();
  });

  jQuery(".new-color input[name='color-shade'],.new-color input[name='color-level']").on("change", function() {
    jQuery(".multi-swatches ul li,.selected-swatch img,.color-preview img,.selected-color").remove();
    level_val = jQuery(".new-color input[name='color-level']:checked").val();
    var shade_val = jQuery(".new-color input[name='color-shade']:checked").val();
    var image_name;
    // var options = colorFormula(user_tone, all_over, level);
    let shade = getInputValue('color-shade');
    let over = getInputValue('color-type') === "All Over Color" ? 1 : 0;
    let level = getInputValue('color-level');
    let options = colorFormula(shade, over, level);
    //console.log(color_swatches[shade_val][level_val]);
    jQuery(".swatches div.multi-swatches h4").text(shade_val + " Color Swatches");
    jQuery.each(options.listColorsResult, function(index, value) {
      var img_name = this + "_edit.png";
      image_name = this;
      //  console.log(img_name);
      if (bg_img.indexOf(img_name) !== -1) {
        var url = base + '/wp-content/uploads/color_swatches/' + img_name;
        jQuery(".swatches div.multi-swatches ul").append('<li><label class="no-hover"><input type="radio" name="color-swatches" title="' + image_name + '" value="' + value + '"><span class="input-label" style="background-image:url(' + url + ')!important;">' + value + '</span></label></li>');
      }
    });
    jQuery(".multi-swatches ul li:first-child").addClass("active");
    jQuery(".multi-swatches ul li.active input").click();
    var color = jQuery(".multi-swatches ul li.active input").attr("title");
    var active_color = jQuery(".multi-swatches ul li.active span").css('background-image');
    active_color = active_color.replace('url(', '').replace(')', '').replace(/\"/gi, "");
    jQuery(".selected-swatch").append("<p class='selected-color'>" + color + "</p><img src='" + active_color + "'>");
    jQuery(".preview .edit-color .color-preview").append("<p class='selected-color'>" + color + "</p><img src='" + active_color + "'>");


  });

  /*****ON CHANGING/CLICKING SWATCHES CHOICE*******/
  jQuery(document).on('click', ".swatches .multi-swatches input", function() {
    jQuery(".preview .edit-color .color-listing ul").remove();
    jQuery(".multi-swatches ul li,.color-listing ul li").removeClass("active");
    jQuery(this).parent().parent().addClass("active");
    current_color = jQuery(".swatch-selection .multi-swatches ul li.active input").val();
    var color = jQuery(".multi-swatches ul li.active input").attr("title");
    var active_color = jQuery(".multi-swatches ul li.active span").css('background-image');
    active_color = active_color.replace('url(', '').replace(')', '').replace(/\"/gi, "");
    jQuery(".selected-swatch p.selected-color,.color-preview p.selected-color").text(color);
    jQuery(".selected-swatch img,.color-preview img").attr("src", active_color);
    if (jQuery('.multi-swatches ul li').length > 0) {
      jQuery(".preview .edit-color h6").text("Based on the answers, this is the suggested color swatch. Please look at the other color swatches and if you find one that you like better,Please select that ");
      jQuery(".preview .col-sm-6").css("width", "50%");
      jQuery(".preview .edit-color .color-listing").append(jQuery(".swatch-selection .multi-swatches .listing").html());
    }

    console.log('-------------swatches', {
      active_color
    });
  });

  /****PREVIEW SWATCHES*****/
  jQuery(document).on('click', ".preview .multi-swatches .color-listing input", function() {
    jQuery(".color-listing ul li").removeClass("active");
    jQuery(this).parent().parent().addClass("active");
    var color = jQuery(".color-listing ul li.active input").attr("title");
    var active_color = jQuery(".color-listing ul li.active span").css('background-image');
    active_color = active_color.replace('url(', '').replace(')', '').replace(/\"/gi, "");
    jQuery(".color-preview img,.selected-swatch img").attr("src", active_color);
    jQuery(".color-preview p.selected-color").text(color);
    //  jQuery(".preview .edit-color .color-listing").append(jQuery(".swatch-selection .multi-swatches .listing").html());  
    console.log('-------------preview swatches', {
      active_color
    });
  });

  //*****COLOR TYPE 'ROOT TOUCH UP' OR 'ALL OVER COLOR'******/
  // jQuery(" .color-type .last").click(function() {
  //   jQuery(".dont-know-color-brand .select-hair-length").removeClass('hidden');
  //   jQuery(".dont-know-color-brand .last-color,.hair-resistant").addClass('hidden');
  // });

  // jQuery(" .last-color input").click(function() {
  //   jQuery(".hair-resistant").removeClass("hidden");
  // });

  /*******If input is checked CLICK NEXT Button*******/
  jQuery(".color-shades input[name='color-shades'],input[name='color-shade'],input[name='hair-length'],input[name='color-level']").click(function() {
    //	jQuery("fieldset .previous").attr('disabled', false);
    jQuery(this).parents('fieldset').find(".next").click();
    //	console.log(jQuery(this).parents('fieldset').find(".next").click());
  });

  jQuery(".color-shades input[name='color-shades']").click(function() {
    console.log('------------', {
      options: colorFormula()
    });

  });

  /****R E C E N T   S E R V I C E S******/
  jQuery(".services input.last").click(function() {
    jQuery(".services input:not(.last)").prop('checked', false);
    //  jQuery("fieldset .previous").attr('disabled', false);
    jQuery(this).parents('fieldset').find(".next").click();
  });
  jQuery(".services input:not(.last)").click(function() {

    jQuery.each(jQuery(".services input[type='checkbox']:checked"), function() {
      if (jQuery(this).val() == "All Over Bleach") {
        jQuery(".service-checkbox .warning-section").removeClass("hidden");
        jQuery(this).attr("do-uncheck");
      } else {
        jQuery(".service-checkbox .warning-section").addClass("hidden");
      }
    });
    jQuery(".services input.last").prop('checked', false);
  });

  var humanDate = () => {
    var d = new Date();
    var date = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    return month + "/" + date + "/" + year;
  }

  var getFormula = () => {
    if (jQuery(".preview ul li.active input").val() !== current_color) {
      current_color = jQuery(".preview ul li.active input").val();
    }
    return current_color;
  }

  var getRecentServices = () => {
    var services = '';
    jQuery.each(jQuery("input[name='recent-services']:checked"), function(index) {
      if (index === 0) {
        services = jQuery(this).val();
      } else {
        services = services + ', ' + jQuery(this).val();
      }
    });

    return services;
  }

  var getKeratin = () => {
    var keratin = false;
    jQuery.each(jQuery("input[name='recent-services']:checked"), function() {
      if (jQuery(this).val() === 'Keratin') {
        keratin = true;
      }
    });
    return keratin;
  }

  var getPorous = () => jQuery("input[name='hair-condition']:checked").val() === 'Porous';

  var getHairCondition = () => {
    var hairCondition = getInputValue("hair-condition");
    var hairConditionDamaged = hairCondition === 'Damaged' ? ' / ' + getInputValue("hair-condition-damaged") : '';
    return hairCondition + hairConditionDamaged;
  }

  /***Questionnaire Summary****/
  jQuery(".action-button.preview").click(function() {
    var dying_service, hair_type, keratin, porous;

    if (jQuery("input[name='hair-condition']:checked").val() === 'Porous') {
      porous = "POROUS ALERT!";
      jQuery(".porous").css("color", "red");
      jQuery(".warning").append("<img src='<?php echo get_stylesheet_directory_uri(); ?>/images/danger.png'>");
    } else {
      porous = "Not Porous";
      jQuery(".warning").append("<span class='no-warning' style='font-size:18px;'>No Warnings</span>");
    }

    if (jQuery("input[name='hair-resistant']:checked").val() == 'Yes') {
      resistant = "Resistant";
      //  hair_type="Resistant";
    } else {
      resistant = "Not Resistant";
    }

    if (jQuery("input[name='color-type']:checked").val() == 'Root Touch Up') {
      hair_type = "Virgin";
    }

    jQuery(".current_date").text(humanDate());
    jQuery(".formula").text(getSelectValue("#color-shade-product"));
    jQuery(".kit_choice").text(getSelectValue("#color-shade-product") + '/' + getAmountOfProduct() + '(oz)');

    jQuery(".input-natural-color-shades").text(getInputValue('natural-color-shades'));
    jQuery(".input-natural-color-level").text(getInputValue('natural-color-level'));
    jQuery(".input-natural-color-shade").text(getInputValue('natural-color-shade'));
    jQuery(".input-brand-name").text(getSelectValue('#brand-name'));
    jQuery(".input-color-shade-product").text(getSelectValue('#color-shade-product'));

    jQuery(".input-color-shades").text(getInputValue('color-shades'));
    jQuery(".input-color-level").text(getInputValue('color-level'));
    jQuery(".input-color-shade").text(getInputValue('color-shade'));
    jQuery(".input-hair-type").text(getInputValue('hair-type'));
    jQuery(".input-keratin").text(getKeratin() ? 'Yes' : 'No');
    jQuery(".input-porous").text(porous);

    jQuery(".input-hair-type").text(getInputValue('color-type'));
    jQuery(".input-highlights").text(getInputValue('highlights'));
    jQuery(".input-resistant").text(getInputValue('hair-resistant'));
    jQuery(".input-recent_services_arr").text(JSON.stringify(getRecentServices()));
    jQuery(".input-keratin").text();
    jQuery(".input-clr_amt").text(getAmountOfProduct() + '(oz)');

    jQuery(".input-last-coloring").text(getInputValue('last-coloring'));

    //gettInputValue("natural-color-shades"),
    //getInputValue("natural-color-level"),
    //getInputValue("natural-color-shade"),
    //getSelectValue("#brand-name"),
    //getSelectValue("#color-shade-product"),
    //getInputValue("color-shades"),
    //getInputValue("color-level"),
    //getInputValue("color-shade"),
    //getInputValue("color-type"),
    //getInputValue("currently-coloring"),
    //getInputValue("highlights"),
    //getInputValue("recent-services"),
    //getInputValue("last-coloring"),
    //getInputValue("hair-condition"),
    //getInputValue("hair-length"),
    //getInputValue("much-gray"),
    //getInputValue("hair-resistant"),
    //getInputValue("natural-texture"),
    //getInputValue("color-shades"),
    //getInputValue("color-level"),
    //getInputValue("color-shade"),
    //getInputValue("color-swatches"),
    //getInputValue("user-name"),
    //getInputValue("mobile-number")
  });

  /*******F O R M   S E R V I C E S*****/
  var textFieldsValidation = () => {
    console.log('-----textFieldsValidation');
  }

  /*******F O R M   S E R V I C E S*****/
  jQuery("#user-fields").click(function(e) {
    let error = false;
    e.preventDefault();
    textFieldsValidation();
    if (!error) {
      jQuery(this).parents('fieldset').find(".next").click();
      jQuery('.screen-step14').fadeOut();
      jQuery('.screen-step15').fadeIn();
      jQuery(".user-name").text(getSelectValue('#user-name'));
      jQuery(".user-phone").text(getSelectValue('#user-phone'));
      jQuery(".user-email").text(getSelectValue('#user-email'));
    }
    getFormInfo();
  });

  /*******F O R M   S E R V I C E S*****/
  /*jQuery(".form-submit").click(function()
  {
  	var base_url = window.location.origin;
  	jQuery(".error-msg,.success").text("");
  	jQuery(".success").css("border","none");
  	var status=false;
   
  	if(status == false)
  	{
  		var services = [];
  	    jQuery.each(jQuery(".services input[type='checkbox']:checked"), function(){
  	        services.push(jQuery(this).val());
  	    });         
      
  		if(services.includes("All Over Bleach"))
  		{
  		//	 selected_color = next_color;
  		}
  	
  	}	
   
  });*/
  jQuery('.custom-tooltips .got-custom-tooltip').on('mouseover', function() {
    jQuery(this).parent().find('.custom-tooltip').fadeIn();
  })

  jQuery('.custom-tooltips .got-custom-tooltip').on('mouseleave', function() {
    jQuery('.custom-tooltip').fadeOut()
  })
</script>
<style type="text/css">
  .restart {
    position: absolute;
    right: 40px;
    z-index: 1;
    top: -20px;
  }

  .custom-tooltips {
    position: relative;
  }

  .custom-tooltip {
    border: #ccc solid;
    border-radius: 5px;
    padding: 9px;
    font-size: 12px;
    color: white;
    background-color: #024c63;
    display: none;
    max-width: 300px;
    text-align: left;
    transition: ease-in-out all 0.5;
    margin-left: 47%;
    transform: translate(-50%);
    opacity: 0.9;
    position: absolute;
    left: 0;
    top: 50px;
    z-index: 100;
  }

  #questionaire-form .listing-centered .input-label {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  #questionaire-form input[type=text],
  #questionaire-form input[type=tel],
  #questionaire-form input[type=email],
  #questionaire-form select {
    font-family: century-gothic !important;
    border: 1px solid #024c63 !important;
    border-radius: 0 !important;
    box-shadow: none !important;
    background: #ddd;
    height: 50px;
    margin-bottom: 14px;
  }

  #json_output ul {
    display: flex;
    align-content: center;
    flex-wrap: wrap;
    list-style: none;
    padding: 0;
  }

  #json_output ul li {
    border: 1px solid #024c63 !important;
    background: #ccc;
    font-size: 12px;
    padding: 5px;
    border-radius: 5px;
    margin: 2px;
  }

  .summary-field {
    padding: 10px !important;
    border: 1px solid !important;
    margin-bottom: 15px !important;
    text-align: left !important;
    background-color: white !important;
    font-size: 16px !important;
    box-shadow: none !important;
  }
</style>