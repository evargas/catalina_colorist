<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

function enqueue_parent_styles() {
   	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

   	wp_register_style( 'custom-style1', get_stylesheet_directory_uri() . '/css/custom.css', array(), '1.6.0' );

	wp_register_style( 'custom-style2', get_stylesheet_directory_uri() . '/css/my-style.css', array(), '1.6.0' );

    wp_register_style( 'scheduling-style', get_stylesheet_directory_uri() . '/css/scheduling.css', array(), filemtime( get_stylesheet_directory() . '/css/scheduling.css' ) );

    wp_register_style( 'custom-style3', get_stylesheet_directory_uri() . '/css/questionnaire.css', array(), filemtime( get_stylesheet_directory() . '/css/questionnaire.css' ) );
	
    wp_register_script( 'custom-js', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ),filemtime( get_stylesheet_directory() . '/js/custom.js' ), true );


	wp_enqueue_style( 'custom-style1' );
	wp_enqueue_style( 'custom-style2' );
    wp_enqueue_style( 'scheduling-style' );
	wp_enqueue_style( 'custom-style3' );
    wp_enqueue_script( 'custom-js' );

}


function wpb_custom_new_menu() {
  register_nav_menus(
    array(
      'my-custom-menu' => __( 'My Custom Menu' ),
       'salon-menu' => __( 'Salon Page Menu' ),
       'terms-n-conditions-menu' => __( 'T&C Menu' )
    )
  );
}
add_action( 'init', 'wpb_custom_new_menu' );

if (function_exists('register_sidebar')) {
register_sidebar(array(
'name' => 'header_1',
'id'   => 'header_1',
'description'   => 'Widget Area',
'before_widget' => '<div class="two">',
'after_widget' => '</div>',
'before_title' => '<h2>',
'after_title'   => '</h2>'
));
}

/****Footer Area*****/
function tutsplus_widgets_init() {
 
    // First footer widget area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Area above Footer', 'tutsplus' ),
        'id' => 'footer-top',
        'description' => __( '', 'tutsplus' ),
        'before_widget' => '<div id="%1$s" class="widget-container quiz-bnr %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
 }
 
add_action( 'widgets_init', 'tutsplus_widgets_init' );


function custom_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Inspiration', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'Inspiration', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'Inspiration', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent Inspiration', 'twentythirteen' ),
        'all_items'           => __( 'All Inspiration', 'twentythirteen' ),
        'view_item'           => __( 'View Inspiration', 'twentythirteen' ),
        'add_new_item'        => __( 'Add New Inspiration', 'twentythirteen' ),
        'add_new'             => __( 'Add New', 'twentythirteen' ),
        'edit_item'           => __( 'Edit Inspiration', 'twentythirteen' ),
        'update_item'         => __( 'Update Inspiration', 'twentythirteen' ),
        'search_items'        => __( 'Search Inspiration', 'twentythirteen' ),
        'not_found'           => __( 'Not Found', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),

    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'inspiration', 'twentythirteen' ),
        'description'         => __( 'Inspiration news and reviews', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
/*        'taxonomies'          => array( 'genres' ),
ss*/        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
         

        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'taxonomies'          => array( 'category' ),
    );
     
    // Registering your Custom Post Type
    register_post_type( 'inspiration', $args );
 
}
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
add_action( 'init', 'custom_post_type', 0 );


// hide update notifications
/*function remove_core_updates(){
global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}
add_filter('pre_site_transient_update_core','remove_core_updates'); //hide updates for WordPress itself
add_filter('pre_site_transient_update_plugins','remove_core_updates'); //hide updates for all plugins
add_filter('pre_site_transient_update_themes','remove_core_updates'); //hide updates for all themes*/

/******LAst Modified DAte******/
add_shortcode('last-modified', 'wpv_post_modified_shortcode');
function wpv_post_modified_shortcode($atts) {
if (empty($atts['format'])) {
$atts['format'] = get_option('date_format');
}
return get_the_modified_date($atts['format']);
}



/**
 * Show products only of selected category.
 */
function hide_appointment_category_from_shop_page( $terms, $taxonomies, $args ) {
 
    $new_terms  = array();
    $hide_category  = array( 62 ); // Ids of the category you don't want to display on the shop page
    
      // if a product category and on the shop page
    if ( in_array( 'product_cat', $taxonomies ) && !is_admin() && is_shop() ) {

        foreach ( $terms as $key => $term ) {

        if ( ! in_array( $term->term_id, $hide_category ) ) { 
            $new_terms[] = $term;
        }
        }
        $terms = $new_terms;
    }
  return $terms;
}
add_filter( 'get_terms', 'hide_appointment_category_from_shop_page', 10, 3 );

function add_short_desc_in_loop(){
    global $post;
    $id = $post->ID;
    echo '<p>'.wp_trim_words($post->post_excerpt,20).'</p>';
    ?>

   <!--  <div itemprop="description">
        <?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
    </div> -->
    <?php
}
add_action('woocommerce_after_shop_loop_item_title','add_short_desc_in_loop',9);

function show_product_search_bar(){

    $toshow = false;
    if ( is_shop() ) {
        $toshow = true;
    }elseif ( is_product_category() ) {
            $parent_id    = get_queried_object_id();
            $toshow = false;
            //$display_type = get_term_meta( $parent_id, 'display_type', true );
            //$display_type = '' === $display_type ? get_option( 'woocommerce_category_archive_display', '' ) : $display_type;
            // $subcategories = woocommerce_get_product_subcategories( $parent_id );
            // if ( empty( $subcategories ) ) {
            //    $toshow = false;
            // }else{
            //     $toshow = true;
            // }
        }

    if($toshow){ ?>
        <div class="productsearchbar">
            <?php if ( function_exists( 'aws_get_search_form' ) ) { aws_get_search_form(); }  ?>
        </div>
    <?php } 
}
add_action('woocommerce_archive_description','show_product_search_bar');


/*****Hide Related Prpducts from appointment page ******/

add_action( 'wp', 'wc_remove_related_products' );

function wc_remove_related_products() {
  // replace 'accessories' with the slug for your product category
  if ( is_product() && has_term( 'book-an-appointment', 'product_cat' ) ) {

    //Remove Related products
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

    //Remove Price
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);

    //Remove Featured Images
    remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
    //Remove Categories
    
  }

}


// For Hair dye products, show related products from Membership category.
function changerelatedproductsforhardye( $related_posts, $product_id, $args ){
    if (has_term( 54, 'product_cat', $product_id )) { 
        global $post;
        $related_posts = array();
        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_key' => '_price',
            'tax_query' => array(
                 array(
                    'taxonomy' => 'product_cat',
                     'field'    => 'slug',
                     'terms'     =>  array('membershipandsubscription')
                     )
                    )
            );
        
        $related_posts = get_posts($args);
       
    }


    return $related_posts;
}
add_filter('woocommerce_related_products','changerelatedproductsforhardye', 10,3);


// add_filter('woocommerce_product_related_posts_shuffle',function(){return false;});


/******RELATED PRODUCTS SORTED BY PRICE*******/

function custom_remove_hook(){
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
    add_action( 'woocommerce_after_single_product_summary', 'custom_function_to_sort_related', 22 );
}
add_action( 'woocommerce_after_single_product_summary', 'custom_remove_hook' );

function custom_function_to_sort_related( $args = array() ) {
        global $product;

        if ( ! $product ) {
            return;
        }

        $defaults = array(
            'posts_per_page' => 4,
            'columns'        => 4,
            'orderby'        => 'price', // @codingStandardsIgnoreLine.
            'order'          => 'ASC'
        );

        $args = wp_parse_args( $args, $defaults );

        // Get visible related products then sort them at random.
        $args['related_products'] = array_filter( array_map( 'wc_get_product', wc_get_related_products( $product->get_id(), $args['posts_per_page'], $product->get_upsell_ids() ) ), 'wc_products_array_filter_visible' );

        // Handle orderby.
        $args['related_products'] = wc_products_array_orderby( $args['related_products'], $args['orderby'], $args['order'] );

        wc_get_template( 'single-product/related.php', $args );
    }

function wooc_add_phone_number_field() {
    return apply_filters( 'woocommerce_forms_field', array(
        'wooc_user_phone' => array(
            'type'        => 'text',
            'label'       => __( 'Phone Number', ' woocommerce' ),
            'placeholder' => __( 'Your phone number', 'woocommerce' ),
            'required'    => true,
        ),
    ) );
}
add_action( 'woocommerce_register_form', 'wooc_add_field_to_registeration_form', 15 );
function wooc_add_field_to_registeration_form() {
    $fields = wooc_add_phone_number_field();
    foreach ( $fields as $key => $field_args ) {
        woocommerce_form_field( $key, $field_args );
    }
}

add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );
function wooc_save_extra_register_fields( $customer_id ) {
    if (isset($_POST['wooc_user_phone'])){
        update_user_meta( $customer_id, 'wooc_user_phone', sanitize_text_field( $_POST['wooc_user_phone'] ) );
    }
}

function wooc_get_users_by_phone($phone_number){
    $user_query = new \WP_User_Query( array(
        'meta_key' => 'wooc_user_phone',
        'meta_value' => $phone_number,
        'compare'=> '='
    ));
    return $user_query->get_results();
}

add_filter('authenticate','wooc_login_with_phone',30,3);
function wooc_login_with_phone($user, $username, $password ){
    if($username != ''){
        $users_with_phone = wooc_get_users_by_phone($username);
        if(empty($users_with_phone)){
            return $user;
        }
        $phone_user = $users_with_phone[0];
        
        if ( wp_check_password( $password, $phone_user->user_pass, $phone_user->ID ) ){
            return $phone_user;
        }
    }
    return $user;
}

add_filter( 'gettext', 'wooc_change_login_label', 10, 3 );
function wooc_change_login_label( $translated, $original, $domain ) {
    if ( $translated == "Username or email address" && $domain === 'woocommerce' ) {
        $translated = "Username or email address or phone";
    }
    return $translated;
}

add_filter('add_to_cart_redirect', 'cw_redirect_add_to_cart');
function cw_redirect_add_to_cart() {
    global $woocommerce;
    $cw_redirect_url_checkout = $woocommerce->cart->get_checkout_url();
    return $cw_redirect_url_checkout;
}

/*
**
    by erickorso@gmail.com
*/
    get_template_part('functions/formulas');
    get_template_part('functions/woo-ajax');
    get_template_part('functions/scripts');
/*
    by erickorso@gmail.com
**
*/