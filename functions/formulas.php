<?php


add_action('wp_ajax_nopriv_find_swatches_products', 'find_swatches');
add_action('wp_ajax_find_swatches_products', 'find_swatches');
/**
 * Take a SKU as a string and extract the level, tone, and name.
 **/
function formula_parts($sku)
{
  if (preg_match('/EC1\d\d\d([0-9\.]+)(.+)N/', $sku, $match_data)) {
    $color_data = array();
    $color_data['level'] = $match_data[1];
    $color_data['tone'] = $match_data[2];
    $color_data['name'] = $match_data[1] . $match_data[2];
    return $color_data;
  } else {
    return null;
  }
}

/**
 * Get all the formulas from the items in WooCommerce
 **/
function formulas_table()
{
  global $wpdb;
  $formulas = array();
  if ($result = $wpdb->get_results("select distinct meta_value from $wpdb->postmeta where meta_key='_sku' and meta_value like 'EC1%N'")) {
    foreach ($result as $formula) {
      array_push($formulas, formula_parts($formula->meta_value));
    }
  }
  return $formulas;
}

/**
 * Input an array of levels, and an array of tones. This function will return any combinations of level/tone that exist in woocommerce
 **/
function find_formulas($levels, $tones)
{
  $matching_formulas = array();
  foreach (formulas_table() as $formula_a) {
    if (in_array($formula_a['level'], $levels) && in_array($formula_a['tone'], $tones)) {
      array_push($matching_formulas, $formula_a['name']);
    }
  }
  return $matching_formulas;
}

/**
 * Show all tones that belong to each base as found in the "pillars" spreadsheet
 **/
function tones_for($base_color)
{
  if ($base_color == 'red') {
    return ["NGM", " NMG", "RM", "NRM", "NCG", "CC", "RC", "NCR", "NAC", "RC", "NCR", "NRR", "RR", "RV", "VR", "NVA", "NV", "NVV"];
  } else { // all non-reds (black, brown, blonde) are given together now
    return [
      "AA", "NNA", "NVV", "N", "NN", "NNN", "NGM", "NMG", "N", "NN", "NNN", "NA", "NNA", "NAA", "N", "B", "BB", "N", "NNN",
      "NAA", "NA", "NNA", "NB", "G", "GA", "NGM", "NG", "N", "NAA", "NA", "NGM", "NMG", "B", "BB", "NB", "NRR", "CC", "NV", "NVA",
      "NRR", "G", "GA", "NAA", "NA", "AV", "N", "NN", "NNN"
    ];
  }
}

/**
 * Given a level and a tone, return all the swatches that should be presented if that was your result in the quiz.
 **/

function find_swatches(){
  $base_color = $_POST['base_color'];
  $level = $_POST['level'];
  echo json_encode( find_formulas([$level], tones_for($base_color)));
  wp_die();
}
