<?php

add_action('wp_ajax_nopriv_get_quiz_products', 'get_quiz');
add_action('wp_ajax_get_quiz_products', 'get_quiz');

function get_quiz(){
    
    $searchProduct = $_POST['searchProduct'];

    if (!class_exists('WooCommerce')) return;
    $query = new WC_Product_Query(array(
        // 'limit' => 10,
        // 'return' => 'ids',
        'orderby' => 'date',
        'order' => 'DESC',
        's' => $searchProduct,
    ));

    $products = $query->get_products();
    $productsMap = [];
    foreach ($products as $key => $product) {
        $productObj = [];
        $productObj['id'] = $product->id;
        $productObj['permalink'] = get_permalink($product->id);
        $productObj['name'] = $product->name;
        $productObj['description'] = $product->description;
        $productObj['short_description'] = $product->short_description;
        $productObj['image_id'] = $product->image_id;
        $productObj['image_url'] = wp_get_attachment_image_src($product->image_id, 'single-post-thumbnail', false);
        $productObj['sku'] = $product->sku;
        $productObj['price'] = $product->price;
        $productsMap[$key] = $productObj;
    }
    echo json_encode($productsMap);
    die();
}