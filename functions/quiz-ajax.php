<?php

add_action('wp_ajax_nopriv_save_quiz_form', 'save_quiz');
add_action('wp_ajax_save_quiz_form', 'save_quiz');

function save_quiz(){
    $user_id = wp_get_current_user()->ID;
    $dataQuiz = $_POST['dataQuiz'];
    $metadata = [
        "client-id"            => $user_id,
        "user-name"            => $dataQuiz["user-name"],
        "email"                => $dataQuiz["email"],
        "mobile-number"        => $dataQuiz["mobile-number"],
        "color-type"           => $dataQuiz["color-type"],
        "recent_services"      => $dataQuiz["recent_services"],
        "last-coloring"        => $dataQuiz["last-coloring"],
        "hair-condition"       => $dataQuiz["hair-condition"],
        "hair-length"          => $dataQuiz["hair-length"],
        "much-gray"            => $dataQuiz["much-gray"],
        "hair-resistant"       => $dataQuiz["hair-resistant"],
        "natural-texture"      => $dataQuiz["natural-texture"],
        "keratin"              => $dataQuiz["keratin"],
        "porous"               => $dataQuiz["porous"],
        "amount"               => $dataQuiz["amount"],
        "natural-color-shades" => $dataQuiz["natural-color-shades"],
        "natural-color-level"  => $dataQuiz["natural-color-level"],
        "current-color-shades" => $dataQuiz["current-color-shades"],
        "current-color-level"  => $dataQuiz["current-color-level"],
        "color-shades"         => $dataQuiz["color-shades"],
        "color-level"          => $dataQuiz["color-level"],
        "color-shade"          => $dataQuiz["color-shade"],
        "color-swatches"       => $dataQuiz["color-swatches"],
    ];
    $title = $dataQuiz["user-name"] . " - - ";
    if(strpos($dataQuiz["color-type"], "Root") === false){
        $title .= $dataQuiz["color-swatches"] . " from " . $dataQuiz["natural-color-shades"] . "/". $dataQuiz["natural-color-level"];
    }else{
        $title .= $dataQuiz["color-swatches"] . " from " . $dataQuiz["current-color-shades"] . "/". $dataQuiz["natural-color-level"];
    }


    $quiz_post = array(
        'post_author'           => $user_id,
        'post_title'            => $title,
        'post_status'           => 'draft',
        'post_type'             => 'quiz',
        'meta_input'            => $metadata,
        'post_category'         => array(1),
        'tags_input'            => array('dont-know'), 
        // 'post_content'          => '',
        // 'post_content_filtered' => '',
        // 'post_excerpt'          => '',
        // 'comment_status'        => '',
        // 'post_password'         => '',
        // 'ping_status'           => '',
        // 'pinged'                => '',
        // 'to_ping'               => '',
        // 'post_parent'           => 0,
        // 'menu_order'            => 0,
        // 'import_id'             => 0,
        // 'guid'                  => '',
        // 'context'               => '',
    );

    // insert quiz
    $post_id = wp_insert_post($quiz_post);

    if (!is_wp_error($post_id)) {

        $quizArr = get_user_meta(intval($user_id), 'user-quiz', false);
        if ($quizArr[0]) {
            $meta = $quizArr[0];
            array_push($meta, $post_id);
        } else {
            $meta = [$post_id];
        }
        // update user meta
        $user_data = update_user_meta($user_id, 'user-quiz', $meta);
        if (!is_wp_error($user_data)) {
            $response = [];
            $response['quiz-id'] = $post_id;
            $response['user'] = $user_data;
            echo json_encode($response);
        } else {
            echo json_encode($user_data->get_error_message());
        }
    } else {
        echo json_encode($post_id->get_error_message());
    }
    die();
}