<?php

add_action('wp_ajax_nopriv_get_brand_tone', 'get_brand');
add_action('wp_ajax_get_brand_tone', 'get_brand');

function get_brand(){

    global $wpdb;

    $brandId = $_POST['brandId'];

    $table = "tone_type";

    $sql1 = "SELECT Tone_Type_Id,Tone_Types,coloronly_shade FROM $table where Brand_Id=$brandId and coloronly_shade != 0 ORDER BY Tone_Types";
    
    $toneTypes = $wpdb->get_results($sql1);
    // $tones = $wpdb->get_results("SELECT * FROM $table WHERE Tone_Type_Id = $brandId");

    echo json_encode($toneTypes);

    die();
}
