<?php

function map_users(){
    $allUserData = get_users();
    $userMapData = array();

    foreach ($allUserData as $user) {
        $userMapData[$user->ID] = $user->display_name;
    }
    return $userMapData;
}

add_action('cmb2_admin_init', 'quiz_metaboxes');

function quiz_metaboxes()
{
    $prefix = 'quiz';
    $cmb = new_cmb2_box(array(
        'id'            => $prefix . '-group',
        'title'         => __('Quiz Results', 'cmb2'),
        'object_types'  => array('quiz'),
        'closed'     => false, 
    ));

    $cmb->add_field(array(
        'name' => 'User Name',
        'id'   => 'user-name',
        'type' => 'text',
        'desc' => 'this field doesn´t change the user profile',
    ));
    $cmb->add_field(array(
        'name' => 'User email',
        'id'   => 'email',
        'type' => 'text',
        'desc' => 'this field doesn´t change the user profile',
    ));
    $cmb->add_field(array(
        'name' => 'Mobile number',
        'id'   => 'mobile-number',
        'type' => 'text',
        'desc' => 'this field doesn´t change the user profile',
    ));
    $cmb->add_field(array(
        'name'    => 'Client',
        'id'      => 'client-id',
        'type'    => 'select',
        'default' => wp_get_current_user()->ID,
        'options' => 'map_users'
    ));

    $cmb->add_field(array(
        'name' => 'Color Type',
        'id'   => 'color-type',
        'type' => 'text',
        'desc' => 'Root Touch Up / All Over Color',
    ));
    $cmb->add_field(array(
        'name' => 'Recent Services',
        'id'   => 'recent_services',
        'type' => 'text',
        'desc' => 'Keratin / Hair Color Remover / Relaxer / Perm / None',
    ));
    $cmb->add_field(array(
        'name' => 'Last-Coloring',
        'id'   => 'last-coloring',
        'type' => 'text',
        'desc' => '2 WKS / 4 WKS / 6 WKS / 8 WKS',
    ));
    $cmb->add_field(array(
        'name' => 'Hair Condition',
        'id'   => 'hair-condition',
        'type' => 'text',
        'desc' => 'Normal - Healthy / Porous / Severe damaged',
    ));
    $cmb->add_field(array(
        'name' => 'Hair Length',
        'id'   => 'hair-length',
        'type' => 'text',
        'desc' => 'Very Short / Short / Shoulder Length / Long / Very Long',
    ));
    $cmb->add_field(array(
        'name' => 'Much Gray',
        'id'   => 'much-gray',
        'type' => 'text',
        'desc' => 'None or hardly any / Some / Mostly Gray / All Gray',
    ));
    $cmb->add_field(array(
        'name' => 'Hair Resistant Gray ',
        'id'   => 'hair-resistant',
        'type' => 'text',
        'desc' => 'Yes / No',
    ));
    $cmb->add_field(array(
        'name' => 'Natural Texture',
        'id'   => 'natural-texture',
        'type' => 'text',
        'desc' => 'Fine / Normal / Coarse / Very Coarse',
    ));
    $cmb->add_field(array(
        'name' => 'Keratin',
        'id'   => 'keratin',
        'type' => 'text',
        'desc' => 'Yes / No',
    ));
    $cmb->add_field(array(
        'name' => 'Porous',
        'id'   => 'porous',
        'type' => 'text',
        'desc' => 'Yes / No',
    ));
    $cmb->add_field(array(
        'name' => 'Amount',
        'id'   => 'amount',
        'type' => 'text',
        'desc' => 'xx(oz)',
    ));
    $cmb->add_field(array(
        'name' => 'Natural Color Shades',
        'id'   => 'natural-color-shades',
        'type' => 'text',
        'desc' => 'Black / Brown / Blonde / Red',
    ));
    $cmb->add_field(array(
        'name' => 'Natural Color Level',
        'id'   => 'natural-color-level',
        'type' => 'text',
        'desc' => '1 / 2 / 3 / 4 / 5 / 6 / 7 / 8 / 9 / 10 / 11',
    ));
    $cmb->add_field(array(
        'name' => 'Current Color Shades',
        'id'   => 'current-color-shades',
        'type' => 'text',
        'desc' => 'Black / Brown / Blonde / Red',
    ));
    $cmb->add_field(array(
        'name' => 'Current Color Level',
        'id'   => 'current-color-level',
        'type' => 'text',
        'desc' => '1 / 2 / 3 / 4 / 5 / 6 / 7 / 8 / 9 / 10 / 11',
    ));
    $cmb->add_field(array(
        'name' => 'Color Shades',
        'id'   => 'color-shades',
        'type' => 'text',
        'desc' => 'Black / Brown / Blonde / Red',
    ));
    $cmb->add_field(array(
        'name' => 'Color Level',
        'id'   => 'color-level',
        'type' => 'text',
        'desc' => '1 / 2 / 3 / 4 / 5 / 6 / 7 / 8 / 9 / 10 / 11',
    ));
    $cmb->add_field(array(
        'name' => 'Color Shade',
        'id'   => 'color-shade',
        'type' => 'text',
        'desc' => 'Multiple options',
    ));
    $cmb->add_field(array(
        'name' => 'Color Swatches',
        'id'   => 'color-swatches',
        'type' => 'text',
        'desc' => 'Result of the formula main color, level & shade',
    ));
    
}
