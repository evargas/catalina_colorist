<?php
function custom_scripts_() {
	$uploads = wp_upload_dir();
	
	if ($dir = opendir($uploads['basedir'] . '/color_swatches')) {
		$images = array();
		while (false !== ($file = readdir($dir))) {
			if ($file != "." && $file != "..") {
				$images[] = $file;
			}
		}
		closedir($dir);
	}

	wp_register_script( 'quiz-ajax',    get_stylesheet_directory_uri().'/js/custom-ajax.js', array('jquery'), '2', true );
	wp_enqueue_script('quiz-ajax' );
	wp_localize_script('quiz-ajax', 'quiz_vars', [
		'ajaxurl' => admin_url('admin-ajax.php'), 
		'base_ajax_url' => get_home_url(), 
		'images_ajax' => $images
		]);
}

add_action( 'wp_enqueue_scripts', 'custom_scripts_' );
add_action( 'admin_enqueue_scripts', 'custom_admin_scripts_' );

