<?php
function quiz_cpt(){

    // post type quiz
    $post_type = 'quiz';
    $labels = array(
        'name'               => 'quiz',
        'singular_name'      => 'quiz',
        'menu_name'          => 'Quiz',
        'name_admin_bar'     => 'quiz',
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New quiz',
        'new_item'           => 'New quiz',
        'edit_item'          => 'Edit quiz',
        'view_item'          => 'View quiz',
        'all_items'          => 'All quizs',
        'search_items'       => 'Search quiz',
        'parent_item_colon'  => 'Parent quiz:',
        'not_found'          => 'No quiz found.',
        'not_found_in_trash' => 'No quiz found in Trash.'
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'menu_position'      => 5,
        'menu_icon'          => 'dashicons-awards',
        'description'        => 'You can add any quiz items',
        'query_var'          => true,
        'rewrite'            => array('slug' => 'quiz'),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'supports'           => array('title', 'excerpt'),
        'taxonomies'         => array('post_tag', 'category')
    );

    register_post_type($post_type, $args);
}
add_action('init', 'quiz_cpt');

function my_rewrite_flush()
{
    quiz_cpt();
    flush_rewrite_rules();
}
register_activation_hook(__FILE__, 'my_rewrite_flush');
