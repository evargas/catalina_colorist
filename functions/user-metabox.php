<?php

add_action('cmb2_admin_init', 'user_metaboxes');

function user_metaboxes(){
    
    if(isset($_GET["user_id"])){
        $user_id = $_GET["user_id"];
    }else{
        $user_id = wp_get_current_user()->ID;
    }
    
    $quizArr = get_user_meta(intval($user_id), 'user-quiz', false);
    $quizStr = '';
    if(isset($quizArr[0])){
        foreach ($quizArr[0] as $key => $quiz) {
            $quizLink = get_site_url(). '/wp-admin/post.php?post='. $quiz.'&action=edit';
            $quizStr .=  '<a class="cmb2-metabox-title" href="'. $quizLink.'">Quiz'.($key + 1 ).'</a><br/>';
        }
    }else{
        $quizStr = 'No color quiz found';
    }

    $cmb = new_cmb2_box(array(
        'id' => 'extra-user-fields',
        'title' => esc_html__('Quiz information', 'cmb2'),
        'object_types' => array('user'),
        'show_names' => true,
        'new_user_section' => 'add-new-user',
    ));

    $cmb->add_field(array(
        'name' => 'Color Quiz List',
        'desc' => $quizStr,
        'id' => 'user-quiz',
        'type' => 'title',
        'on_front' => false,
        'repeatable' => true,
    ));
    
}
